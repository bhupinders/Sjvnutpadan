﻿using ASPSnippets.GoogleAPI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SatlujWebApp
{
    public partial class Login : System.Web.UI.Page
    {
        string clearText = "";
        protected void Page_Load(object sender, EventArgs e)
        {
           
            lblcount.Text = Application["NoOfVisitors"].ToString();
            GetGoogle();
            if (Request.Url.AbsoluteUri.Contains("error"))
            {
                string error = Request.QueryString["error"].ToString();
                if (error == "error")
                {
                    lblinfo.Text = "Invalid Login Attempt For This Location";
                }
            }
        }
        [System.Web.Services.WebMethod]
        public static string Get(string Email, string Id, string Name)
        {
            SqlConnection con = new SqlConnection(GetConnectionString1());
            con.Open();
            SqlCommand cmd = new SqlCommand("select * from TblLoginAuthFb where fbId=@Fbid and Location='Urja' and Roleid=4", con);
            cmd.Parameters.AddWithValue("@Fbid", Id.ToString());
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                dr.Read();

                return Email.ToString();

            }
            else
            {
                SqlCommand cmdinsert = new SqlCommand("insert into TblLoginAuthFb values(@Fbid,@fbname,@FbEmail,@location,@roleid)", con);
                cmdinsert.CommandType = CommandType.Text;
                cmdinsert.Parameters.AddWithValue("@Fbid", Id.ToString());
                cmdinsert.Parameters.AddWithValue("@fbname", Name.ToString());
                cmdinsert.Parameters.AddWithValue("@FbEmail", Email.ToString());
                cmdinsert.Parameters.AddWithValue("@location", "Urja");
                cmdinsert.Parameters.AddWithValue("@roleid", 4);
                cmdinsert.ExecuteNonQuery();

                return Email.ToString();
            }

        }
        protected void btnlogin_Click(object sender, EventArgs e)
        {
            try
            {
                SqlConnection con = new SqlConnection(GetConnectionString());
                con.Open();
                
                byte[] bytes = Encoding.Unicode.GetBytes(Request.Form["txtpassword"].ToString());
                byte[] inArray = HashAlgorithm.Create("SHA1").ComputeHash(bytes);
                string clearText = Convert.ToBase64String(inArray);

                SqlCommand cmd = new SqlCommand("CheckUser1", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter p1 = new SqlParameter("username",Request.Form["txtusername"]);
                SqlParameter p2 = new SqlParameter("password", clearText);
                cmd.Parameters.Add(p1);
                cmd.Parameters.Add(p2);
                SqlDataReader rd = cmd.ExecuteReader();

                if (rd.HasRows)
                {
                    rd.Read();
                    lblinfo.Text = "You are Authorized.";
                    Session["username"] = Request.Form["txtusername"].ToString();
                    Session["RoleId"] = rd["RoleId"].ToString();
                    Session["Location"] = rd["Location"].ToString();
                    Session["Name"] = rd["Name"].ToString();
                    Session["Email"] = rd["EmailId"].ToString();
                    Session["user"] = Session["username"].ToString();
                    FormsAuthentication.RedirectFromLoginPage(Request.Form["txtusername"], true);
                    if (Convert.ToInt32(rd["RoleId"]) == 1 || Convert.ToInt32(rd["RoleId"]) == 2)
                    {
                        if(Convert.ToInt32(rd["RoleId"]) == 1)
                        {
                            Response.Redirect("dashboard.aspx");
                        }
                       else if (Convert.ToInt32(rd["RoleId"]) == 2 && Session["Location"].ToString().ToUpper().Contains("URJA"))
                        {
                            Response.Redirect("dashboard.aspx");
                        }
                        else
                        {
                            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Notify", "alert('Please select the date firstInvalid Login Attempt For This Location');", true);
                            Response.Redirect("Login.aspx?error=error");
                       
                        }
                    }
                   
                    else
                    {
                        if ((Convert.ToInt32(rd["RoleId"]) == 3 && Session["Location"].ToString().ToUpper().Contains("URJA")) || (Convert.ToInt32(rd["RoleId"]) == 4 && Session["Location"].ToString().ToUpper().Contains("URJA")))
                        {
                            Response.Redirect("dashboardsatluj.aspx");
                        }
                        else
                          
                        {
                            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Notify", "alert('Please select the date firstInvalid Login Attempt For This Location');", true);
                            Response.Redirect("Login.aspx?error=error");

                        }
                       
                    }
                }
                else
                {
                   lblinfo.Text = "Invalid username or password.";
                }
            }
            catch(Exception ex)
            {
                lblinfo.Text = "Failed to connect";
            }
            
        }

        public void GetGoogle()
        {
            GoogleConnect.ClientId = "726415226138-kb509idsmm4320d9t8158umi3ot10fsb.apps.googleusercontent.com";
            GoogleConnect.ClientSecret = "cYCH4ww0hqH5BSmX6XNfKY8D";

            GoogleConnect.RedirectUri = Request.Url.AbsoluteUri.Split('?')[0];

            if (!string.IsNullOrEmpty(Request.QueryString["code"]))
            {
                string code = Request.QueryString["code"];
                string json = GoogleConnect.Fetch("me", code);
                GoogleProfile profile = new JavaScriptSerializer().Deserialize<GoogleProfile>(json);
                string ID = profile.Id;
                string Name = profile.DisplayName;
                String Email = profile.Emails.Find(email => email.Type == "account").Value;

                SqlConnection con = new SqlConnection(GetConnectionString1());
                con.Open();
                SqlCommand cmd = new SqlCommand("select * from TblLoginAuthFb where fbId=@Fbid and Location='Satluj' and Roleid=4", con);
                cmd.Parameters.AddWithValue("@Fbid", ID.ToString());
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dr.Read();
                    Response.Redirect("dashboardsatluj.aspx?username=" + Email.ToString());
                }
                else
                {
                    SqlCommand cmdinsert = new SqlCommand("insert into TblLoginAuthFb values(@Fbid,@fbname,@FbEmail,@location,@roleid)", con);
                    cmdinsert.CommandType = CommandType.Text;
                    cmdinsert.Parameters.AddWithValue("@Fbid", ID.ToString());
                    cmdinsert.Parameters.AddWithValue("@fbname", Name.ToString());
                    cmdinsert.Parameters.AddWithValue("@FbEmail", Email.ToString());
                    cmdinsert.Parameters.AddWithValue("@location", "Satluj");
                    cmdinsert.Parameters.AddWithValue("@roleid", 4);
                    cmdinsert.ExecuteNonQuery();
                    Response.Redirect("dashboardsatluj.aspx?username=" + Email.ToString());
                }

            }
            if (Request.QueryString["error"] == "access_denied")
            {
                ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", "alert('Access denied.')", true);
            }
        }
        protected void btngoogle_Click(object sender, EventArgs e)
        {
            GoogleConnect.Authorize("profile", "email");
        }
        public class GoogleProfile
        {
            public string Id { get; set; }
            public string DisplayName { get; set; }
            public Image Image { get; set; }
            public List<Email> Emails { get; set; }
            public string Gender { get; set; }
            public string ObjectType { get; set; }
        }

        public class Email
        {
            public string Value { get; set; }
            public string Type { get; set; }
        }

        public class Image
        {
            public string Url { get; set; }
        }

        public string GetConnectionString()
        {
            return System.Configuration.ConfigurationManager.ConnectionStrings["Local"].ConnectionString;
        }
        public static string GetConnectionString1()
        {
            return System.Configuration.ConfigurationManager.ConnectionStrings["Local"].ConnectionString;
        }
    }
    }

