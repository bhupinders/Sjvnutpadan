﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SatlujWebApp
{
    public partial class ResetPassword : System.Web.UI.Page
    {
        string password = "";
        string newpassword = "";
        string confirmpassword = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["username"] == null)
            {
                Response.Redirect("Login.aspx");
            }
            if (!Page.IsPostBack)
            {
                getNotification();
                getLink();
                SqlConnection con1 = new SqlConnection(GetConnectionString());
                SqlCommand cmdcount = new SqlCommand("select count(*) from tblnotificationurja", con1);
                con1.Open();
                int countnotification = (int)cmdcount.ExecuteScalar();
                lblnotificationcount.Text = countnotification.ToString();
                cmdcount.Dispose();
                con1.Close();
            }
        }
        public void getLink()
        {
            SqlConnection con1 = new SqlConnection(GetConnectionString());
            SqlCommand cmdcount = new SqlCommand("select count(*) from  [TblLinksUrja]", con1);
            con1.Open();
            int count = (int)cmdcount.ExecuteScalar();
            if (count == 1)
            {
                SqlConnection con = new SqlConnection(GetConnectionString());
                SqlCommand cmd = new SqlCommand("select top(1) id,name,description,url from [TblLinksUrja] order by id desc", con);
                con.Open();
                cmd.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dr.Read();
                    lbllink4.Text = dr["description"].ToString();
                    urllink4.Text = dr["url"].ToString();
                }
                dr.Dispose();
                cmd.Dispose();
                con.Close();
            }
            if (count == 2)
            {
                SqlConnection con = new SqlConnection(GetConnectionString());
                SqlCommand cmd = new SqlCommand("select top(1) id,name,description,url from [TblLinksUrja] order by id desc", con);
                con.Open();
                cmd.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dr.Read();
                    lbllink4.Text = dr["description"].ToString();
                    urllink4.Text = dr["url"].ToString();
                }
                dr.Dispose();
                cmd.Dispose();
                SqlCommand cmd1 = new SqlCommand("SELECT name,Description,Url FROM [TblLinksUrja] WHERE Id NOT IN (SELECT MAX( Id )FROM [TblLinksUrja] ) order by id desc", con);

                cmd1.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr1 = cmd1.ExecuteReader();
                if (dr1.HasRows)
                {
                    dr1.Read();
                    lbllink3.Text = dr1["description"].ToString();
                    urllink3.Text = dr1["url"].ToString();
                }
                dr1.Dispose();
                cmd1.Dispose();
                con.Close();
            }
            if (count == 3)
            {
                SqlConnection con = new SqlConnection(GetConnectionString());
                SqlCommand cmd = new SqlCommand("select top(1) id,name,description,url from [TblLinksUrja] order by id desc", con);
                con.Open();
                cmd.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dr.Read();
                    lbllink4.Text = dr["description"].ToString();
                    urllink4.Text = dr["url"].ToString();
                }
                dr.Dispose();
                cmd.Dispose();
                SqlCommand cmd1 = new SqlCommand("SELECT name,Description,Url FROM [TblLinksUrja] WHERE Id NOT IN (SELECT MAX( Id )FROM [TblLinksUrja] ) order by id desc", con);

                cmd1.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr1 = cmd1.ExecuteReader();
                if (dr1.HasRows)
                {
                    dr1.Read();
                    lbllink3.Text = dr1["description"].ToString();
                    urllink3.Text = dr1["url"].ToString();
                }
                dr1.Dispose();
                cmd1.Dispose();

                SqlCommand cmd2 = new SqlCommand("SELECT TOP 1 * From(select Top 3 * from [TblLinksUrja] ORDER BY Id DESC) x ORDER BY Id", con);

                cmd2.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr2 = cmd2.ExecuteReader();
                if (dr2.HasRows)
                {
                    dr2.Read();
                    lbllink2.Text = dr2["description"].ToString();
                    urllink2.Text = dr2["url"].ToString();
                }
                dr2.Dispose();
                cmd2.Dispose();
                con.Close();
            }

            if (count >= 4)
            {
                SqlConnection con = new SqlConnection(GetConnectionString());
                SqlCommand cmd = new SqlCommand("select top(1) id,name,description,url from [TblLinksUrja] order by id desc", con);
                con.Open();
                cmd.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dr.Read();
                    lbllink4.Text = dr["description"].ToString();
                    urllink4.Text = dr["url"].ToString();
                }
                dr.Dispose();
                cmd.Dispose();
                SqlCommand cmd1 = new SqlCommand("SELECT name,Description,Url FROM [TblLinksUrja] WHERE Id NOT IN (SELECT MAX( Id )FROM [TblLinksUrja] ) order by id desc", con);

                cmd1.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr1 = cmd1.ExecuteReader();
                if (dr1.HasRows)
                {
                    dr1.Read();
                    lbllink3.Text = dr1["description"].ToString();
                    urllink3.Text = dr1["url"].ToString();
                }
                dr1.Dispose();
                cmd1.Dispose();

                SqlCommand cmd2 = new SqlCommand("SELECT TOP 1 * From(select Top 3 * from [TblLinksUrja] ORDER BY Id DESC) x ORDER BY Id", con);

                cmd2.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr2 = cmd2.ExecuteReader();
                if (dr2.HasRows)
                {
                    dr2.Read();
                    lbllink2.Text = dr2["description"].ToString();
                    urllink2.Text = dr2["url"].ToString();
                }
                dr2.Dispose();
                cmd2.Dispose();

                SqlCommand cmd4 = new SqlCommand("SELECT TOP 1 * From(select Top 4 * from [TblLinksUrja] ORDER BY Id DESC) x ORDER BY Id", con);

                cmd4.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr4 = cmd4.ExecuteReader();
                if (dr4.HasRows)
                {
                    dr4.Read();
                    lbllink1.Text = dr4["description"].ToString();
                    urllink1.Text = dr4["url"].ToString();
                }
                dr4.Dispose();
                cmd4.Dispose();
                con.Close();

            }
            con1.Close();
        }
        public void getNotification()
        {
            SqlConnection con1 = new SqlConnection(GetConnectionString());
            SqlCommand cmdcount = new SqlCommand("select count(*) from tblnotificationurja", con1);
            con1.Open();
            int countnotification = (int)cmdcount.ExecuteScalar();
            lblnotificationcount.Text = countnotification.ToString();
            if (countnotification == 1)
            {
                SqlConnection con = new SqlConnection(GetConnectionString());
                SqlCommand cmd = new SqlCommand("select top(1) notification from [tblnotificationurja] order by id desc", con);
                con.Open();
                cmd.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dr.Read();
                    notification1.Text = dr["notification"].ToString();
                    lblnotification.Text = dr["notification"].ToString();
                }

                dr.Dispose();
                cmd.Dispose();
                con.Close();
            }

            if (countnotification == 2)
            {
                SqlConnection con = new SqlConnection(GetConnectionString());
                SqlCommand cmd = new SqlCommand("select top(1) notification from [tblnotificationurja] order by id desc", con);
                con.Open();
                cmd.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dr.Read();
                    notification1.Text = dr["notification"].ToString();
                    lblnotification.Text = dr["notification"].ToString();
                }

                dr.Dispose();
                cmd.Dispose();

                SqlCommand cmd1 = new SqlCommand("SELECT id, notification FROM [tblnotificationurja] WHERE Id NOT IN (SELECT MAX( Id )FROM [tblnotificationurja] ) order by id desc", con);

                cmd1.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr1 = cmd1.ExecuteReader();
                if (dr1.HasRows)
                {
                    dr1.Read();

                    notification2.Text = dr1["notification"].ToString();
                }
                dr1.Dispose();
                cmd1.Dispose();
                con.Close();

            }
            if (countnotification >= 3)
            {
                SqlConnection con = new SqlConnection(GetConnectionString());
                SqlCommand cmd = new SqlCommand("select top(1) notification from [tblnotificationurja] order by id desc", con);
                con.Open();
                cmd.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dr.Read();
                    notification1.Text = dr["notification"].ToString();
                    lblnotification.Text = dr["notification"].ToString();
                }

                dr.Dispose();
                cmd.Dispose();

                SqlCommand cmd1 = new SqlCommand("SELECT id, notification FROM [tblnotificationurja] WHERE Id NOT IN (SELECT MAX( Id )FROM [tblnotificationurja] ) order by id desc", con);

                cmd1.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr1 = cmd1.ExecuteReader();
                if (dr1.HasRows)
                {
                    dr1.Read();

                    notification2.Text = dr1["notification"].ToString();
                }
                dr1.Dispose();
                cmd1.Dispose();

                SqlCommand cmd2 = new SqlCommand("SELECT TOP 1 * From(select Top 3 * from [tblnotificationurja] ORDER BY Id DESC) x ORDER BY Id", con);

                cmd2.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr2 = cmd2.ExecuteReader();
                if (dr2.HasRows)
                {
                    dr2.Read();
                    notification3.Text = dr2["notification"].ToString();
                }
                dr2.Dispose();
                cmd2.Dispose();
                con.Close();
            }
            con1.Close();
            cmdcount.Dispose();

        }
        protected void btnsubmit_Click(object sender, EventArgs e)
        {
            password = Request.Form["oldpassword"];
            newpassword = Request.Form["newpassword"];
            confirmpassword = Request.Form["confirmpassword"];
            SqlConnection con = new SqlConnection(GetConnectionString());
            if (Session["username"].ToString().Contains("@"))
            {
                SqlCommand chkoldpassword = new SqlCommand("select * from tbllogin where emailid=@emailid and password=@password", con);
                chkoldpassword.CommandType = CommandType.Text;
                con.Open();

                byte[] bytes = Encoding.Unicode.GetBytes(password.ToString());
                byte[] inArray = HashAlgorithm.Create("SHA1").ComputeHash(bytes);
                password = Convert.ToBase64String(inArray);


                chkoldpassword.Parameters.AddWithValue("emailid", Session["username".ToString()]);
                chkoldpassword.Parameters.AddWithValue("password", password.ToString());
                SqlDataReader dr = chkoldpassword.ExecuteReader();
                if (!dr.HasRows)
                {
                     lblmessage.Text = "Old Password is incorrect:";
                     lblmessage.ForeColor = System.Drawing.Color.Red;
                    chkoldpassword.Dispose();
                    dr.Dispose();
                    return;
                }
                else
                {
                    SqlCommand update = new SqlCommand("update tbllogin set password=@password where emailid=@emailid", con);
                    update.CommandType = CommandType.Text;

                    byte[] bytes1 = Encoding.Unicode.GetBytes(newpassword.ToString());
                    byte[] inArray1 = HashAlgorithm.Create("SHA1").ComputeHash(bytes1);
                    newpassword = Convert.ToBase64String(inArray1);



                    update.Parameters.AddWithValue("@emailid", Session["username"].ToString());
                    update.Parameters.AddWithValue("@password", newpassword.ToString());
                    update.ExecuteNonQuery();
                    update.Dispose();
                     lblmessage.Text="Password Changed";
                }
            }

            else
            {
                SqlCommand chkoldpass = new SqlCommand("select * from tbllogin where phone=@emailid and password=@password", con);
                chkoldpass.CommandType = CommandType.Text;
                con.Open();
                chkoldpass.Parameters.AddWithValue("@emailid", Session["username".ToString()]);
                chkoldpass.Parameters.AddWithValue("password", password.ToString());
                SqlDataReader dr1 = chkoldpass.ExecuteReader();
                if (!dr1.HasRows)
                {
                      lblmessage.Text = "User already exsists:";
                      lblmessage.ForeColor = System.Drawing.Color.Red;
                    dr1.Dispose();
                    chkoldpass.Dispose();
                    return;
                }
                else
                {

                    SqlCommand updatepass = new SqlCommand("update tbllogin set password=@password where phone=@emailid", con);
                    updatepass.CommandType = CommandType.Text;
                    updatepass.Parameters.AddWithValue("@emailid", Session["username"].ToString());
                    updatepass.Parameters.AddWithValue("@password", newpassword.ToString());
                    updatepass.ExecuteNonQuery();
                    updatepass.Dispose();
                     lblmessage.Text="Password Changed";
                }

            }


        }
        public string GetConnectionString()
        {
            return System.Configuration.ConfigurationManager.ConnectionStrings["Local"].ConnectionString;
        }

    }
}