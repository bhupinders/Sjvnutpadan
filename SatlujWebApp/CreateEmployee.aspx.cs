﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace SatlujWebApp
{
    public partial class CreateEmployee : System.Web.UI.Page
    {
        string Name = "";
        string Password = "";
        string ConfirmPassword = "";
        string Mobile = "";
        string Email = "";
        string Location = "";
        int RoleID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["username"] == null)
            {
                Response.Redirect("Login.aspx");
            }
            if (!Page.IsPostBack)
            {
                getNotification();
                SqlConnection con1 = new SqlConnection(GetConnectionString());
                SqlCommand cmdcount = new SqlCommand("select count(*) from tblnotificationurja", con1);
                con1.Open();
                int countnotification = (int)cmdcount.ExecuteScalar();
                lblnotificationcount.Text = countnotification.ToString();
                cmdcount.Dispose();
                con1.Close();
            }
        }
        protected void btnreset_Click(object sender, EventArgs e)
        {
            foreach (Control c in Controls)
            {
                if (c.GetType() == typeof(TextBox))
                {
                    ((TextBox)(c)).Text = string.Empty;
                }
            }
            lblmessage.Text = "Form has been reset!";
            lblmessage.ForeColor = System.Drawing.Color.Green;
        }
        public void getNotification()
        {
            SqlConnection con1 = new SqlConnection(GetConnectionString());
            SqlCommand cmdcount = new SqlCommand("select count(*) from tblnotificationurja", con1);
            con1.Open();
            int countnotification = (int)cmdcount.ExecuteScalar();
            lblnotificationcount.Text = countnotification.ToString();
            if (countnotification == 1)
            {
                SqlConnection con = new SqlConnection(GetConnectionString());
                SqlCommand cmd = new SqlCommand("select top(1) notification from [tblnotificationurja] order by id desc", con);
                con.Open();
                cmd.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dr.Read();
                    notification1.Text = dr["notification"].ToString();
                    lblnotification.Text = dr["notification"].ToString();
                }

                dr.Dispose();
                cmd.Dispose();
                con.Close();
            }

            if (countnotification == 2)
            {
                SqlConnection con = new SqlConnection(GetConnectionString());
                SqlCommand cmd = new SqlCommand("select top(1) notification from [tblnotificationurja] order by id desc", con);
                con.Open();
                cmd.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dr.Read();
                    notification1.Text = dr["notification"].ToString();
                    lblnotification.Text = dr["notification"].ToString();
                }

                dr.Dispose();
                cmd.Dispose();

                SqlCommand cmd1 = new SqlCommand("SELECT id, notification FROM [tblnotificationurja] WHERE Id NOT IN (SELECT MAX( Id )FROM [tblnotificationurja] ) order by id desc", con);

                cmd1.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr1 = cmd1.ExecuteReader();
                if (dr1.HasRows)
                {
                    dr1.Read();

                    notification2.Text = dr1["notification"].ToString();
                }
                dr1.Dispose();
                cmd1.Dispose();
                con.Close();

            }
            if (countnotification >= 3)
            {
                SqlConnection con = new SqlConnection(GetConnectionString());
                SqlCommand cmd = new SqlCommand("select top(1) notification from [tblnotificationurja] order by id desc", con);
                con.Open();
                cmd.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dr.Read();
                    notification1.Text = dr["notification"].ToString();
                    lblnotification.Text = dr["notification"].ToString();
                }

                dr.Dispose();
                cmd.Dispose();

                SqlCommand cmd1 = new SqlCommand("SELECT id, notification FROM [tblnotificationurja] WHERE Id NOT IN (SELECT MAX( Id )FROM [tblnotificationurja] ) order by id desc", con);

                cmd1.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr1 = cmd1.ExecuteReader();
                if (dr1.HasRows)
                {
                    dr1.Read();

                    notification2.Text = dr1["notification"].ToString();
                }
                dr1.Dispose();
                cmd1.Dispose();

                SqlCommand cmd2 = new SqlCommand("SELECT TOP 1 * From(select Top 3 * from [tblnotificationurja] ORDER BY Id DESC) x ORDER BY Id", con);

                cmd2.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr2 = cmd2.ExecuteReader();
                if (dr2.HasRows)
                {
                    dr2.Read();
                    notification3.Text = dr2["notification"].ToString();
                }
                dr2.Dispose();
                cmd2.Dispose();
                con.Close();
            }
            con1.Close();
            cmdcount.Dispose();

        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            Name = Request.Form["Name"];
            Password = Request.Form["password"];
            ConfirmPassword = Request.Form["Confirmpassword"];
            Mobile = Request.Form["mobile"];
            Email = Request.Form["email"];
            Location = rdlist.SelectedItem.Text;
            RoleID = 3;
            execution(Name, Password, Mobile, Email, Location, RoleID);
           this.lblmessage.Text = "New Employee profile has been created ";
            Name = "";
            Password = "";
            ConfirmPassword = "";
            Mobile = "";

        }
        public string GetConnectionString()
        {
            return System.Configuration.ConfigurationManager.ConnectionStrings["Local"].ConnectionString;
        }
        private void execution(string Name, string Password, string Mobile, string Email, string Location, int RoleID)
        {
            byte[] bytes = Encoding.Unicode.GetBytes(Password.ToString());
            byte[] inArray = HashAlgorithm.Create("SHA1").ComputeHash(bytes);
            Password = Convert.ToBase64String(inArray);
            SqlConnection con = new SqlConnection(GetConnectionString());
            con.Open();
            SqlCommand cmd = new SqlCommand("CreateAdmin", con);
            cmd.CommandType = CommandType.StoredProcedure;
            Checkadmin();
            SqlParameter p2 = new SqlParameter("Email", Email);
            SqlParameter p4 = new SqlParameter("Mobile", Mobile);
            SqlParameter p3 = new SqlParameter("Password", Password);
            SqlParameter p6 = new SqlParameter("RoleID", RoleID);
            SqlParameter p1 = new SqlParameter("Name", Name);
            SqlParameter p5 = new SqlParameter("Location", "Urja");
            cmd.Parameters.Add(p1);
            cmd.Parameters.Add(p2);
            cmd.Parameters.Add(p3);
            cmd.Parameters.Add(p4);
            cmd.Parameters.Add(p5);
            cmd.Parameters.Add(p6);
            cmd.ExecuteNonQuery();

            con.Close();
            cmd.Dispose();

        }
        public void Checkadmin()
        {
            SqlConnection con = new SqlConnection(GetConnectionString());
            con.Open();
            SqlCommand cmd = new SqlCommand("select * from tbllogin where emailid=@email or phone=@phone ", con);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@email", Session["username"].ToString());
            cmd.Parameters.AddWithValue("@phone", Session["username"].ToString());
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                lblmessage.Text = "User already exsists";
                lblmessage.ForeColor = System.Drawing.Color.Red;
                return;
            }
            con.Close();
        }
    }
}