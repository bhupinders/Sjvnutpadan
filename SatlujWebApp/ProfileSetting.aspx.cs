﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SatlujWebApp
{
    public partial class ProfileSetting : System.Web.UI.Page
    {
        string username = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                BindData();
                getNotification();
                getLink();
                SqlConnection con1 = new SqlConnection(GetConnectionString());
                SqlCommand cmdcount = new SqlCommand("select count(*) from tblnotificationurja", con1);
                con1.Open();
                int countnotification = (int)cmdcount.ExecuteScalar();
                lblnotificationcount.Text = countnotification.ToString();
                con1.Close();
                cmdcount.Dispose();
            }
            if (Session["username"].ToString() == null || Session["username"].ToString() == "")
            {
                Response.Redirect("Login.aspx");
            }
            else
            {
                username = Session["username"].ToString();
            }
        }
        public void getLink()
        {
            SqlConnection con1 = new SqlConnection(GetConnectionString());
            SqlCommand cmdcount = new SqlCommand("select count(*) from  [TblLinksUrja]", con1);
            con1.Open();
            int count = (int)cmdcount.ExecuteScalar();
            if (count == 1)
            {
                SqlConnection con = new SqlConnection(GetConnectionString());
                SqlCommand cmd = new SqlCommand("select top(1) id,name,description,url from [TblLinksUrja] order by id desc", con);
                con.Open();
                cmd.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dr.Read();
                    lbllink4.Text = dr["description"].ToString();
                    urllink4.Text = dr["url"].ToString();
                }
                dr.Dispose();
                cmd.Dispose();
                con.Close();
            }
            if (count == 2)
            {
                SqlConnection con = new SqlConnection(GetConnectionString());
                SqlCommand cmd = new SqlCommand("select top(1) id,name,description,url from [TblLinksUrja] order by id desc", con);
                con.Open();
                cmd.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dr.Read();
                    lbllink4.Text = dr["description"].ToString();
                    urllink4.Text = dr["url"].ToString();
                }
                dr.Dispose();
                cmd.Dispose();
                SqlCommand cmd1 = new SqlCommand("SELECT name,Description,Url FROM [TblLinksUrja] WHERE Id NOT IN (SELECT MAX( Id )FROM [TblLinksUrja] ) order by id desc", con);

                cmd1.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr1 = cmd1.ExecuteReader();
                if (dr1.HasRows)
                {
                    dr1.Read();
                    lbllink3.Text = dr1["description"].ToString();
                    urllink3.Text = dr1["url"].ToString();
                }
                dr1.Dispose();
                cmd1.Dispose();
                con.Close();
            }
            if (count == 3)
            {
                SqlConnection con = new SqlConnection(GetConnectionString());
                SqlCommand cmd = new SqlCommand("select top(1) id,name,description,url from [TblLinksUrja] order by id desc", con);
                con.Open();
                cmd.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dr.Read();
                    lbllink4.Text = dr["description"].ToString();
                    urllink4.Text = dr["url"].ToString();
                }
                dr.Dispose();
                cmd.Dispose();
                SqlCommand cmd1 = new SqlCommand("SELECT name,Description,Url FROM [TblLinksUrja] WHERE Id NOT IN (SELECT MAX( Id )FROM [TblLinksUrja] ) order by id desc", con);

                cmd1.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr1 = cmd1.ExecuteReader();
                if (dr1.HasRows)
                {
                    dr1.Read();
                    lbllink3.Text = dr1["description"].ToString();
                    urllink3.Text = dr1["url"].ToString();
                }
                dr1.Dispose();
                cmd1.Dispose();

                SqlCommand cmd2 = new SqlCommand("SELECT TOP 1 * From(select Top 3 * from [TblLinksUrja] ORDER BY Id DESC) x ORDER BY Id", con);

                cmd2.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr2 = cmd2.ExecuteReader();
                if (dr2.HasRows)
                {
                    dr2.Read();
                    lbllink2.Text = dr2["description"].ToString();
                    urllink2.Text = dr2["url"].ToString();
                }
                dr2.Dispose();
                cmd2.Dispose();
                con.Close();
            }

            if (count >= 4)
            {
                SqlConnection con = new SqlConnection(GetConnectionString());
                SqlCommand cmd = new SqlCommand("select top(1) id,name,description,url from [TblLinksUrja] order by id desc", con);
                con.Open();
                cmd.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dr.Read();
                    lbllink4.Text = dr["description"].ToString();
                    urllink4.Text = dr["url"].ToString();
                }
                dr.Dispose();
                cmd.Dispose();
                SqlCommand cmd1 = new SqlCommand("SELECT name,Description,Url FROM [TblLinksUrja] WHERE Id NOT IN (SELECT MAX( Id )FROM [TblLinksUrja] ) order by id desc", con);

                cmd1.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr1 = cmd1.ExecuteReader();
                if (dr1.HasRows)
                {
                    dr1.Read();
                    lbllink3.Text = dr1["description"].ToString();
                    urllink3.Text = dr1["url"].ToString();
                }
                dr1.Dispose();
                cmd1.Dispose();

                SqlCommand cmd2 = new SqlCommand("SELECT TOP 1 * From(select Top 3 * from [TblLinksUrja] ORDER BY Id DESC) x ORDER BY Id", con);

                cmd2.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr2 = cmd2.ExecuteReader();
                if (dr2.HasRows)
                {
                    dr2.Read();
                    lbllink2.Text = dr2["description"].ToString();
                    urllink2.Text = dr2["url"].ToString();
                }
                dr2.Dispose();
                cmd2.Dispose();

                SqlCommand cmd4 = new SqlCommand("SELECT TOP 1 * From(select Top 4 * from [TblLinksUrja] ORDER BY Id DESC) x ORDER BY Id", con);

                cmd4.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr4 = cmd4.ExecuteReader();
                if (dr4.HasRows)
                {
                    dr4.Read();
                    lbllink1.Text = dr4["description"].ToString();
                    urllink1.Text = dr4["url"].ToString();
                }
                dr4.Dispose();
                cmd4.Dispose();
                con.Close();

            }
            con1.Close();
        }
        public void getNotification()
        {
            SqlConnection con1 = new SqlConnection(GetConnectionString());
            SqlCommand cmdcount = new SqlCommand("select count(*) from tblnotificationurja", con1);
            con1.Open();
            int countnotification = (int)cmdcount.ExecuteScalar();
            lblnotificationcount.Text = countnotification.ToString();
            if (countnotification == 1)
            {
                SqlConnection con = new SqlConnection(GetConnectionString());
                SqlCommand cmd = new SqlCommand("select top(1) notification from [tblnotificationurja] order by id desc", con);
                con.Open();
                cmd.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dr.Read();
                    notification1.Text = dr["notification"].ToString();
                    lblnotification.Text = dr["notification"].ToString();
                }

                dr.Dispose();
                cmd.Dispose();
                con.Close();
            }

            if (countnotification == 2)
            {
                SqlConnection con = new SqlConnection(GetConnectionString());
                SqlCommand cmd = new SqlCommand("select top(1) notification from [tblnotificationurja] order by id desc", con);
                con.Open();
                cmd.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dr.Read();
                    notification1.Text = dr["notification"].ToString();
                    lblnotification.Text = dr["notification"].ToString();
                }

                dr.Dispose();
                cmd.Dispose();

                SqlCommand cmd1 = new SqlCommand("SELECT id, notification FROM [tblnotificationurja] WHERE Id NOT IN (SELECT MAX( Id )FROM [tblnotificationurja] ) order by id desc", con);

                cmd1.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr1 = cmd1.ExecuteReader();
                if (dr1.HasRows)
                {
                    dr1.Read();

                    notification2.Text = dr1["notification"].ToString();
                }
                dr1.Dispose();
                cmd1.Dispose();
                con.Close();

            }
            if (countnotification >= 3)
            {
                SqlConnection con = new SqlConnection(GetConnectionString());
                SqlCommand cmd = new SqlCommand("select top(1) notification from [tblnotificationurja] order by id desc", con);
                con.Open();
                cmd.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dr.Read();
                    notification1.Text = dr["notification"].ToString();
                    lblnotification.Text = dr["notification"].ToString();
                }

                dr.Dispose();
                cmd.Dispose();

                SqlCommand cmd1 = new SqlCommand("SELECT id, notification FROM [tblnotificationurja] WHERE Id NOT IN (SELECT MAX( Id )FROM [tblnotificationurja] ) order by id desc", con);

                cmd1.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr1 = cmd1.ExecuteReader();
                if (dr1.HasRows)
                {
                    dr1.Read();

                    notification2.Text = dr1["notification"].ToString();
                }
                dr1.Dispose();
                cmd1.Dispose();

                SqlCommand cmd2 = new SqlCommand("SELECT TOP 1 * From(select Top 3 * from [tblnotificationurja] ORDER BY Id DESC) x ORDER BY Id", con);

                cmd2.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr2 = cmd2.ExecuteReader();
                if (dr2.HasRows)
                {
                    dr2.Read();
                    notification3.Text = dr2["notification"].ToString();
                }
                dr2.Dispose();
                cmd2.Dispose();
                con.Close();
            }
            con1.Dispose();
            cmdcount.Dispose();

        }
        public void BindData()
        {
            SqlConnection con = new SqlConnection(GetConnectionString());
            SqlCommand cmd = new SqlCommand("select name,emailid,phone from tblLogin where emailid=@emailid or phone=@phone", con);
            con.Open();
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.Parameters.AddWithValue("@emailid", Session["username"].ToString());
            cmd.Parameters.AddWithValue("@phone", Session["username"].ToString());
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                dr.Read();
                name.Value = dr["name"].ToString();
                email.Value = dr["emailid"].ToString();
                phone.Value = dr["phone"].ToString();
                
            }
            dr.Dispose();
            cmd.Dispose();
            con.Close();
        }
        public string GetConnectionString()
        {
            return System.Configuration.ConfigurationManager.ConnectionStrings["Local"].ConnectionString;
        }

        protected void btnsave_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(GetConnectionString());
            SqlCommand cmd = new SqlCommand("update tblLogin set name=@name,emailid=@emailid,phone=@phone where emailid=@emailid1 or phone=@phone1", con);
            con.Open();
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.Parameters.AddWithValue("@emailid1", Session["username"].ToString());
            cmd.Parameters.AddWithValue("@phone1", Session["username"].ToString());
            cmd.Parameters.AddWithValue("@name", name.Value.ToString());
            cmd.Parameters.AddWithValue("@emailid", email.Value.ToString());
            cmd.Parameters.AddWithValue("@phone", phone.Value.ToString());
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
            lblmessage.Text = "Profile Updated!";
            lblmessage.ForeColor = System.Drawing.Color.Green;
            
        }
    }
}