﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
namespace SatlujWebApp
{
    public partial class Project : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["username"].ToString() == null || Session["username"].ToString() == "")
            {
                Response.Redirect("Login.aspx");
            }
            if (!Page.IsPostBack)
            {
                BindData();
                getNotification();
                SqlConnection con1 = new SqlConnection(GetConnectionString());
                SqlCommand cmdcount = new SqlCommand("select count(*) from tblnotificationurja", con1);
                con1.Open();
                int countnotification = (int)cmdcount.ExecuteScalar();
                lblnotificationcount.Text = countnotification.ToString();
                cmdcount.Dispose();
            }
        }
        public void getNotification()
        {
            SqlConnection con1 = new SqlConnection(GetConnectionString());
            SqlCommand cmdcount = new SqlCommand("select count(*) from tblnotificationurja", con1);
            con1.Open();
            int countnotification = (int)cmdcount.ExecuteScalar();
            lblnotificationcount.Text = countnotification.ToString();
            if (countnotification == 1)
            {
                SqlConnection con = new SqlConnection(GetConnectionString());
                SqlCommand cmd = new SqlCommand("select top(1) notification from [tblnotificationurja] order by id desc", con);
                con.Open();
                cmd.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dr.Read();
                    notification1.Text = dr["notification"].ToString();
                    lblnotification.Text = dr["notification"].ToString();
                }

                dr.Dispose();
                cmd.Dispose();
                con.Close();
            }

            if (countnotification == 2)
            {
                SqlConnection con = new SqlConnection(GetConnectionString());
                SqlCommand cmd = new SqlCommand("select top(1) notification from [tblnotificationurja] order by id desc", con);
                con.Open();
                cmd.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dr.Read();
                    notification1.Text = dr["notification"].ToString();
                    lblnotification.Text = dr["notification"].ToString();
                }

                dr.Dispose();
                cmd.Dispose();

                SqlCommand cmd1 = new SqlCommand("SELECT id, notification FROM [tblnotificationurja] WHERE Id NOT IN (SELECT MAX( Id )FROM [tblnotificationurja] ) order by id desc", con);

                cmd1.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr1 = cmd1.ExecuteReader();
                if (dr1.HasRows)
                {
                    dr1.Read();

                    notification2.Text = dr1["notification"].ToString();
                }
                dr1.Dispose();
                cmd1.Dispose();


            }
            if (countnotification >= 3)
            {
                SqlConnection con = new SqlConnection(GetConnectionString());
                SqlCommand cmd = new SqlCommand("select top(1) notification from [tblnotificationurja] order by id desc", con);
                con.Open();
                cmd.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dr.Read();
                    notification1.Text = dr["notification"].ToString();
                    lblnotification.Text = dr["notification"].ToString();
                }

                dr.Dispose();
                cmd.Dispose();

                SqlCommand cmd1 = new SqlCommand("SELECT id, notification FROM [tblnotificationurja] WHERE Id NOT IN (SELECT MAX( Id )FROM [tblnotificationurja] ) order by id desc", con);

                cmd1.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr1 = cmd1.ExecuteReader();
                if (dr1.HasRows)
                {
                    dr1.Read();

                    notification2.Text = dr1["notification"].ToString();
                }
                dr1.Dispose();
                cmd1.Dispose();

                SqlCommand cmd2 = new SqlCommand("SELECT TOP 1 * From(select Top 3 * from [tblnotificationurja] ORDER BY Id DESC) x ORDER BY Id", con);

                cmd2.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr2 = cmd2.ExecuteReader();
                if (dr2.HasRows)
                {
                    dr2.Read();
                    notification3.Text = dr2["notification"].ToString();
                }
                dr2.Dispose();
                cmd2.Dispose();
            }

            cmdcount.Dispose();

        }
        public void BindData()
        {
            SqlConnection con = new SqlConnection(GetConnectionString());
            con.Open();
            SqlCommand cmd = new SqlCommand("select * from tblLocationUrja order by id desc", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            grdfeedback.DataSource = ds;
            grdfeedback.DataBind();
            cmd.Dispose();
        }
        public string GetConnectionString()
        {
            return System.Configuration.ConfigurationManager.ConnectionStrings["Local"].ConnectionString;
        }

        protected void grdfeedback_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            grdfeedback.EditIndex = -1;
            BindData();
        }

      

        protected void grdfeedback_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            Label id = grdfeedback.Rows[e.RowIndex].FindControl("lblid") as Label;
            TextBox txtprojectname = grdfeedback.Rows[e.RowIndex].FindControl("txtprojectname") as TextBox;
            TextBox txtstate = grdfeedback.Rows[e.RowIndex].FindControl("txtstate") as TextBox;
            TextBox txtcity = grdfeedback.Rows[e.RowIndex].FindControl("txtcity") as TextBox;
            TextBox txtunit = grdfeedback.Rows[e.RowIndex].FindControl("txtunit") as TextBox;
            TextBox txtdescription = grdfeedback.Rows[e.RowIndex].FindControl("txtdescription") as TextBox;


            SqlConnection con = new SqlConnection(GetConnectionString());
            con.Open();
            //updating the record  
            SqlCommand cmd = new SqlCommand("Update tblLocationUrja set ProjectName='" + txtprojectname.Text + "',State='" + txtstate.Text + "',City='" + txtcity.Text + "',Unit='" + txtunit.Text + "',Description='" + txtdescription.Text + "' where ID=" + Convert.ToInt32(id.Text), con);
            cmd.ExecuteNonQuery();
            con.Close();
            //Setting the EditIndex property to -1 to cancel the Edit mode in Gridview  
            grdfeedback.EditIndex = -1;
            //Call ShowData method for displaying updated data  
            BindData();
        }

        protected void grdfeedback_RowEditing(object sender, GridViewEditEventArgs e)
        {
            grdfeedback.EditIndex = e.NewEditIndex;
            BindData();
        }

        protected void btnsave_Click(object sender, EventArgs e)
        {
            string filena = "";
           
            if (FileUpload1.HasFile)
            {
                 filena = Path.GetFileName(FileUpload1.PostedFile.FileName);


                FileUpload1.SaveAs(Server.MapPath("images/" + filena));

            }
            SqlConnection con = new SqlConnection(GetConnectionString());
            SqlCommand cmd = new SqlCommand("insert into tblLocationUrja(projectname,state,city,unit,description,FileName,FilePath) values(@name,@state,@city,@unit,@description,@FileName,@filePath)", con);
            cmd.CommandType = System.Data.CommandType.Text;
            con.Open();
            cmd.Parameters.AddWithValue("@name", Request.Form["locationname"].ToString());
            cmd.Parameters.AddWithValue("@state", Request.Form["state"].ToString());
            cmd.Parameters.AddWithValue("@city", Request.Form["city"].ToString());
            cmd.Parameters.AddWithValue("@unit", Request.Form["unit"].ToString());
            cmd.Parameters.AddWithValue("@description", Request.Form["textarea"].ToString());
            cmd.Parameters.AddWithValue("@FileName", filena);
            cmd.Parameters.AddWithValue("@FilePath", "images/" + filena);

           

            cmd.ExecuteNonQuery();
            cmd.Dispose();
           
            lblmessage.Text = "Record Inserted";
            lblmessage.ForeColor = System.Drawing.Color.Green;
           // btnsave.Enabled = false;
            BindData();
        }
       
       
    }
}