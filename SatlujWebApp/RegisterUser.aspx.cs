﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Security.Cryptography;
using System.Text;
using System.IO;

namespace SatlujWebApp
{
    public partial class RegisterUser : System.Web.UI.Page
    {
        string clearText = "";
        protected void Page_Load(object sender, EventArgs e)
        {
           
        }
        public string GetConnectionString()
        {
            return System.Configuration.ConfigurationManager.ConnectionStrings["Local"].ConnectionString;
        }

        protected void btnsignin_Click(object sender, EventArgs e)
        {
            Session["username"] = username.Value.ToString();
            SqlConnection con = new SqlConnection(GetConnectionString());
            SqlCommand cmd = new SqlCommand("insert into tbllogin values(@email,@phone,@password,@roleid,@name,@location)", con);
            cmd.CommandType = CommandType.Text;
            con.Open();
            Checkauser();
            if (lblmessage.Text == "1")
            {
                lblmessage.Text = "User Already Exists!";
                lblmessage.ForeColor = System.Drawing.Color.Red;
            }
            else
            {
                //string EncryptionKey = "MAKV2SPBNI99212";
                //byte[] clearBytes = Encoding.Unicode.GetBytes(password.Value.ToString());
                //using (Aes encryptor = Aes.Create())
                //{
                //    Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                //    encryptor.Key = pdb.GetBytes(32);
                //    encryptor.IV = pdb.GetBytes(16);
                //    using (MemoryStream ms = new MemoryStream())
                //    {
                //        using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                //        {
                //            cs.Write(clearBytes, 0, clearBytes.Length);
                //            cs.Close();
                //        }
                //        clearText = Convert.ToBase64String(ms.ToArray());
                //    }
                //}
                byte[] bytes = Encoding.Unicode.GetBytes(password.Value.ToString());
                byte[] inArray = HashAlgorithm.Create("SHA1").ComputeHash(bytes);
                string clearText = Convert.ToBase64String(inArray);

                cmd.Parameters.AddWithValue("@email", username.Value.ToString());
                cmd.Parameters.AddWithValue("@phone", phone.Value.ToString());
                cmd.Parameters.AddWithValue("@password", clearText.ToString());
                cmd.Parameters.AddWithValue("@roleid", 4);
                cmd.Parameters.AddWithValue("@name", "User");
                cmd.Parameters.AddWithValue("@location", "Urja");
                cmd.ExecuteNonQuery();
                cmd.Dispose();
                con.Close();
                lblmessage.Text = "Registered Successfully!";
                lblmessage.ForeColor = System.Drawing.Color.Green;
                username.Value = "";
                phone.Value = "";
                Response.Redirect("Login.aspx");
            }
        }
        public void Checkauser()
        {
            SqlConnection con = new SqlConnection(GetConnectionString());
            con.Open();
            SqlCommand cmd = new SqlCommand("select * from tbllogin where emailid=@email or phone=@phone and Location='Urja' ", con);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@email", Session["username"].ToString());
            cmd.Parameters.AddWithValue("@phone", Session["username"].ToString());
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                lblmessage.Text = "1";
               
                return;
            }

        }

    }
}