﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SatlujWebApp
{
    public partial class ViewAdmin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Username"].ToString() == "" || Session["Username"].ToString()==null)
            {
                Response.Redirect("Login.aspx");
            }
            if(!Page.IsPostBack)
            {
                BindData();
                getNotification();
                SqlConnection con1 = new SqlConnection(GetConnectionString());
                SqlCommand cmdcount = new SqlCommand("select count(*) from tblnotificationurja", con1);
                con1.Open();
                int countnotification = (int)cmdcount.ExecuteScalar();
                lblnotificationcount.Text = countnotification.ToString();
                cmdcount.Dispose();
                con1.Close();
            }
        }
        public void getNotification()
        {
            SqlConnection con1 = new SqlConnection(GetConnectionString());
            SqlCommand cmdcount = new SqlCommand("select count(*) from tblnotificationurja", con1);
            con1.Open();
            int countnotification = (int)cmdcount.ExecuteScalar();
            lblnotificationcount.Text = countnotification.ToString();
            if (countnotification == 1)
            {
                SqlConnection con = new SqlConnection(GetConnectionString());
                SqlCommand cmd = new SqlCommand("select top(1) notification from [tblnotificationurja] order by id desc", con);
                con.Open();
                cmd.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dr.Read();
                    notification1.Text = dr["notification"].ToString();
                    lblnotification.Text = dr["notification"].ToString();
                }

                dr.Dispose();
                cmd.Dispose();
                con.Close();
            }

            if (countnotification == 2)
            {
                SqlConnection con = new SqlConnection(GetConnectionString());
                SqlCommand cmd = new SqlCommand("select top(1) notification from [tblnotificationurja] order by id desc", con);
                con.Open();
                cmd.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dr.Read();
                    notification1.Text = dr["notification"].ToString();
                    lblnotification.Text = dr["notification"].ToString();
                }

                dr.Dispose();
                cmd.Dispose();

                SqlCommand cmd1 = new SqlCommand("SELECT id, notification FROM [tblnotificationurja] WHERE Id NOT IN (SELECT MAX( Id )FROM [tblnotificationurja] ) order by id desc", con);

                cmd1.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr1 = cmd1.ExecuteReader();
                if (dr1.HasRows)
                {
                    dr1.Read();

                    notification2.Text = dr1["notification"].ToString();
                }
                dr1.Dispose();
                cmd1.Dispose();
                con.Close();

            }
            if (countnotification >= 3)
            {
                SqlConnection con = new SqlConnection(GetConnectionString());
                SqlCommand cmd = new SqlCommand("select top(1) notification from [tblnotificationurja] order by id desc", con);
                con.Open();
                cmd.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dr.Read();
                    notification1.Text = dr["notification"].ToString();
                    lblnotification.Text = dr["notification"].ToString();
                }

                dr.Dispose();
                cmd.Dispose();

                SqlCommand cmd1 = new SqlCommand("SELECT id, notification FROM [tblnotificationurja] WHERE Id NOT IN (SELECT MAX( Id )FROM [tblnotificationurja] ) order by id desc", con);

                cmd1.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr1 = cmd1.ExecuteReader();
                if (dr1.HasRows)
                {
                    dr1.Read();

                    notification2.Text = dr1["notification"].ToString();
                }
                dr1.Dispose();
                cmd1.Dispose();

                SqlCommand cmd2 = new SqlCommand("SELECT TOP 1 * From(select Top 3 * from [tblnotificationurja] ORDER BY Id DESC) x ORDER BY Id", con);

                cmd2.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr2 = cmd2.ExecuteReader();
                if (dr2.HasRows)
                {
                    dr2.Read();
                    notification3.Text = dr2["notification"].ToString();
                }
                dr2.Dispose();
                cmd2.Dispose();
                con.Close();
            }
            con1.Close();
            cmdcount.Dispose();

        }
      
        protected void grdadmin_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            Label id = grdadmin.Rows[e.RowIndex].FindControl("lblid") as Label;
            TextBox Name = grdadmin.Rows[e.RowIndex].FindControl("txtname") as TextBox;
            TextBox Email = grdadmin.Rows[e.RowIndex].FindControl("txtemail") as TextBox;
            TextBox Phone = grdadmin.Rows[e.RowIndex].FindControl("txtnumber") as TextBox;
            SqlConnection con = new SqlConnection(GetConnectionString());
            con.Open();
            //updating the record  
            SqlCommand cmd = new SqlCommand("Update tblLogin set Name='" + Name.Text + "',EmailId='" + Email.Text + "',Phone='" + Phone.Text + "' where ID=" + Convert.ToInt32(id.Text), con);
            cmd.ExecuteNonQuery();
            con.Close();
            //Setting the EditIndex property to -1 to cancel the Edit mode in Gridview  
            grdadmin.EditIndex = -1;
            con.Close();
            //Call ShowData method for displaying updated data  
            BindData();  
        }

        protected void grdadmin_RowEditing(object sender, GridViewEditEventArgs e)
        {
            grdadmin.EditIndex = e.NewEditIndex;
            BindData();
        }
      
       
        public void BindData()
        {
            SqlCommand cmd = new SqlCommand();
            SqlConnection con = new SqlConnection(GetConnectionString());
            con.Open();
            if (Convert.ToInt32(Session["RoleId"]) == 1)
            {
                 cmd.CommandText = "select * from tblLogin where  RoleId!=1 order by id desc";
                 cmd.Connection = con;
            }
            else
            {
                 cmd.CommandText = "select * from tblLogin where  RoleId=3 order by id desc";
                 cmd.Connection = con;
            }
                SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            grdadmin.DataSource = ds;
            grdadmin.DataBind();
            con.Close();

        }
        public string GetConnectionString()
        {
            return System.Configuration.ConfigurationManager.ConnectionStrings["Local"].ConnectionString;
        }

        protected void grdadmin_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            grdadmin.EditIndex = -1;
            BindData();  
        }

        protected void grdadmin_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            Label id = grdadmin.Rows[e.RowIndex].FindControl("lblid") as Label;
            SqlConnection con = new SqlConnection(GetConnectionString());
            con.Open();
            //updating the record  
            SqlCommand cmd = new SqlCommand("delete from tblLogin where ID=" + Convert.ToInt32(id.Text), con);
            cmd.ExecuteNonQuery();
            con.Close();
            //Setting the EditIndex property to -1 to cancel the Edit mode in Gridview  
         
            con.Close();
            //Call ShowData method for displaying updated data  
            BindData();  
        }

        protected void grdadmin_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            Label lblUserID = (Label)e.Row.FindControl("lbltype");
            Label type = (Label)e.Row.FindControl("Label4");
            if (lblUserID != null)
            {
                if (lblUserID.Text == "2")
                {
                    type.Text = "Admin";
                }
                else if (lblUserID.Text == "3")
                {
                    type.Text = "Employee";
                }
                else
                {
                    type.Text = "User";
                }
            }
        }

       
    }
}