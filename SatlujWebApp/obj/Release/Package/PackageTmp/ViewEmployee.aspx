﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewEmployee.aspx.cs" Inherits="SatlujWebApp.ViewEmployee" %>

<!DOCTYPE html>

<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    <title>View Employee</title>
    <link type="text/css" rel="stylesheet" href="css/font-awesome.css">
    <link type="text/css" rel="stylesheet" href="css/material-design-iconic-font.css">
    <link type="text/css" rel="stylesheet" href="css/bootstrap.css">
    <link type="text/css" rel="stylesheet" href="css/animate.css">
    <link type="text/css" rel="stylesheet" href="css/layout.css">
    <link type="text/css" rel="stylesheet" href="css/components.css">
    <link type="text/css" rel="stylesheet" href="css/widgets.css">
    <link type="text/css" rel="stylesheet" href="css/plugins.css">
    <link type="text/css" rel="stylesheet" href="css/pages.css">
    <link type="text/css" rel="stylesheet" href="css/bootstrap-extend.css">
    <link type="text/css" rel="stylesheet" href="css/common.css">
    <link type="text/css" rel="stylesheet" href="css/responsive.css">
</head>
    <form id="form1" runat="server">
<body class="leftbar-view">
<!--Topbar Start Here-->
<header class="topbar clearfix">
    <!--Top Search Bar Start Here-->
    <div class="top-search-bar">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="search-input-addon">
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Top Search Bar End Here-->
    <!--Topbar Left Branding With Logo Start-->
    <div class="topbar-left pull-left">
        <div class="clearfix">
            <ul class="left-branding pull-left clickablemenu ttmenu dark-style menu-color-gradient">
                <li><span class="left-toggle-switch"><i class="fa fa-bars" aria-hidden="true"></i></span></li>
                <li>
                    <div class="logo">
                   <h3><img src="images/logo.png" ></h3>
                       <!--  <a href="index.html" title="Admin Template"><img src="images/logo.png" alt="logo"></a> -->
                    </div>
                </li>
            </ul>
            <!--Mobile Search and Rightbar Toggle-->
            <ul class="branding-right pull-right">
                <li><a href="#" class="btn-mobile-search btn-top-search"><i class="zmdi zmdi-search"></i></a></li>
                <li><a href="#" class="btn-mobile-bar"><i class="fa fa-bars" aria-hidden="true"></i></a></li>
            </ul>
        </div>
    </div>
    <!--Topbar Left Branding With Logo End-->
    <!--Topbar Right Start-->
    <h3 class="app-name">SJVN Utpadan</h3><div class="topbar-right pull-right">
        <div class="clearfix">
            <!--Mobile View Leftbar Toggle-->
            <ul class="left-bar-switch pull-left">
                <li><span class="left-toggle-switch"><i class="fa fa-bars" aria-hidden="true"></i></span></li>
            </ul>
            <ul class="pull-right top-right-icons">
              <!--   <li><a href="#" class="btn-top-search"><i class="zmdi zmdi-search"></i></a></li> -->
                <li class="dropdown apps-dropdown">
                    <a href="#" class="btn-apps dropdown-toggle" data-toggle="dropdown"><i class="zmdi zmdi-apps"></i></a>
                    <div class="dropdown-menu">
                        <ul class="apps-shortcut clearfix">
                            <li>
                                <a href="#"><i class="zmdi zmdi-email"></i>
                                    <span class="apps-noty">23</span>
                                    <span class="apps-label">Facebook</span>
                                </a>
                            </li>
                            <li>
                                <a href="#"><i class="zmdi zmdi-accounts-alt"></i>
                                    <span class="apps-noty">15</span>
                                    <span class="apps-label">Twitter</span>
                                </a>
                            </li>
                          
                       
                        </ul>
                     
                    </div>
                </li>
                <li class="dropdown notifications-dropdown">
                                       <a href="#" class="btn-notification dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><%--<span class="noty-bubble"><%= lblnotificationcount.Text %></span>--%><i class="zmdi zmdi-globe"></i></a>
                    <div class="dropdown-menu notifications-tabs">
                        <div>
                            <ul class="nav material-tabs nav-tabs" role="tablist">
                               <!--  <li class="active"><a href="#message" aria-controls="message" role="tab" data-toggle="tab">Message</a></li>
                                <li><a href="#notifications" aria-controls="notifications" role="tab" data-toggle="tab">Notifications --></a></li>
                            </ul> 
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="message">
                                    <div class="message-list-container">
                                        <asp:Label ID="lblnotificationcount" runat="server" Text="" Visible="false"></asp:Label>
                                        <asp:Label ID="lblnotification" runat="server" Text="" Visible="false"></asp:Label>
                                        <%--<h4>You have <%= lblnotificationcount.Text %> notification</h4>--%>
                                         <ul class="clearfix">
                                            <li class="clearfix">
                                                <a href="#" class="message-thumb">
                                                </a><a href="#" class="message-intro"><span class="message-meta"> <asp:Label ID="notification1" runat="server" Text=""></asp:Label> </span>   <span class="message-time"></span></a>
                                            </li>
                                          <li class="clearfix">
                                                <a href="#" class="message-thumb">
                                                </a><a href="#" class="message-intro"><span class="message-meta"><asp:Label ID="notification2" runat="server" Text=""></asp:Label> </span>   <span class="message-time"></span></a>
                                            </li>
											 <li class="clearfix">
                                                <a href="#" class="message-thumb">
                                                </a><a href="#" class="message-intro"><span class="message-meta"><asp:Label ID="notification3" runat="server" Text=""></asp:Label> </span>   <span class="message-time"></span></a>
                                            </li>
                                            
                                            
                                            
                                            
                                        </ul>
                                        <%--<a class="btn btn-link btn-block btn-view-all" href="#"><span>View All</span></a>--%>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </li>
                <li><a href="#" class="right-toggle-switch"><i class="zmdi zmdi-format-align-left"></i><span class="more-noty"></span></a></li>
            </ul>
        </div>
    </div>
    <!--Topbar Right End-->
</header>
<!--Topbar End Here-->
<!--Leftbar Start Here-->
<aside class="leftbar" style="height: 189px;">
    <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: 250px; height: 189px;"><div class="left-aside-container" style="overflow: hidden; width: 250px; height: 189px;">
   <ul class="list-accordion tree-style">
           <% if(Session["RoleId"].ToString()!="4" ||Session["RoleId"].ToString()!="3") { %>
            <li class="list-title"><a href="dashboard.aspx">DashBoard</a></li>
        <% } %> 
        <% else { %>
            <li class="list-title"><a href="dashboardSatluj.aspx">DashBoard</a></li>
        <% } %> 
            <li class="acc-parent-li">
                <a href="#" class="acc-parent"><i class="zmdi zmdi-view-dashboard"></i><span class="list-label">User Adminstrator</span><span class="acc-icon"></span></a>
                <ul style="display: none;"> 
                  
                     <% if(Session["RoleId"].ToString()=="1") { %>
              <li><a href="Create-super-admin.aspx">Add Adminstrator</a></li>
                    <% } %> 
                    <% if(Session["RoleId"].ToString()=="2") { %>
              <li><a href="CreateEmployee.aspx">Add Employee</a></li>
                    <% } %> 
                      <% if (Session["RoleId"].ToString() == "1")
                        { %>
                   <li><a href="ViewAdmin.aspx">View Adminstrator</a></li>
                     <% } %> 
                     <% if (Session["RoleId"].ToString() == "2" )
                        { %>
                   <li><a href="ViewAdmin.aspx">View Employee</a></li>
                     <% } %>  
                    <!-- <li><a href="material-style-leftbar.html">Material Styled Leftbar</a></li> -->
                    <!-- <li><a href="tile-leftbar.html">Tile Leftbar</a></li> -->
                    <!-- <li><a href="with-language-bar.html">With Language Bar</a></li> -->
                    <!-- <li><a href="top-user-thumb.html">Topbar User Thumb</a></li> -->
                    <!-- <li><a href="tabby-leftbar.html">Tabby Leftbar</a></li> -->
                    <!-- <li><a href="notification-rightbar.html">Only Notifications Rightbar</a></li> -->
                    <!-- <li><a href="without-rightbar.html">Without Rightbar</a></li> -->
                    <!-- <li><a href="activities-rightbar.html">Activities Rightbar</a></li> -->
             </ul>
            </li>
            <!-- <li class="list-title">Forms</li> -->
            <li class="acc-parent-li">
                <a href="#" class="acc-parent"><i class="zmdi zmdi-view-web"></i><span class="list-label">User Management</span><span class="acc-icon"></span></a>
                <ul style="display: none;">
                     <% if(Session["RoleId"].ToString()=="1") { %>
               <li><a href="AssignRole.aspx">Assign Role </a></li>
                    <% } %> 
                     <% if(Session["RoleId"].ToString()=="2") { %>
               <li><a href="UserRoles.aspx">Assign Role </a></li>
                    <% } %> 
                    <!-- <li><a href="form-basic-elements.html">View Adminstrator</a></li> -->
                    <!-- <li><a href="form-material-elements.html">Material Form Elements</a></li> -->
                    <!-- <li><a href="form-icons-label.html">All Icons Labels</a></li> -->
                    <!-- <li><a href="form-clone-element.html">Clone Form ELements </a></li> -->
                    <!-- <li><a href="form-autocomplete.html">Autocomplete </a></li> -->
                    <!-- <li><a href="form-currency-format.html">Currency Format</a></li> -->
                    <!-- <li><a href="form-success-state.html">Form Success State </a></li> -->
                    <!-- <li><a href="form-error-state.html">Form Error State </a></li> -->
                    <!-- <li><a href="form-disable-state.html">Form Disable State </a></li> -->
                    <!-- <li><a href="form-grid.html">Form Grid </a></li> -->
                    <!-- <li><a href="form-tooltips.html">Form Tooltips </a></li> -->
                    <!-- <li><a href="tags-input.html">Tags Input</a></li> -->
                    <!-- <li><a href="input-mask.html">Input Mask </a></li> -->
                    <!-- <li><a href="select2.html">Select2</a></li> -->
                    <!-- <li><a href="file-styles.html">File Styles</a></li> -->
                    <!-- <li><a href="spinner.html">Spinner</a></li> -->
                    <!-- <li><a href="icheck.html">iCheck</a></li> -->
                    <!-- <li><a href="form-ui-datepicker.html">UI Datepickers</a></li> -->
                    <!-- <li><a href="form-timepicker.html">UI Timepickers</a></li> -->
                    <!-- <li><a href="form-spectrum-colorpickers.html">Spectrum Colorpickers</a></li> -->
                    <!-- <li><a href="date-pickers.html">Bootstrap Datepickers</a></li> -->
                    <!-- <li><a href="color-pickers.html">Bootstrap &amp; jqColorPickers</a></li> -->
                </ul>
            </li>
            <li class="acc-parent-li">
                <a href="#" class="acc-parent"><i class="zmdi zmdi-check"></i><span class="list-label"> Setting</span><span class="acc-icon"></span></a>
                <ul style="display: none;">
                    <li><a href="ProfileSetting.aspx">Profile </a></li>
                    <li><a href="ResetPassword.aspx">Reset Password </a></li>
                    <!-- <li><a href="form-call-me.html">Call Me Form </a></li> -->
                    <!-- <li><a href="form-call-me-captcha.html">Call Me Cptacha Form </a></li> -->
                    <!-- <li><a href="form-checkout.html">Checkout Form </a></li> -->
                    <!-- <li><a href="form-order-check-radio.html">Order Form </a></li> -->
                    <!-- <li><a href="form-order-field.html">Order Form With Quantity </a></li> -->
                </ul>
            </li>

            <li class="acc-parent-li">
             <% if(Session["RoleId"].ToString()!="1" && Session["RoleId"].ToString()!="4" && Session["RoleId"].ToString()!="3") { %>
              <a href="Project.aspx" class="acc-parent"><i class="zmdi zmdi-check"></i><span class="list-label"> Project</span><span class="acc-icon"></span></a>
                    <% } %> 
               
               
            </li 
              <li class="acc-parent-li">
                  <% if(Session["RoleId"].ToString()!="1" && Session["RoleId"].ToString()!="4" && Session["RoleId"].ToString()!="3") { %>
                      <a href="Link.aspx" class="acc-parent"><i class="zmdi zmdi-check"></i><span class="list-label"> Add Link</span><span class="acc-icon"></span></a>
                    <% } %>
     
               
            </li>
        <li class="acc-parent-li">
            <% if(Session["RoleId"].ToString()!="1"  && Session["RoleId"].ToString()!="3") { %>
             <a href="FeedBack.aspx" class="acc-parent"><i class="zmdi zmdi-check"></i><span class="list-label"> Feed Back</span><span class="acc-icon"></span></a>
                    <% } %>
                
               
            </li>    
                 <li class="acc-parent-li">
            <% if(Session["RoleId"].ToString()!="1" && Session["RoleId"].ToString()!="4" && Session["RoleId"].ToString()!="2") { %>
             <a href="Notification.aspx" class="acc-parent"><i class="zmdi zmdi-check"></i><span class="list-label"> Notification</span><span class="acc-icon"></span></a>
                    <% } %>
                
               
            </li>
            
        </ul>
    </div><div class="slimScrollBar" style="background: rgba(255, 255, 255, 0.5); width: 4px; position: absolute; top: 48px; opacity: 0.4; display: none; border-radius: 0px; z-index: 99; right: 1px; height: 189px;"></div><div class="slimScrollRail" style="width: 4px; height: 100%; position: absolute; top: 0px; border-radius: 0px; background: rgb(34, 34, 34); opacity: 0.3; z-index: 90; right: 1px; display: none;"></div></div>
</aside>
<!--Leftbar End Here-->
<!--Page Container Start Here-->
<section class="main-container">
    <div class="container-fluid">
        <div class="page-header filled full-block light">
            <div class="row">
                <div class="col-md-12">
                     <marquee> <h2></h2>
            <p> <%= lblnotification.Text %></p></marquee>
                    <h2>View Employee</h2>
                    <!-- <p>All kind of form elements here </p> -->
                </div>
            
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="widget-wrap">
                    <div class="widget-header block-header margin-bottom-0 clearfix">
                    
                    </div>
                    <div class="widget-container">
                        <div class="widget-content">
                            <div class="row">
                                <div class="col-md-12">
                                <div class="col-md-12">
<div class="widget-wrap">

<div class="widget-container">
<div class="widget-content">
    <asp:GridView ID="grdadmin" runat="server" AutoGenerateColumns="False" AutoGenerateEditButton="True"  OnRowEditing="grdadmin_RowEditing" OnRowUpdating="grdadmin_RowUpdating" OnRowCancelingEdit="grdadmin_RowCancelingEdit" OnRowDeleting="grdadmin_RowDeleting" AutoGenerateDeleteButton="True" OnRowDataBound="grdadmin_RowDataBound">
        <Columns>
            <asp:TemplateField HeaderText="RoleId" Visible="false">
               
                <ItemTemplate>
                    <asp:Label ID="lbltype" runat="server" Text='<%# Eval("RoleId") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle Font-Bold="True" />
            </asp:TemplateField>
             <asp:TemplateField HeaderText="ID" Visible="false">
               
                <ItemTemplate>
                    <asp:Label ID="lblid" runat="server" Text='<%# Eval("Id") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle Font-Bold="True" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="User Name">
                <EditItemTemplate>
                    <asp:TextBox ID="txtname" runat="server" Text='<%# Eval("Name") %>' ></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("Name") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle Font-Bold="True" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Email">
                <EditItemTemplate>
                    <asp:TextBox ID="txtemail" runat="server" Text='<%# Eval("Emailid") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<%# Eval("Emailid") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle Font-Bold="True" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Mobile">
                <EditItemTemplate>
                    <asp:TextBox ID="txtnumber" runat="server" Text='<%# Eval("phone") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label3" runat="server" Text='<%# Eval("phone") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle Font-Bold="True" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="User Type">
                <ItemTemplate>
                    <asp:Label ID="Label4" runat="server" Text='<%# Eval("RoleId") %>'></asp:Label>
                      
                </ItemTemplate>
                <HeaderStyle Font-Bold="True" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Location">
                <ItemTemplate>
                    <asp:Label ID="lbllocation" runat="server" Text='<%# Eval("Location") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle Font-Bold="True" />
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</div>
</div>
</div>
</div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
             
            </div>
        </div>

    </div>
    <!--Footer Start Here -->
    <footer class="footer-container">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <div class="footer-left">
                        <%--<span>© 2015 <a href="http://themeforest.net/user/westilian">westilian</a></span>--%>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <div class="footer-right">
                        <%--<span class="footer-meta">Crafted with&nbsp;<i class="fa fa-heart"></i>&nbsp;by&nbsp;<a href="http://themeforest.net/user/westilian">westilian</a></span>--%>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!--Footer End Here -->
</section>
<!--Page Container End Here-->
<!--Rightbar Start Here-->
<aside class="rightbar">
<div class="rightbar-container">
<div class="aside-chat-box">
    <div class="coversation-toolbar">
        <div class="chat-back">
            <i class="zmdi zmdi-long-arrow-left"></i>
        </div>
        <div class="active-conversation">
            <div class="chat-avatar">
                <img src="images/avatar/amarkdalen.jpg" alt="user">
            </div>
            <div class="chat-user-status">
                <ul>
                    <li>Feeling Blessed</li>
                    <li>Amarkdalen</li>
                </ul>
            </div>
        </div>
        <div class="conversation-action">
            <ul>
                <li><i class="zmdi zmdi-phone-in-talk"></i></li>
                <li class="dropdown">
                    <a href="#" class="btn-more dropdown-toggle" data-toggle="dropdown"><i class="zmdi zmdi-more-vert"></i></a>
                    <ul class="dropdown-menu">
                        <li><a href="#"><i class="zmdi zmdi-attachment-alt"></i>Attach A File</a></li>
                        <li><a href="#"><i class="zmdi zmdi-mic"></i>Voice</a></li>
                        <li><a href="#"><i class="zmdi zmdi-block"></i>Block User</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <div class="conversation-container">
        <div class="conversation-row even">
            <ul class="conversation-list">
                <li>
                    <p>
                        Hi! this is mike how can I help you?
                    </p>
                </li>
                <li>
                    <p>
                        Hello Sir!
                    </p>
                </li>
            </ul>
        </div>
        <div class="conversation-row odd">
            <ul class="conversation-list">
                <li>
                    <p>
                        Hi! Mike I need a support my account is suspended but I don't know why?
                    </p>
                </li>
            </ul>
        </div>
        <div class="conversation-row even">
            <ul class="conversation-list">
                <li>
                    <p>
                        Ok Sir! Let me check this issue please wait a min
                    </p>
                </li>
            </ul>
        </div>
        <div class="conversation-row odd">
            <ul class="conversation-list">
                <li>
                    <p>
                        Ok sure :)
                    </p>
                </li>
            </ul>
        </div>
    </div>
    <div class="chat-text-input">
        <input type="text" class="form-control">
    </div>
</div>
<ul class="nav nav-tabs material-tabs rightbar-tab" role="tablist">
    <li class="active"><a href="#chat" aria-controls="message" role="tab" data-toggle="tab">Chat</a></li>
    <li><a href="#activities" aria-controls="notifications" role="tab" data-toggle="tab">Activities</a></li>
</ul>
<div class="tab-content">
<div role="tabpanel" class="tab-pane active" id="chat">
    <div class="chat-user-toolbar clearfix">
        <div class="chat-user-search pull-left">
            <span class="addon-icon"><i class="zmdi zmdi-search"></i></span>
            <input type="text" class="form-control" placeholder="Search">
        </div>
        <div class="add-chat-list pull-right">
            <i class="zmdi zmdi-accounts-add"></i>
        </div>
    </div>
    <div class="chat-user-container">
        <h3 class="clearfix"><span class="pull-left">Members</span><span class="pull-right online-counter">3 Online</span></h3>
        <ul class="chat-user-list">
            <li>
                <div data-trigger="hover" title="Robertoortiz" data-content="<div class='chat-user-info'>
                                        <div class='chat-user-avatar'>
                                        <img src='images/avatar/robertoortiz.jpg' alt='Avatar'>
                                        </div>
                                        <div class='chat-user-details'>
                                        <ul>
                                        <li>Status: <span>Online</span></li>
                                        <li>Type: <span>Admin</span></li>
                                        <li>Last Login: <span>3 hours Ago</span></li>
                                        <li></li>
                                        </ul>
                                        </div>
                                        </div>
                                        " data-placement="left"><span class="chat-avatar"><img src="images/avatar/robertoortiz.jpg" alt="Avatar"></span><span class="chat-u-info">Adellecharles<cite>New York</cite></span>
                </div>
                <span class="chat-u-status"><i class="fa fa-circle"></i></span>
            </li>
            <li class="chat-u-online">
                <div data-trigger="hover" title="Kurafire" data-content="<div class='chat-user-info'>
                                        <div class='chat-user-avatar'>
                                        <img src='images/avatar/kurafire.jpg' alt='Avatar'>
                                        </div>
                                        <div class='chat-user-details'>
                                        <ul>
                                        <li>Status: <span>Online</span></li>
                                        <li>Type: <span>Moderator</span></li>
                                        <li>Last Login: <span>3 hours Ago</span></li>
                                        <li></li>
                                        </ul>
                                        </div>
                                        </div>
                                        " data-placement="left"><span class="chat-avatar"><img src="images/avatar/kurafire.jpg" alt="Avatar"></span><span class="chat-u-info">Kurafire<cite>New York</cite></span>
                </div>
                <span class="chat-u-status"><i class="fa fa-circle"></i></span>
            </li>
            <li class="chat-u-away">
                <div data-trigger="hover" title="Mikeluby" data-content="<div class='chat-user-info'>
                                        <div class='chat-user-avatar'>
                                        <img src='images/avatar/mikeluby.jpg' alt='Avatar'>
                                        </div>
                                        <div class='chat-user-details'>
                                        <ul>
                                        <li>Status: <span>Online</span></li>
                                        <li>Type: <span>Moderator</span></li>
                                        <li>Last Login: <span>3 hours Ago</span></li>
                                        <li></li>
                                        </ul>
                                        </div>
                                        </div>
                                        " data-placement="left">
                    <span class="chat-avatar"><img src="images/avatar/mikeluby.jpg" alt="Avatar"></span><span class="chat-u-info">Bobbyjkane<cite>London City</cite></span>
                </div>
                <span class="chat-u-status"><i class="fa fa-circle"></i></span>
            </li>
            <li class="chat-u-busy">
                <div data-trigger="hover" title="Joostvanderree" data-content="<div class='chat-user-info'>
                                        <div class='chat-user-avatar'>
                                        <img src='images/avatar/joostvanderree.jpg' alt='Avatar'>
                                        </div>
                                        <div class='chat-user-details'>
                                        <ul>
                                        <li>Status: <span>Online</span></li>
                                        <li>Type: <span>Moderator</span></li>
                                        <li>Last Login: <span>3 hours Ago</span></li>
                                        <li></li>
                                        </ul>
                                        </div>
                                        </div>
                                        " data-placement="left">
                    <span class="chat-avatar"><img src="images/avatar/joostvanderree.jpg" alt="Avatar"></span><span class="chat-u-info">Joostvanderree<cite>New York</cite></span>
                </div>
                <span class="chat-u-status"><i class="fa fa-circle"></i></span>
            </li>
        </ul>
        <h3 class="clearfix"><span class="pull-left">Guests</span><span class="pull-right online-counter">1 Online</span></h3>
        <ul class="chat-user-list">
            <li>
                <div data-trigger="hover" title="Kevinthompson" data-content="<div class='chat-user-info'>
                                        <div class='chat-user-avatar'>
                                        <img src='images/avatar/Kevinthompson.jpg' alt='Avatar'>
                                        </div>
                                        <div class='chat-user-details'>
                                        <ul>
                                        <li>Status: <span>Online</span></li>
                                        <li>Type: <span>Moderator</span></li>
                                        <li>Last Login: <span>3 hours Ago</span></li>
                                        <li></li>
                                        </ul>
                                        </div>
                                        </div>
                                        " data-placement="left">
                    <span class="chat-avatar"><img src="images/avatar/kevinthompson.jpg" alt="Avatar"></span><span class="chat-u-info">Kevinthompson<cite>Scotland</cite></span>
                </div>
                <span class="chat-u-status"><i class="fa fa-circle"></i></span>
            </li>
            <li class="chat-u-online">
                <div data-trigger="hover" title="Mds" data-content="<div class='chat-user-info'>
                                        <div class='chat-user-avatar'>
                                        <img src='images/avatar/mds.jpg' alt='Avatar'>
                                        </div>
                                        <div class='chat-user-details'>
                                        <ul>
                                        <li>Status: <span>Online</span></li>
                                        <li>Type: <span>Moderator</span></li>
                                        <li>Last Login: <span>3 hours Ago</span></li>
                                        <li></li>
                                        </ul>
                                        </div>
                                        </div>
                                        " data-placement="left">
                    <span class="chat-avatar"><img src="images/avatar/mds.jpg" alt="Avatar"></span><span class="chat-u-info">Mds<cite>South West, England</cite></span>
                </div>
                <span class="chat-u-status"><i class="fa fa-circle"></i></span>
            </li>
            <li>
                <div data-trigger="hover" title="Mko" data-content="<div class='chat-user-info'>
                                        <div class='chat-user-avatar'>
                                        <img src='images/avatar/mko.jpg' alt='Avatar'>
                                        </div>
                                        <div class='chat-user-details'>
                                        <ul>
                                        <li>Status: <span>Online</span></li>
                                        <li>Type: <span>Moderator</span></li>
                                        <li>Last Login: <span>3 hours Ago</span></li>
                                        <li></li>
                                        </ul>
                                        </div>
                                        </div>
                                        " data-placement="left">
                    <span class="chat-avatar"><img src="images/avatar/mko.jpg" alt="Avatar"></span><span class="chat-u-info">Mko<cite>New York</cite></span>
                </div>
                <span class="chat-u-status"><i class="fa fa-circle"></i></span>
            </li>
            <li>
                <div data-trigger="hover" title="Coreyweb" data-content="<div class='chat-user-info'>
                                        <div class='chat-user-avatar'>
                                        <img src='images/avatar/coreyweb.jpg' alt='Avatar'>
                                        </div>
                                        <div class='chat-user-details'>
                                        <ul>
                                        <li>Status: <span>Online</span></li>
                                        <li>Type: <span>Moderator</span></li>
                                        <li>Last Login: <span>3 hours Ago</span></li>
                                        <li></li>
                                        </ul>
                                        </div>
                                        </div>
                                        " data-placement="left">
                    <span class="chat-avatar"><img src="images/avatar/coreyweb.jpg" alt="Avatar"></span><span class="chat-u-info">Coreyweb<cite>Northern Ireland</cite></span>
                </div>
                <span class="chat-u-status"><i class="fa fa-circle"></i></span>
            </li>
            <li>
                <div data-trigger="hover" title="Amarkdalen" data-content="<div class='chat-user-info'>
                                        <div class='chat-user-avatar'>
                                        <img src='images/avatar/amarkdalen.jpg' alt='Avatar'>
                                        </div>
                                        <div class='chat-user-details'>
                                        <ul>
                                        <li>Status: <span>Online</span></li>
                                        <li>Type: <span>Moderator</span></li>
                                        <li>Last Login: <span>3 hours Ago</span></li>
                                        <li></li>
                                        </ul>
                                        </div>
                                        </div>
                                        " data-placement="left">
                    <span class="chat-avatar"><img src="images/avatar/amarkdalen.jpg" alt="Avatar"></span><span class="chat-u-info">Oykun<cite>New York</cite></span>
                </div>
                <span class="chat-u-status"><i class="fa fa-circle"></i></span>
            </li>
        </ul>
    </div>
</div>
<div role="tabpanel" class="tab-pane" id="activities">
    <div class="activities-timeline">
        <h3 class="tab-pane-header">Recent Activities</h3>
        <ul class="activities-list">
            <li>
                <div class="activities-badge">
                    <span class="w_bg_amber"><i class="zmdi zmdi-ticket-star"></i></span>
                </div>
                <div class="activities-details">
                    <h3 class="activities-header"><a href="#">Resolved Tickets #LTK7865</a></h3>
                    <div class="activities-meta">
                        <i class="fa fa-clock-o"></i> 30 min ago
                    </div>
                </div>
            </li>
            <li>
                <div class="activities-badge">
                    <span class="w_bg_cyan"><i class="zmdi zmdi-file-plus"></i></span>
                </div>
                <div class="activities-details">
                    <h3 class="activities-header"><a href="#">Files Uploaded</a></h3>
                    <div class="activities-meta">
                        <i class="fa fa-clock-o"></i> 1 hour ago
                    </div>
                    <div class="activities-post">
                        <ul class="new-file-lists">
                            <li><a href="#"><i class="fa fa-file-text"></i> change-log.txt</a></li>
                            <li><a href="#"><i class="fa fa-file-audio-o"></i> skype-conversation.mp3</a></li>
                            <li><a href="#"><i class="fa fa-file-powerpoint-o"></i> presentation.ppt</a></li>
                            <li><a href="#"><i class="fa fa-file-video-o"></i> howtouse.avi</a></li>
                            <li><a href="#"><i class="fa fa-file-image-o"></i> screenshot.jpg</a></li>
                            <li><a href="#"><i class="fa fa-file-word-o"></i> nda.doc</a></li>
                            <li><a href="#"><i class="fa fa-file-pdf-o"></i> resume.pdf</a></li>
                            <li><a href="#"><i class="fa fa-file-archive-o"></i> all-files.zip</a></li>
                            <li><a href="#"><i class="fa fa-file-excel-o"></i> bill.xls</a></li>
                            <li><a href="#">+10</a></li>
                        </ul>
                    </div>
                </div>
            </li>
            <li>
                <div class="activities-badge">
                    <span class="w_bg_light_blue"><i class="zmdi zmdi-image"></i></span>
                </div>
                <div class="activities-details">
                    <h3 class="activities-header"><a href="#">Images Uploaded</a></h3>
                    <div class="activities-meta">
                        <i class="fa fa-clock-o"></i> July 22 at 1:12pm
                    </div>
                    <div class="activities-post">
                        <ul class="new-image-lists">
                            <li><a href="#"><img src="images/img-1-thumb.jpg" alt="image"></a></li>
                            <li><a href="#"><img src="images/img-2-thumb.jpg" alt="image"></a></li>
                            <li><a href="#"><img src="images/img-3-thumb.jpg" alt="image"></a></li>
                            <li><a href="#" class="more-list"><i class="zmdi zmdi-more-horiz"></i></a></li>
                        </ul>
                    </div>
                </div>
            </li>
            <li>
                <div class="activities-badge">
                    <span class="w_bg_green"><i class="zmdi zmdi-accounts-alt"></i></span>
                </div>
                <div class="activities-details">
                    <h3 class="activities-header"><a href="#">Users Approved</a></h3>
                    <div class="activities-meta">
                        <i class="fa fa-clock-o"></i> July 22 at 1:12pm
                    </div>
                    <div class="activities-post">
                        <ul class="new-user-lists">
                            <li><a href="#"><img src="images/avatar/oykun.jpg" alt="image"></a></li>
                            <li><a href="#"><img src="images/avatar/mds.jpg" alt="image"></a></li>
                            <li><a href="#"><img src="images/avatar/robertoortiz.jpg" alt="image"></a></li>
                            <li><a href="#" class="more-list"><i class="zmdi zmdi-more-horiz"></i></a></li>
                        </ul>
                    </div>
                </div>
            </li>
            <li>
                <div class="activities-badge">
                    <span class="w_bg_deep_purple"><i class="zmdi zmdi-file-text"></i></span>
                </div>
                <div class="activities-details">
                    <h3 class="activities-header"><a href="#">Post New Article</a></h3>
                    <div class="activities-meta">
                        <i class="fa fa-clock-o"></i> July 22 at 1:12pm
                    </div>
                    <div class="activities-post">
                        <ul class="new-post-lists">
                            <li><a href="#">Man in the Verde Valley</a></li>
                            <li><a href="#">Sinagua Pueblo Life</a></li>
                            <li><a href="#">Montezuma Well</a></li>
                            <li><a href="#">The Natural Scene</a></li>
                            <li><a href="#">+6</a></li>
                        </ul>
                    </div>
                </div>
            </li>
            <li>
                <div class="activities-badge">
                    <span class="w_bg_teal"><i class="zmdi zmdi-comments"></i></span>
                </div>
                <div class="activities-details">
                    <h3 class="activities-header"><a href="#">Comments Replied</a></h3>
                    <div class="activities-meta">
                        <i class="fa fa-clock-o"></i> July 22 at 1:12pm
                    </div>
                    <div class="activities-post">
                        <ul class="new-comments-lists">
                            <li><a href="#">As long as you are reasonably careful about where you step and avoid putting ...</a></li>
                            <li><a href="#">Montezuma Castle is 5 miles north of Camp Verde, 60 miles south...</a></li>
                        </ul>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</div>
</div>
</div>
</aside>
<!--Rightbar End Here-->
<script src="js/lib/jquery.js"></script>
<script src="js/lib/jquery-migrate.js"></script>
<script src="js/lib/bootstrap.js"></script>
<script src="js/lib/jquery.ui.js"></script>
<script src="js/lib/jRespond.js"></script>
<script src="js/lib/nav.accordion.js"></script>
<script src="js/lib/hover.intent.js"></script>
<script src="js/lib/hammerjs.js"></script>
<script src="js/lib/jquery.hammer.js"></script>
<script src="js/lib/jquery.fitvids.js"></script>
<script src="js/lib/scrollup.js"></script>
<script src="js/lib/smoothscroll.js"></script>
<script src="js/lib/jquery.slimscroll.js"></script>
<script src="js/lib/jquery.syntaxhighlighter.js"></script>
<script src="js/lib/velocity.js"></script>
<script src="js/lib/smart-resize.js"></script>
<!--Forms-->
<script src="js/lib/jquery.maskedinput.js"></script>
<script src="js/lib/jquery.validate.js"></script>
<script src="js/lib/jquery.form.js"></script>
<script src="js/lib/additional-methods.js"></script>
<script src="js/lib/j-forms.js"></script>
<!--[if lt IE 10]>
<script src="js/lib/jquery.placeholder.min.js"></script>
<![endif]-->
<!--Select2-->
<script src="js/lib/select2.full.js"></script>
<script src="js/lib/jquery.loadmask.js"></script>
<script src="js/apps.js"></script>
</body>
        </form>
</html>
