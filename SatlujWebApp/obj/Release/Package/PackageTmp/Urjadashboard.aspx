﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Employeedashboard.aspx.cs" Inherits="SatlujWebApp.Employeedashboard" %>

<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    <title></title>
    <link type="text/css" rel="stylesheet" href="css/font-awesome.css">
    <link type="text/css" rel="stylesheet" href="css/material-design-iconic-font.css">
    <link type="text/css" rel="stylesheet" href="css/bootstrap.css">
    <link type="text/css" rel="stylesheet" href="css/animate.css">
    <link type="text/css" rel="stylesheet" href="css/layout.css">
    <link type="text/css" rel="stylesheet" href="css/components.css">
    <link type="text/css" rel="stylesheet" href="css/widgets.css">
    <link type="text/css" rel="stylesheet" href="css/plugins.css">
    <link type="text/css" rel="stylesheet" href="css/pages.css">
    <link type="text/css" rel="stylesheet" href="css/bootstrap-extend.css">
    <link type="text/css" rel="stylesheet" href="css/common.css">
    <link type="text/css" rel="stylesheet" href="css/responsive.css">
    <link type="text/css" id="themes" rel="stylesheet" href="">
</head>
    <form runat="server" id="form1">
<body class="leftbar-view">
<!--Topbar Start Here-->
<header class="topbar clearfix">
    <!--Top Search Bar Start Here-->
    <div class="top-search-bar">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="search-input-addon">
                        <span class="addon-icon"><i class="zmdi zmdi-search"></i></span>
                        <input type="text" class="form-control top-search-input" placeholder="Search">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Top Search Bar End Here-->
    <!--Topbar Left Branding With Logo Start-->
    <div class="topbar-left pull-left">
        <div class="clearfix">
            <ul class="left-branding pull-left clickablemenu ttmenu dark-style menu-color-gradient">
                <li><span class="left-toggle-switch"><i class="fa fa-bars" aria-hidden="true"></i></span></li>
                <li>
                    <div class="logo">
                   <h3 style="
    color: #ffffff;
    padding: 15px;
    font-weight: 600;
" class="__web-inspector-hide-shortcut__">SJVN</h3>
                       <!--  <a href="index.html" title="Admin Template"><img src="images/logo.png" alt="logo"></a> -->
                    </div>
                </li>
            </ul>
            <!--Mobile Search and Rightbar Toggle-->
            <ul class="branding-right pull-right">
                <li><a href="#" class="btn-mobile-search btn-top-search"><i class="zmdi zmdi-search"></i></a></li>
                <li><a href="#" class="btn-mobile-bar"><i class="fa fa-bars" aria-hidden="true"></i></a></li>
            </ul>
        </div>
    </div>
    <!--Topbar Left Branding With Logo End-->
    <!--Topbar Right Start-->
    <h3 class="app-name">SJVN Utpadan</h3><div class="topbar-right pull-right">
        <div class="clearfix">
            <!--Mobile View Leftbar Toggle-->
            <ul class="left-bar-switch pull-left">
                <li><span class="left-toggle-switch"><i class="fa fa-bars" aria-hidden="true"></i></span></li>
            </ul>
            <ul class="pull-right top-right-icons">
              <!--   <li><a href="#" class="btn-top-search"><i class="zmdi zmdi-search"></i></a></li> -->
                <li class="dropdown apps-dropdown">
                    <a href="#" class="btn-apps dropdown-toggle" data-toggle="dropdown"><i class="zmdi zmdi-apps"></i></a>
                    <div class="dropdown-menu">
                        <ul class="apps-shortcut clearfix">
                            <li>
                                <a href="#"><i class="zmdi zmdi-email"></i>
                                    <span class="apps-noty">23</span>
                                    <span class="apps-label">Facebook</span>
                                </a>
                            </li>
                            <li>
                                <a href="#"><i class="zmdi zmdi-accounts-alt"></i>
                                    <span class="apps-noty">15</span>
                                    <span class="apps-label">Twitter</span>
                                </a>
                            </li>
                          
                       
                        </ul>
                     
                    </div>
                </li>
                <li class="dropdown notifications-dropdown">
                    <a href="#" class="btn-notification dropdown-toggle" data-toggle="dropdown"><span class="noty-bubble">10</span><i class="zmdi zmdi-globe"></i></a>
                    <div class="dropdown-menu notifications-tabs">
                        <div>
                            <ul class="nav material-tabs nav-tabs" role="tablist">
                               <!--  <li class="active"><a href="#message" aria-controls="message" role="tab" data-toggle="tab">Message</a></li>
                                <li><a href="#notifications" aria-controls="notifications" role="tab" data-toggle="tab">Notifications --></a></li>
                            </ul> 
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="message">
                                    <div class="message-list-container">
                                        <h4>You have 15 notification</h4>
                                        <ul class="clearfix">
                                            <li class="clearfix">
                                                <a href="#" class="message-thumb">
                                                </a><a href="#" class="message-intro"><span class="message-meta">What is Lorem Ipsum? </span>Ipsum   <span class="message-time">today at 8:29 pm</span></a>
                                            </li>
                                          <li class="clearfix">
                                                <a href="#" class="message-thumb">
                                                </a><a href="#" class="message-intro"><span class="message-meta">What is Lorem Ipsum? </span>Ipsum   <span class="message-time">today at 8:29 pm</span></a>
                                            </li>
											 <li class="clearfix">
                                                <a href="#" class="message-thumb">
                                                </a><a href="#" class="message-intro"><span class="message-meta">What is Lorem Ipsum? </span>Ipsum   <span class="message-time">today at 8:29 pm</span></a>
                                            </li>
                                            
                                            
                                            
                                            
                                        </ul>
                                        <a class="btn btn-link btn-block btn-view-all" href="#"><span>View All</span></a>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </li>
                <li><a href="#" class="right-toggle-switch"><i class="zmdi zmdi-format-align-left"></i><span class="more-noty"></span></a></li>
            </ul>
        </div>
    </div>
    <!--Topbar Right End-->
</header>
<!--Topbar End Here-->

<!--Leftbar Start Here-->
<aside class="leftbar" style="height: 189px;">
    <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: 250px; height: 189px;"><div class="left-aside-container" style="overflow: hidden; width: 250px; height: 189px;">
   <ul class="list-accordion tree-style">
            <li class="list-title">Dashboard</li>

            <!-- <li class="list-title">Forms</li> -->
            <% if(Session["RoleId"].ToString()=="3") { %>
			 <li class="acc-parent-li">             
           <a href="Employeedashboard.aspx" class="acc-parent"><i class="zmdi zmdi-view-web"></i><span class="list-label">Enter Data</span></a>
             
            </li>  
          <% } %> 
			 <li class="acc-parent-li">
                <a href="ViewData.aspx" class="acc-parent"><i class="zmdi zmdi-view-web"></i><span class="list-label">View Data</span></a>
                
            </li>  
       <% if(Session["RoleId"].ToString()=="2") { %>
        <li class="acc-parent-li">
                <a href="Link.aspx" class="acc-parent"><i class="zmdi zmdi-view-web"></i><span class="list-label">Link</span></a>
                
            </li> 
       <% } %>  
         <% if(Session["RoleId"].ToString()=="2") { %>
        <li class="acc-parent-li">
                <a href="Feedback.aspx" class="acc-parent"><i class="zmdi zmdi-view-web"></i><span class="list-label">FeedBack</span></a>
                
            </li> 
          <% } %>   
       <% if(Session["RoleId"].ToString()=="3") { %>
			 <li class="acc-parent-li">
                <a href="#" class="acc-parent"><i class="zmdi zmdi-view-web"></i><span class="list-label">Generation Status</span></a>
                
            </li>
        <% } %>    dr
       <% if(Session["RoleId"].ToString()=="2") { %> 
            <li class="acc-parent-li">
                <a href="Notification.aspx" class="acc-parent"><i class="zmdi zmdi-view-web"></i><span class="list-label">Notification</span></a>
                
            </li>  
        <% } %>   
            <li class="acc-parent-li">
                <a href="#" class="acc-parent"><i class="zmdi zmdi-check"></i><span class="list-label"> Setting</span></a>
                <ul style="display: none;">
                    <li><a href="profilesetting.aspx">Profile </a></li>
                    <li><a href="ResetPassword.aspx">Reset Password </a></li>
                    <!-- <li><a href="form-call-me.html">Call Me Form </a></li> -->
                    <!-- <li><a href="form-call-me-captcha.html">Call Me Cptacha Form </a></li> -->
                    <!-- <li><a href="form-checkout.html">Checkout Form </a></li> -->
                    <!-- <li><a href="form-order-check-radio.html">Order Form </a></li> -->
                    <!-- <li><a href="form-order-field.html">Order Form With Quantity </a></li> -->
                </ul>
            </li>

                
                
            
        </ul>
    </div><div class="slimScrollBar" style="background: rgba(255, 255, 255, 0.5); width: 4px; position: absolute; top: 48px; opacity: 0.4; display: none; border-radius: 0px; z-index: 99; right: 1px; height: 189px;"></div><div class="slimScrollRail" style="width: 4px; height: 100%; position: absolute; top: 0px; border-radius: 0px; background: rgb(34, 34, 34); opacity: 0.3; z-index: 90; right: 1px; display: none;"></div></div>
</aside>
<!--Leftbar End Here-->
<!--Page Container Start Here-->
<section class="main-container">
    

<div class="row">
            <div class="col-md-12">
                <div class="widget-wrap">
                    <div class="widget-header block-header margin-bottom-0 clearfix">
                    
                    </div>
                    <div class="widget-container">
                        <div class="widget-content">
                            <div class="row">
                                <div class="col-md-12">
                                <div class="col-md-12">
<div class="widget-wrap">

<div class="widget-container">
       <% if(Session["RoleId"].ToString()=="3") { %> 
<div class="widget-content">
<table class="table table-bordered foo-data-table">
<thead>
<tr>
<th></th>
<th data-hide="all">Daily</th>
<th></th>
<th>Monthly</th>
<th></th>
<th style="
    border-right: none;
"></th>

<th class="td-center">Yearly</th>


</tr>
</thead>
<tbody>
<tr>
<td>Project Name
</td>
<td>MoU Target</td>
<td>Actual Generation(MU)</td>
<td>MoU Target</td>
<td>Actual Generation(MU)</td>
<td>MoU Target</td>
<td>Actual Generation(MU)</td>



</tr>
<tr>
<td style="
    padding: 0;
    margin: 0;
    margin-top: 9px;
    display: block;
"><div>
<!-- <span class="tfh-label">Select Time: </span> -->
<select name="dropdown" id="dropdown1" style="
    width: auto;
" class=" form-control">
	<option value="2">Project 1</option>
	<option value="3">Project 2</option>
	
<option value="5">Project 3</option>
<option value="5">Project 4</option><option value="5">Project 5</option>
</select>
</div>
</td>
<td><input name="khabsatluj" type="text" id="khabsatluj" class="form-control">  </td>
<td><input name="khabspliti" type="text" id="khabspliti" class="form-control"></td>
<td><input name="powari" type="text" id="powari" class="form-control">  </td>
<td><input name="wangtoo" type="text" id="wangtoo" class="form-control">  </td>
<td><input name="dkhabsatluj" type="text" id="dkhabsatluj" class="form-control"></td>
<td><input name="dkhabspiti" type="text" id="dkhabspiti" class="form-control">  </td>




</tr>


</tbody>
</table>
<div class="form-footer">
                                    <!-- <button type="submit" class="btn btn-success primary-btn processing">Proc essing...</button> -->
                                    <!-- <button type="reset" class="btn btn-danger secondary-btn">Reset</button> -->

                                  <asp:Button ID="btnsave" runat="server" Text="Save" class="btn btn-info primary-btn" OnClick="btnsave_Click" />
                                </div>
    <div>
        <asp:Label ID="lblmessage" runat="server" Text=""></asp:Label>
    </div>
</div>
      <% } %>   
</div>
</div>

<div>


   <%-- <h2>ADD Total Button in grid</h2>--%>

    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None" PageSize="15">
        <AlternatingRowStyle BackColor="White" />
        <Columns>
            <asp:BoundField HeaderText="Id" DataField="Id" Visible="false">
            <HeaderStyle Font-Bold="True" />
            </asp:BoundField>
            <asp:BoundField HeaderText="Time" DataField="Time">
            <HeaderStyle Font-Bold="True" />
            </asp:BoundField>
            <asp:BoundField HeaderText="Khab(Satluj)" DataField="SplitPPM1">
            <HeaderStyle Font-Bold="True" />
            </asp:BoundField>
            <asp:BoundField HeaderText="Khab(Spiti)" DataField="SplitPPM2">
            <HeaderStyle Font-Bold="True" />
            </asp:BoundField>
            <asp:BoundField HeaderText="Powari" DataField="SplitPPM3">
            <HeaderStyle Font-Bold="True" />
            </asp:BoundField>
            <asp:BoundField HeaderText="Wangtoo" DataField="SplitPPM4">
            <HeaderStyle Font-Bold="True" />
            </asp:BoundField>
            <asp:BoundField HeaderText="Khab(Satluj)" DataField="DischargeKhab1">
            <HeaderStyle Font-Bold="True" />
            </asp:BoundField>
            <asp:BoundField HeaderText="Khab(Spiti)" DataField="DischargeKhab2">
            <HeaderStyle Font-Bold="True" />
            </asp:BoundField>
            <asp:BoundField HeaderText="Naptha" DataField="DischargeNathpa">
            <HeaderStyle Font-Bold="True" />
            </asp:BoundField>
            <asp:BoundField HeaderText="DateTime" DataField="TimeString">
            <HeaderStyle Font-Bold="True" />
            </asp:BoundField>
        </Columns>
        <EditRowStyle BackColor="#2461BF" />
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#EFF3FB" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#F5F7FB" />
        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
        <SortedDescendingCellStyle BackColor="#E9EBEF" />
        <SortedDescendingHeaderStyle BackColor="#4870BE" />
    </asp:GridView>
</div>


</div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
             
            </div>
        </div>

<!--Footer Start Here -->
<footer class="footer-container">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <div class="footer-left">
                  <%--  <span>© 2015 <a href="">dummy</a></span>--%>
                </div>
            </div>
            <div class="col-md-6 col-sm-6">
                <div class="footer-right">
                    <%--<span class="footer-meta">dfgdfdgdfb &nbsp;<i class="fa fa-heart"></i>&nbsp;by&nbsp;<a href="">gfdgdfg</a></span>--%>
                </div>
            </div>
        </div>
    </div>
</footer>
<!--Footer End Here -->
</section>
<!--Page Container End Here-->
<!--Rightbar Start Here-->
<aside class="rightbar">
<div class="user-profile-container">
            <div class="user-profile clearfix">
                <div class="admin-user-thumb">
                    <img src="images/Dummy.jpg" alt="admin">
                </div>
                <div class="admin-user-info">
                    <ul>
                        <li><a href="#"> Super Admin Panel</a></li>
                        <li><a href="#">dummy@abc.com</a></li>
                    </ul>
                </div>
            </div>
            <div class="admin-bar">
                <ul>
                    <li><a href="#"><i class="zmdi zmdi-power"></i>
                    </a>
                    </li>
                  
                    <li><a href="#"><i class="zmdi zmdi-settings"></i>
                    </a>
                    </li>
                </ul>
            </div>
        </div>
</aside>
<!--Rightbar End Here-->
<script src="js/lib/jquery.js"></script>
<script src="js/lib/jquery-migrate.js"></script>
<script src="js/lib/bootstrap.js"></script>
<script src="js/lib/jquery.ui.js"></script>
<script src="js/lib/jRespond.js"></script>
<script src="js/lib/nav.accordion.js"></script>
<script src="js/lib/hover.intent.js"></script>
<script src="js/lib/hammerjs.js"></script>
<script src="js/lib/jquery.hammer.js"></script>
<script src="js/lib/jquery.fitvids.js"></script>
<script src="js/lib/scrollup.js"></script>
<script src="js/lib/smoothscroll.js"></script>
<script src="js/lib/jquery.slimscroll.js"></script>
<script src="js/lib/jquery.syntaxhighlighter.js"></script>
<script src="js/lib/velocity.js"></script>
<script src="js/lib/jquery-jvectormap.js"></script>
<script src="js/lib/jquery-jvectormap-world-mill.js"></script>
<script src="js/lib/jquery-jvectormap-us-aea.js"></script>
<script src="js/lib/smart-resize.js"></script>
<!--iCheck-->
<script src="js/lib/icheck.js"></script>
<script src="js/lib/jquery.switch.button.js"></script>
<!--CHARTS-->
<script src="js/lib/chart/sparkline/jquery.sparkline.js"></script>
<script src="js/lib/chart/easypie/jquery.easypiechart.min.js"></script>
<script src="js/lib/chart/flot/excanvas.min.js"></script>
<script src="js/lib/chart/flot/jquery.flot.min.js"></script>
<script src="js/lib/chart/flot/curvedLines.js"></script>
<script src="js/lib/chart/flot/jquery.flot.time.min.js"></script>
<script src="js/lib/chart/flot/jquery.flot.stack.min.js"></script>
<script src="js/lib/chart/flot/jquery.flot.axislabels.js"></script>
<script src="js/lib/chart/flot/jquery.flot.resize.min.js"></script>
<script src="js/lib/chart/flot/jquery.flot.tooltip.min.js"></script>
<script src="js/lib/chart/flot/jquery.flot.spline.js"></script>
<script src="js/lib/chart/flot/jquery.flot.pie.min.js"></script>
<!--Forms-->
<script src="js/lib/jquery.maskedinput.js"></script>
<script src="js/lib/jquery.validate.js"></script>
<script src="js/lib/jquery.form.js"></script>
<script src="js/lib/j-forms.js"></script>
<script src="js/lib/jquery.loadmask.js"></script>
<script src="js/lib/vmap.init.js"></script>
<script src="js/lib/theme-switcher.js"></script>
<script src="js/apps.js"></script>
</body>
    </form>
</html>