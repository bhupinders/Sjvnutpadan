﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Utility.aspx.cs" Inherits="SatlujWebApp.Utility" %>

<!DOCTYPE html>

<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    <title>Generation Status</title>
    <link type="text/css" rel="stylesheet" href="css/font-awesome.css">
    <link type="text/css" rel="stylesheet" href="css/material-design-iconic-font.css">
    <link type="text/css" rel="stylesheet" href="css/bootstrap.css">
    <link type="text/css" rel="stylesheet" href="css/animate.css">
    <link type="text/css" rel="stylesheet" href="css/layout.css">
    <link type="text/css" rel="stylesheet" href="css/components.css">
    <link type="text/css" rel="stylesheet" href="css/widgets.css">
    <link type="text/css" rel="stylesheet" href="css/plugins.css">
    <link type="text/css" rel="stylesheet" href="css/pages.css">
    <link type="text/css" rel="stylesheet" href="css/bootstrap-extend.css">
    <link type="text/css" rel="stylesheet" href="css/common.css">
    <link type="text/css" rel="stylesheet" href="css/responsive.css">
    
   
</head>
   
    <form id="form2" runat="server">
<body class="leftbar-view">
<!--Topbar Start Here-->
<header class="topbar clearfix">
    <!--Top Search Bar Start Here-->
    <div class="top-search-bar">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="search-input-addon">
                        <span class="addon-icon"><i class="zmdi zmdi-search"></i></span>
                        <input type="text" class="form-control top-search-input" placeholder="Search">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Top Search Bar End Here-->
    <!--Topbar Left Branding With Logo Start-->
    <div class="topbar-left pull-left">
        <div class="clearfix">
            <ul class="left-branding pull-left clickablemenu ttmenu dark-style menu-color-gradient">
                <li><span class="left-toggle-switch" style="touch-action: pan-y; user-select: none; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"><i class="fa fa-bars" aria-hidden="true"></i></span></li>
                <li>
                    <div class="logo">
                   <h3><img src="images/logo.png" ></h3>
                       <!--  <a href="index.html" title="Admin Template"><img src="images/logo.png" alt="logo"></a> -->
                    </div>
                </li>
            </ul>
            <!--Mobile Search and Rightbar Toggle-->
            <ul class="branding-right pull-right">
                <li><a href="#" class="btn-mobile-search btn-top-search" style="touch-action: pan-y; user-select: none; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"><i class="zmdi zmdi-search"></i></a></li>
                <li><a href="#" class="btn-mobile-bar" style="touch-action: pan-y; user-select: none; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"><i class="fa fa-bars" aria-hidden="true"></i></a></li>
            </ul>
        </div>
    </div>
    <!--Topbar Left Branding With Logo End-->
    <!--Topbar Right Start-->
    
    <h3 class="app-name">SJVN Utpadan</h3><div class="topbar-right pull-right">
        
        <div class="clearfix">
            <!--Mobile View Leftbar Toggle-->
            <ul class="left-bar-switch pull-left">
                <li><span class="left-toggle-switch" style="touch-action: pan-y; user-select: none; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></span></li>
            </ul>
            <ul class="pull-right top-right-icons">
              <!--   <li><a href="#" class="btn-top-search"><i class="zmdi zmdi-search"></i></a></li> -->
                
                <li class="dropdown notifications-dropdown">
                                      <a href="#" class="btn-notification dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><%--<span class="noty-bubble"><%= lblnotificationcount.Text %></span>--%><i class="zmdi zmdi-globe"></i></a>
                    <div class="dropdown-menu notifications-tabs">
                        <div>
                            <ul class="nav material-tabs nav-tabs" role="tablist">
                               <!--  <li class="active"><a href="#message" aria-controls="message" role="tab" data-toggle="tab">Message</a></li>
                                <li><a href="#notifications" aria-controls="notifications" role="tab" data-toggle="tab">Notifications -->
                            </ul> 
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="message">
                                    <div class="message-list-container">
                                        <asp:Label ID="lblnotificationcount" runat="server" Text="" Visible="false"></asp:Label>
                                        <asp:Label ID="lblnotification" runat="server" Text="" Visible="false"></asp:Label>
                                        <%--<h4>You have <%= lblnotificationcount.Text %> notification</h4>--%>
                                         <ul class="clearfix">
                                            <li class="clearfix">
                                                <a href="#" class="message-thumb">
                                                </a><a href="#" class="message-intro"><span class="message-meta"> <asp:Label ID="notification1" runat="server" Text=""></asp:Label> </span>   <span class="message-time"></span></a>
                                            </li>
                                          <li class="clearfix">
                                                <a href="#" class="message-thumb">
                                                </a><a href="#" class="message-intro"><span class="message-meta"><asp:Label ID="notification2" runat="server" Text=""></asp:Label> </span>   <span class="message-time"></span></a>
                                            </li>
											 <li class="clearfix">
                                                <a href="#" class="message-thumb">
                                                </a><a href="#" class="message-intro"><span class="message-meta"><asp:Label ID="notification3" runat="server" Text=""></asp:Label> </span>   <span class="message-time"></span></a>
                                            </li>
                                            
                                            
                                            
                                            
                                        </ul>
                                        <%--<a class="btn btn-link btn-block btn-view-all" href="#"><span>View All</span></a>--%>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </li>
                <li><a href="#" class="right-toggle-switch" style="touch-action: pan-y; user-select: none; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"><i class="zmdi zmdi-format-align-left"></i><span class="more-noty"></span></a></li>
            </ul>
        </div>
    </div>
    <!--Topbar Right End-->
</header>
<!--Topbar End Here-->
<!--Leftbar Start Here-->
<aside class="leftbar" style="height: 311px;">
    <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: 250px; height: 311px;"><div class="left-aside-container" style="overflow: hidden; width: 250px; height: 311px;">
   <ul class="list-accordion tree-style">
           
            <li class="list-title"><a href="dashboardSatluj.aspx">DashBoard</a></li>
         

            <!-- <li class="list-title">Forms</li> -->
             <% if(Session["RoleId"].ToString()=="3") { %>
			 <li class="acc-parent-li">             
           <a href="Employeedashboard.aspx" class="acc-parent"><i class="zmdi zmdi-view-web"></i><span class="list-label">Enter Data</span></a>
             
            </li>  
           <% } %> 

       <% if(Session["RoleId"].ToString()=="4") { %>
			 <li class="acc-parent-li">             
           <a href="feedback.aspx" class="acc-parent"><i class="zmdi zmdi-view-web"></i><span class="list-label">Feedabck</span></a>
             
            </li>  
           <% } %> 
			 <li class="acc-parent-li">
                <a href="ViewData.aspx" class="acc-parent active"><i class="zmdi zmdi-view-web"></i><span class="list-label">Generation Status</span></a>
                
            </li>
       <% if(Session["RoleId"].ToString()=="3") { %>
        <li class="acc-parent-li">
                <a href="Notification.aspx" class="acc-parent active"><i class="zmdi zmdi-view-web"></i><span class="list-label">Notification</span></a>
                <% } %>  
            </li>
           
            <li class="acc-parent-li">
                <a href="#" class="acc-parent"><i class="zmdi zmdi-check"></i><span class="list-label"> Setting</span><span class="acc-icon"></span></a>
                <ul style="display: none;">
                    <li><a href="profilesetting.aspx">Profile </a></li>
                    <li><a href="ResetPassword.aspx">Reset Password </a></li>
                    <!-- <li><a href="form-call-me.html">Call Me Form </a></li> -->
                    <!-- <li><a href="form-call-me-captcha.html">Call Me Cptacha Form </a></li> -->
                    <!-- <li><a href="form-checkout.html">Checkout Form </a></li> -->
                    <!-- <li><a href="form-order-check-radio.html">Order Form </a></li> -->
                    <!-- <li><a href="form-order-field.html">Order Form With Quantity </a></li> -->
                </ul>
            </li>

         <li class="acc-parent-li" style="visibility:hidden">
            <% if(Session["RoleId"].ToString()=="1") { %>
             <a href="Utility.aspx" class="acc-parent"><i class="zmdi zmdi-check"></i><span class="list-label"> Descrypt Password</span><span class="acc-icon"></span></a>
                    <% } %>
                
               
            </li>
                  <% if(Session["RoleId"].ToString()=="4") { %>   
           <li class="acc-parent-li">
                <a href="#" class="acc-parent"><i class="zmdi zmdi-check"></i><span class="list-label"> Links</span></a>
                <ul style="display: none;">
                    <li> <a href="<%= urllink1.Text %>" class="alert-link"><%= lbllink1.Text %></a></li>
                    <li><a href="<%= urllink2.Text %>" class="alert-link"><%= lbllink2.Text %> </a></li>
                    <li><a href="<%= urllink3.Text %>" class="alert-link"><%= lbllink3.Text %></a></li>
                    <li> <a href="<%= urllink4.Text %>" class="alert-link"><%= lbllink4.Text %></a></li>
                </ul>
            </li>
        <% } %> 
        </ul>
         <% if(Session["RoleId"].ToString()=="8") { %>   
        <ul>
 <li class="acc-parent-li">
     <asp:Label ID="urllink1" runat="server" Text="" style="display:none"></asp:Label>
           <asp:Label ID="lbllink1" runat="server" Text="" style="display:none"></asp:Label>
                    <a href="<%= urllink1.Text %>" class="alert-link" target="_blank"><%= lbllink1.Text %></a>.
       </li>
        <li class="acc-parent-li">
            <asp:Label ID="urllink2" runat="server" Text="" style="display:none"></asp:Label>
          <asp:Label ID="lbllink2" runat="server" Text="" style="display:none"></asp:Label>
                    <a href="<%= urllink2.Text %>" class="alert-link" target="_blank"><%= lbllink2.Text %> </a>
       </li>
       <li class="acc-parent-li">
           <asp:Label ID="urllink3" runat="server" Text="" style="display:none"></asp:Label>
             <asp:Label ID="lbllink3" runat="server" Text="" style="display:none"></asp:Label>
                    <a href="<%= urllink3.Text %>" class="alert-link" target="_blank"><%= lbllink3.Text %> </a>.
       </li>
       <li class="acc-parent-li">
           <asp:Label ID="urllink4" runat="server" Text="" style="display:none"></asp:Label>
             <asp:Label ID="lbllink4" runat="server" Text="" style="display:none"></asp:Label>
                    <a href="<%= urllink4.Text %>" class="alert-link" target="_blank"><%= lbllink4.Text %> </a>.
       </li>
        </ul>
             <% } %>  
    </div><div class="slimScrollBar" style="background: rgba(255, 255, 255, 0.5); width: 4px; position: absolute; top: 48px; opacity: 0.4; display: none; border-radius: 0px; z-index: 99; right: 1px; height: 189px;"></div><div class="slimScrollRail" style="width: 4px; height: 100%; position: absolute; top: 0px; border-radius: 0px; background: rgb(34, 34, 34); opacity: 0.3; z-index: 90; right: 1px; display: none;"></div></div>
</aside>
<!--Leftbar End Here-->
<!--Page Container Start Here-->
<section class="main-container">
    <div class="container-fluid">
        <div class="page-header filled full-block light">
            <div class="row">
                <div class="col-md-12">
                     <marquee> <h2></h2>
            <p> <%= lblnotification.Text %></p></marquee>
                    <h2>Decrypt Password</h2>
                    <!-- <p>All kind of form elements here </p> -->
                </div>
            
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="widget-wrap">
                    <div class="widget-header block-header margin-bottom-0 clearfix">
                    
                    </div>
                    <div class="widget-container">
                        <div class="widget-content">
                            <div class="row">
                                <div class="col-md-12">
                                <div class="col-md-12">
<div class="widget-wrap" style="
    padding-top: 0;
">

<div class="widget-container">
<div class="widget-content">
   
     <div class="col-md-12">
     
        <asp:TextBox ID="txtdtp" CssClass="form-control" runat="server" placeholder="Encrypted Password" required="required" style="
    float: left;
    width: auto;
    margin-right: 6px;
"></asp:TextBox>
    
   <asp:Button ID="btnshow" class="btn btn-warning" runat="server"  Text="Get Password"  style="
    margin-bottom: 2%;
    float: left;
    margin-right: 4px;
" OnClick="btnshow_Click"/>     
     
</div>
    &nbsp:
  
</div>
</div>
</div>
                                    <p style="float:left;">Password Is:</p>&nbsp;&nbsp; <asp:Label ID="lblmessagedate" runat="server" Text="Label" style="color:Red;text-align: center;display: block;font-weight: 600;font-size: 19px; float:left"></asp:Label>
                                </div>
 
                            </div>
                               
                        </div>
                    </div>
                </div>
             
            </div>
        </div>

    </div>
    <!--Footer Start Here -->
    <footer class="footer-container">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <div class="footer-left">
                        <%--<span>© 2015 <a href="http://themeforest.net/user/westilian">westilian</a></span>--%>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <div class="footer-right">
                        <%--<span class="footer-meta">Crafted with&nbsp;<i class="fa fa-heart"></i>&nbsp;by&nbsp;<a href="http://themeforest.net/user/westilian">westilian</a></span>--%>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!--Footer End Here -->
</section>
<!--Page Container End Here-->
<!--Rightbar Start Here-->
<aside class="rightbar" style="height: 299px;">
<div class="user-profile-container">
            <div class="user-profile clearfix">
                <div class="admin-user-thumb">
                    <img src="images/Dummy.jpg" alt="admin">
                </div>
                <div class="admin-user-info">
                    <ul>
                       
                           <%= Session["Name"].ToString()%>

                    </ul>
                </div>
            </div>
            <div class="admin-bar">
                <ul>
                    <li><a href="LogOut.aspx"><i class="zmdi zmdi-power"></i>
                    </a>
                    </li>
                  
                    <li><a href="#"><i class="zmdi zmdi-settings"></i>
                    </a>
                    </li>
                </ul>
            </div>
        </div>
</aside>
<!--Rightbar End Here-->
<script src="js/lib/jquery.js"></script>
<script src="js/lib/jquery-migrate.js"></script>
<script src="js/lib/bootstrap.js"></script>
<script src="js/lib/jquery.ui.js"></script>
<script src="js/lib/jRespond.js"></script>
<script src="js/lib/nav.accordion.js"></script>
<script src="js/lib/hover.intent.js"></script>
<script src="js/lib/hammerjs.js"></script>
<script src="js/lib/jquery.hammer.js"></script>
<script src="js/lib/jquery.fitvids.js"></script>
<script src="js/lib/scrollup.js"></script>
<script src="js/lib/smoothscroll.js"></script>
<script src="js/lib/jquery.slimscroll.js"></script>
<script src="js/lib/jquery.syntaxhighlighter.js"></script>
<script src="js/lib/velocity.js"></script>
<script src="js/lib/smart-resize.js"></script>
<!--Forms-->
<script src="js/lib/jquery.maskedinput.js"></script>
<script src="js/lib/jquery.validate.js"></script>
<script src="js/lib/jquery.form.js"></script>
<script src="js/lib/additional-methods.js"></script>
<script src="js/lib/j-forms.js"></script>
<!--[if lt IE 10]>
<script src="js/lib/jquery.placeholder.min.js"></script>
<![endif]-->
<!--Select2-->
<script src="js/lib/select2.full.js"></script>
<script src="js/lib/jquery.loadmask.js"></script>
<script src="js/apps.js"></script>
</body>
        </form>
</html>
