﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GetPassword.aspx.cs" Inherits="SatlujWebApp.GetPassword" %>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
<title></title>
<link type="text/css" rel="stylesheet" href="css/font-awesome.css">
<link type="text/css" rel="stylesheet" href="css/material-design-iconic-font.css">
<link type="text/css" rel="stylesheet" href="css/bootstrap.css">
<link type="text/css" rel="stylesheet" href="css/animate.css">
<link type="text/css" rel="stylesheet" href="css/layout.css">
<link type="text/css" rel="stylesheet" href="css/components.css">
<link type="text/css" rel="stylesheet" href="css/widgets.css">
<link type="text/css" rel="stylesheet" href="css/plugins.css">
<link type="text/css" rel="stylesheet" href="css/pages.css">
<link type="text/css" rel="stylesheet" href="css/bootstrap-extend.css">
<link type="text/css" rel="stylesheet" href="css/common.css">
<link type="text/css" rel="stylesheet" href="css/responsive.css">
</head>
  <form runat="server" id="form1">
<body class="login-page social-login">
<!--Page Container Start Here-->
<section class="login-container boxed-login">
<div class="container">
<div class="col-md-6 col-md-offset-6 col-sm-4 col-sm-offset-6" style="
    margin: 0 auto;
">
<div class="login-form-container">
<form action=" method="post" class="j-forms" id="forms-login" novalidate>

<div class="login-form-header">
<div class="logo">
<h3 style="
    color: #ffffff;
    padding: 15px;
    font-weight: 600;
" class="__web-inspector-hide-shortcut__">SJVN</h3></div>
</div>
<div class="login-form-content">



<!-- start login -->

<!-- end login -->

<!-- start password -->
<div class="unit" style="
    margin-bottom: 0;
" >
<div class="input login-input">
<label class="icon-left" for="password">
<i class="zmdi zmdi-key"></i>
</label>
<input class="form-control login-frm-input" type="password" id="password" name="password" placeholder="new Password" runat="server">
<span class="hint">

</span>
</div>
</div>

<div class="unit">
<div class="input login-input">
<label class="icon-left" for="password">
<i class="zmdi zmdi-key"></i>
</label>
<input class="form-control login-frm-input" type="password" id="ConfirmPassword" name="ConfirmPassword" placeholder="Confirm Password" runat="server">
<span class="hint">

</span>
</div>
</div>
<!-- end password -->


<!-- start keep logged -->

<!-- end keep logged -->

<!-- start response from server -->
<div class="response"></div>
<!-- end response from server -->



</div>
<div class="login-form-footer">
<asp:Button ID="btnsignin" runat="server" Text="Reset Password" class="btn-block btn btn-primary" OnClick="btnsignin_Click"/>

</div>
    <div>
    <asp:Label ID="lblmessage" runat="server" Text=""></asp:Label>
    </div>



<!-- start social buttons -->








</form>
</div>
</div>
</div>
<!--Footer Start Here -->
<footer class="login-page-footer">
<div class="container">
<div class="row">
<div class="col-md-12 col-md-offset-12 col-sm-12 col-sm-offset-12">
<div class="footer-content">

</div>
</div>
</div>
</div>
</footer>
<!--Footer End Here -->
</section>
<!--Page Container End Here-->
<script src="js/lib/jquery.js"></script>
<script src="js/lib/jquery-migrate.js"></script>
<script src="js/lib/bootstrap.js"></script>
<script src="js/lib/jRespond.js"></script>
<script src="js/lib/hammerjs.js"></script>
<script src="js/lib/jquery.hammer.js"></script>
<script src="js/lib/smoothscroll.js"></script>
<script src="js/lib/smart-resize.js"></script>

<script src="js/lib/jquery.validate.js"></script>
<script src="js/lib/jquery.form.js"></script>
<script src="js/lib/j-forms.js"></script>
<script src="js/lib/login-validation.js"></script>
</body>
   </form>
</html>

