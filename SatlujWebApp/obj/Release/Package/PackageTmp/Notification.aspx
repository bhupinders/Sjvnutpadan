﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Notification.aspx.cs" Inherits="SatlujWebApp.Notification" %>



<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    <title>Notification</title>
    <link type="text/css" rel="stylesheet" href="css/font-awesome.css">
    <link type="text/css" rel="stylesheet" href="css/material-design-iconic-font.css">
    <link type="text/css" rel="stylesheet" href="css/bootstrap.css">
    <link type="text/css" rel="stylesheet" href="css/animate.css">
    <link type="text/css" rel="stylesheet" href="css/layout.css">
    <link type="text/css" rel="stylesheet" href="css/components.css">
    <link type="text/css" rel="stylesheet" href="css/widgets.css">
    <link type="text/css" rel="stylesheet" href="css/plugins.css">
    <link type="text/css" rel="stylesheet" href="css/pages.css">
    <link type="text/css" rel="stylesheet" href="css/bootstrap-extend.css">
    <link type="text/css" rel="stylesheet" href="css/common.css">
    <link type="text/css" rel="stylesheet" href="css/responsive.css">
    <link type="text/css" id="themes" rel="stylesheet" href="">
    <style type="text/css">
        .auto-style1 {
            position: relative;
            width: 100%;
            min-height: 1px;
            -webkit-box-flex: 0;
            -webkit-flex: 0 0 100%;
            -ms-flex: 0 0 100%;
            flex: 0 0 100%;
            max-width: 100%;
            left: 0px;
            top: 0px;
            height: 81px;
            padding-left: 15px;
            padding-right: 15px;
        }
        .auto-style2 {
            display: block;
            width: 100%;
            font-size: 14px;
            line-height: 1.846;
            color: #555555;
            -webkit-background-clip: padding-box;
            background-clip: padding-box;
            border-radius: 3px;
            -webkit-transition: all border-color ease-in-out .15s, box-shadow ease-in-out .15s ease-out;
            transition: all border-color ease-in-out .15s, box-shadow ease-in-out .15s ease-out;
            -o-transition: all border-color ease-in-out .15s, box-shadow ease-in-out .15s ease-out;
            height: 199px;
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            -moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            -moz-transition: all border-color ease-in-out .15s, box-shadow ease-in-out .15s ease-out;
            border: 1px solid #cccccc;
            padding: 6px 12px;
            background-color: #ffffff;
            background-image: none;
        }
    </style>
</head>
      <form runat="server" id="form1">
<body class="leftbar-view">
<!--Topbar Start Here-->
<header class="topbar clearfix">
    <!--Top Search Bar Start Here-->
    <div class="top-search-bar">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="search-input-addon">
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Top Search Bar End Here-->
    <!--Topbar Left Branding With Logo Start-->
    <div class="topbar-left pull-left">
        <div class="clearfix">
            <ul class="left-branding pull-left clickablemenu ttmenu dark-style menu-color-gradient">
                <li><span class="left-toggle-switch" style="touch-action: pan-y; user-select: none; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"><i class="fa fa-bars" aria-hidden="true"></i></span></li>
               <li>
                    <div class="logo">
                   <h3><img src="images/logo.png" ></h3>
                       <!--  <a href="index.html" title="Admin Template"><img src="images/logo.png" alt="logo"></a> -->
                    </div>
                </li>
            </ul>
            <!--Mobile Search and Rightbar Toggle-->
            <ul class="branding-right pull-right">
                <li><a href="#" class="btn-mobile-search btn-top-search" style="touch-action: pan-y; user-select: none; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"><i class="zmdi zmdi-search"></i></a></li>
                <li><a href="#" class="btn-mobile-bar" style="touch-action: pan-y; user-select: none; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"><i class="fa fa-bars" aria-hidden="true"></i></a></li>
            </ul>
        </div>
    </div>
    <!--Topbar Left Branding With Logo End-->
    <!--Topbar Right Start-->
    <h3 class="app-name">SJVN Utpadan</h3><div class="topbar-right pull-right">
        <div class="clearfix">
            <!--Mobile View Leftbar Toggle-->
           
            <ul class="pull-right top-right-icons">
              <!--   <li><a href="#" class="btn-top-search"><i class="zmdi zmdi-search"></i></a></li> -->
               
                <li class="dropdown notifications-dropdown">
                                      <a href="#" class="btn-notification dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><%--<span class="noty-bubble"><%= lblnotificationcount.Text %></span>--%><i class="zmdi zmdi-globe"></i></a>
                    <div class="dropdown-menu notifications-tabs">
                        <div>
                            <ul class="nav material-tabs nav-tabs" role="tablist">
                               <!--  <li class="active"><a href="#message" aria-controls="message" role="tab" data-toggle="tab">Message</a></li>
                                <li><a href="#notifications" aria-controls="notifications" role="tab" data-toggle="tab">Notifications -->
                            </ul> 
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="message">
                                    <div class="message-list-container">
                                        <asp:Label ID="lblnotificationcount" runat="server" Text="" Visible="false"></asp:Label>
                                        <asp:Label ID="lblnotification" runat="server" Text="" Visible="false"></asp:Label>
                                        <%--<h4>You have <%= lblnotificationcount.Text %> notification</h4>--%>
                                         <ul class="clearfix">
                                            <li class="clearfix">
                                                <a href="#" class="message-thumb">
                                                </a><a href="#" class="message-intro"><span class="message-meta"> <asp:Label ID="notification1" runat="server" Text=""></asp:Label> </span>   <span class="message-time"></span></a>
                                            </li>
                                          <li class="clearfix">
                                                <a href="#" class="message-thumb">
                                                </a><a href="#" class="message-intro"><span class="message-meta"><asp:Label ID="notification2" runat="server" Text=""></asp:Label> </span>   <span class="message-time"></span></a>
                                            </li>
											 <li class="clearfix">
                                                <a href="#" class="message-thumb">
                                                </a><a href="#" class="message-intro"><span class="message-meta"><asp:Label ID="notification3" runat="server" Text=""></asp:Label> </span>   <span class="message-time"></span></a>
                                            </li>
                                            
                                            
                                            
                                            
                                        </ul>
                                        <a class="btn btn-link btn-block btn-view-all" href="#"><span></span></a>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </li>
                <li><a href="#" class="right-toggle-switch" style="touch-action: pan-y; user-select: none; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"><i class="zmdi zmdi-format-align-left"></i><span class="more-noty"></span></a></li>
            </ul>
        </div>
    </div>
    <!--Topbar Right End-->
</header>
<!--Topbar End Here-->

<!--Leftbar Start Here-->
<aside class="leftbar" style="height: 189px;">
    <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: 250px; height: 189px;"><div class="left-aside-container" style="overflow: hidden; width: 250px; height: 189px;">
   <ul class="list-accordion tree-style">
            
            <li class="list-title"><a href="dashboardSatluj.aspx">DashBoard</a></li>
       

            <!-- <li class="list-title">Forms</li> -->
            <% if(Session["RoleId"].ToString()=="3") { %>
			 <li class="acc-parent-li">             
           <a href="Employeedashboard.aspx" class="acc-parent"><i class="zmdi zmdi-view-web"></i><span class="list-label">Enter Data</span></a>
             
            </li>  
          <% } %> 
			 
       <% if(Session["RoleId"].ToString()=="2") { %>
        <li class="acc-parent-li">
                <a href="Link.aspx.aspx" class="acc-parent"><i class="zmdi zmdi-view-web"></i><span class="list-label">Link</span></a>
                
            </li> 
       <% } %>  
         <% if(Session["RoleId"].ToString()=="2" || Session["RoleId"].ToString()=="4") { %>
        <li class="acc-parent-li">
                <a href="Feedback.aspx.aspx" class="acc-parent"><i class="zmdi zmdi-view-web"></i><span class="list-label">FeedBack</span></a>
                
            </li> 
          <% } %>   
         <% if(Session["RoleId"].ToString()=="4" || Session["RoleId"].ToString()=="3") { %>
			 <li class="acc-parent-li">
                <a href="ViewData.aspx" class="acc-parent"><i class="zmdi zmdi-view-web"></i><span class="list-label">Generation Status</span></a>
                
            </li> 
         <% } %> 
         <% if(Session["RoleId"].ToString()=="3") { %>   
            <li class="acc-parent-li">
                <a href="Notification.aspx" class="acc-parent"><i class="zmdi zmdi-view-web"></i><span class="list-label">Notification</span></a>
                
            </li>  
        <% } %> 
            <li class="acc-parent-li">
                <a href="#" class="acc-parent"><i class="zmdi zmdi-check"></i><span class="list-label"> Setting</span></a>
                <ul style="display: none;">
                    <li><a href="profilesetting.aspx">Profile </a></li>
                    <li><a href="ResetPassword.aspx">Reset Password </a></li>
                   
                </ul>
            </li>

        </ul>
    </div><div class="slimScrollBar" style="background: rgba(255, 255, 255, 0.5); width: 4px; position: absolute; top: 48px; opacity: 0.4; display: none; border-radius: 0px; z-index: 99; right: 1px; height: 189px;"></div><div class="slimScrollRail" style="width: 4px; height: 100%; position: absolute; top: 0px; border-radius: 0px; background: rgb(34, 34, 34); opacity: 0.3; z-index: 90; right: 1px; display: none;"></div></div>
</aside>
<!--Leftbar End Here-->
<!-
    <br />
<section class="main-container widget-wrap">
  
    <div class="page-header filled full-block light">
            <div class="row">
                <div class="col-md-12">
                    <marquee> <h2></h2>
            <p> <%= lblnotification.Text %></p></marquee>
                    <!-- <p>All kind of form elements here </p> -->
                </div>
            
            </div>
        </div>
     
<div class="table-responsive1">
<textarea class="form-control" Placeholder="Enter Notification...." name="notification" runat="server" id="notification" style="min-height: 165px;" required="required"></textarea>
</div>
<br>

<div class="form-footer">
                                  
                                    <asp:Button ID="btnsave" runat="server" Text="submit" class="btn btn-info primary-btn" OnClick="btnsave_Click" /> &nbsp;<asp:Label ID="lblmessage" runat="server" Text=""></asp:Label>
<%--                                  --%>
                                </div>
    <div class="table-responsive">
    <asp:GridView ID="grdfeedback" runat="server" class="table table-bordered foo-data-table"  Width="629px" AutoGenerateColumns="False" AutoGenerateEditButton="True" OnRowCancelingEdit="grdfeedback_RowCancelingEdit" OnRowEditing="grdfeedback_RowEditing" OnRowUpdating="grdfeedback_RowUpdating" AllowPaging="True" OnPageIndexChanging="grdfeedback_PageIndexChanging">
        <Columns>
            <asp:TemplateField HeaderText="Notification Name">
                <EditItemTemplate>
                    <asp:TextBox ID="txtname" runat="server" Text='<%# Eval("notification") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblname" runat="server" Text='<%# Eval("notification") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle Font-Bold="True" />
            </asp:TemplateField>
           
           
            <asp:TemplateField HeaderText="Id" Visible="false">
                <ItemTemplate>
                    <asp:Label ID="lblid" runat="server" Text='<%# Eval("Id") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle Font-Bold="True" />
            </asp:TemplateField>
           
        </Columns>

    </asp:GridView>
        </div>
<!--Footer Start Here -->
<footer class="footer-container">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <div class="footer-left">
                    <%--<span>© 2015 <a href="">dummy</a></span>--%>
                </div>
            </div>
            <div class="col-md-6 col-sm-6">
                <div class="footer-right">
                    <%--<span class="footer-meta">dfgdfdgdfb &nbsp;<i class="fa fa-heart"></i>&nbsp;by&nbsp;<a href="">gfdgdfg</a></span>--%>
                </div>
            </div>
        </div>
    </div>
</footer>
</section>
<!--Footer End Here -->

<!--Page Container End Here-->
<!--Rightbar Start Here-->
<aside class="rightbar">
<div class="user-profile-container">
            <div class="user-profile clearfix">
                <div class="admin-user-thumb">
                    <img src="images/Dummy.jpg" alt="admin">
                </div>
                <div class="admin-user-info">
                     <ul>
                        <%-- <% if(Session["RoleId"].ToString()=="1") { %>
                        <li><a href="#"> Super Admin Panel</a></li>
                       <% } %>
                         <% if(Session["RoleId"].ToString()=="2") { %>
                        <li><a href="#"> Admin Panel</a></li>
                       <% } %>
                         <% if(Session["RoleId"].ToString()=="3") { %>
                        <li><a href="#">Employee Panel</a></li>
                       <% } %>
                         <% if(Session["RoleId"].ToString()=="4") { %>
                        <li><a href="#"> User Panel</a></li>
                       <% } %>--%>
                            <%= Session["Name"].ToString()%>

                    </ul>
                </div>
            </div>
            <div class="admin-bar">
                <ul>
                    <li><a href="LogOut.aspx"><i class="zmdi zmdi-power"></i>
                    </a>
                    </li>
                </ul>
            </div>
        </div>
</aside>
<!--Rightbar End Here-->
<script src="js/lib/jquery.js"></script>
<script src="js/lib/jquery-migrate.js"></script>
<script src="js/lib/bootstrap.js"></script>
<script src="js/lib/jquery.ui.js"></script>
<script src="js/lib/jRespond.js"></script>
<script src="js/lib/nav.accordion.js"></script>
<script src="js/lib/hover.intent.js"></script>
<script src="js/lib/hammerjs.js"></script>
<script src="js/lib/jquery.hammer.js"></script>
<script src="js/lib/jquery.fitvids.js"></script>
<script src="js/lib/scrollup.js"></script>
<script src="js/lib/smoothscroll.js"></script>
<script src="js/lib/jquery.slimscroll.js"></script>
<script src="js/lib/jquery.syntaxhighlighter.js"></script>
<script src="js/lib/velocity.js"></script>
<script src="js/lib/jquery-jvectormap.js"></script>
<script src="js/lib/jquery-jvectormap-world-mill.js"></script>
<script src="js/lib/jquery-jvectormap-us-aea.js"></script>
<script src="js/lib/smart-resize.js"></script>
<!--iCheck-->
<script src="js/lib/icheck.js"></script>
<script src="js/lib/jquery.switch.button.js"></script>
<!--CHARTS-->
<script src="js/lib/chart/sparkline/jquery.sparkline.js"></script>
<script src="js/lib/chart/easypie/jquery.easypiechart.min.js"></script>
<script src="js/lib/chart/flot/excanvas.min.js"></script>
<script src="js/lib/chart/flot/jquery.flot.min.js"></script>
<script src="js/lib/chart/flot/curvedLines.js"></script>
<script src="js/lib/chart/flot/jquery.flot.time.min.js"></script>
<script src="js/lib/chart/flot/jquery.flot.stack.min.js"></script>
<script src="js/lib/chart/flot/jquery.flot.axislabels.js"></script>
<script src="js/lib/chart/flot/jquery.flot.resize.min.js"></script>
<script src="js/lib/chart/flot/jquery.flot.tooltip.min.js"></script>
<script src="js/lib/chart/flot/jquery.flot.spline.js"></script>
<script src="js/lib/chart/flot/jquery.flot.pie.min.js"></script>
<!--Forms-->
<script src="js/lib/jquery.maskedinput.js"></script>
<script src="js/lib/jquery.validate.js"></script>
<script src="js/lib/jquery.form.js"></script>
<script src="js/lib/j-forms.js"></script>
<script src="js/lib/jquery.loadmask.js"></script>
<script src="js/lib/vmap.init.js"></script>
<script src="js/lib/theme-switcher.js"></script>
<script src="js/apps.js"></script>
</body>
  </form>
</html>
<br />
    <br />
    <br />
