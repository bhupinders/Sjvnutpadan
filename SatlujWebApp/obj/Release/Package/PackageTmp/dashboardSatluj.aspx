﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="dashboardSatluj.aspx.cs" Inherits="SatlujWebApp.dashboardSatluj" %>

<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    <title>DashBoard :</title>
    <link type="text/css" rel="stylesheet" href="css/font-awesome.css">
    <link type="text/css" rel="stylesheet" href="css/material-design-iconic-font.css">
    <link type="text/css" rel="stylesheet" href="css/bootstrap.css">
    <link type="text/css" rel="stylesheet" href="css/animate.css">
    <link type="text/css" rel="stylesheet" href="css/layout.css">
    <link type="text/css" rel="stylesheet" href="css/components.css">
    <link type="text/css" rel="stylesheet" href="css/widgets.css">
    <link type="text/css" rel="stylesheet" href="css/plugins.css">
    <link type="text/css" rel="stylesheet" href="css/pages.css">
    <link type="text/css" rel="stylesheet" href="css/bootstrap-extend.css">
    <link type="text/css" rel="stylesheet" href="css/common.css">
    <link type="text/css" rel="stylesheet" href="css/responsive.css">
    <link type="text/css" id="themes" rel="stylesheet" href="">
        
    <style>
ul.nav.nav-tabs>li.active>a, ul.nav.nav-tabs>li.active>a:focus, ul.nav.nav-tabs>li.active>a:hover {
    color: #fff;
    cursor: default;
    background-color: #fff;
    border: 1px solid #009eda;
    border-bottom-color: transparent;
    padding: 10px 15px;
    padding-bottom: 4px;
    text-decoration: none;
    background: #009eda;
}
 ul.nav.nav-tabs>li {
    float: left;
    margin-bottom: -1px;
    
}
   ul.nav.nav-tabs>li{
padding: 10px 15px;
    padding-bottom: 4px;
   
       color: #555;
    cursor: default;
   
   }
</style>
</head>
    <form runat="server" id="fom1">
<body class="leftbar-view">
<!--Topbar Start Here-->
<header class="topbar clearfix">
    <!--Top Search Bar Start Here-->
    <div class="top-search-bar">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="search-input-addon">
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Top Search Bar End Here-->
    <!--Topbar Left Branding With Logo Start-->
    <div class="topbar-left pull-left">
        <div class="clearfix">
            <ul class="left-branding pull-left clickablemenu ttmenu dark-style menu-color-gradient">
                <li><span class="left-toggle-switch" style="touch-action: pan-y; user-select: none; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"><i class="fa fa-bars" aria-hidden="true"></i></span></li>
                <li>
                    <div class="logo">
                   <h3><img src="images/logo.png" ></h3>
                       <!--  <a href="index.html" title="Admin Template"><img src="images/logo.png" alt="logo"></a> -->
                    </div>
                </li>
            </ul>
            <!--Mobile Search and Rightbar Toggle-->
            <ul class="branding-right pull-right">
                <li><a href="#" class="btn-mobile-search btn-top-search" style="touch-action: pan-y; user-select: none; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"><i class="zmdi zmdi-search"></i></a></li>
                <li><a href="#" class="btn-mobile-bar" style="touch-action: pan-y; user-select: none; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"><i class="fa fa-bars" aria-hidden="true"></i></a></li>
            </ul>
        </div>
    </div>
    <!--Topbar Left Branding With Logo End-->
    <!--Topbar Right Start-->
    <h3 class="app-name">SJVN Utpadan</h3><div class="topbar-right pull-right">
        <div class="clearfix">
            <!--Mobile View Leftbar Toggle-->
           
            <ul class="pull-right top-right-icons">
              <!--   <li><a href="#" class="btn-top-search"><i class="zmdi zmdi-search"></i></a></li> -->
               
                <li class="dropdown notifications-dropdown">
                                      <a href="#" class="btn-notification dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><%--<span class="noty-bubble"><%= lblnotificationcount.Text %></span>--%><i class="zmdi zmdi-globe"></i></a>
                    <div class="dropdown-menu notifications-tabs">
                        <div>
                            <ul class="nav material-tabs nav-tabs" role="tablist">
                               <!--  <li class="active"><a href="#message" aria-controls="message" role="tab" data-toggle="tab">Message</a></li>
                                <li><a href="#notifications" aria-controls="notifications" role="tab" data-toggle="tab">Notifications -->
                            </ul> 
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="message">
                                    <div class="message-list-container">
                                        <asp:Label ID="Label1" runat="server" Text="" Visible="false"></asp:Label>
                                       <asp:Label ID="lblnotificationcount" runat="server" Text="" Visible="false"></asp:Label>
                                        <%--<h4>You have <%= lblnotificationcount.Text %> notification</h4>--%>
                                         <ul class="clearfix">
                                            <li class="clearfix">
                                                <a href="#" class="message-thumb">
                                                </a><a href="#" class="message-intro"><span class="message-meta"> <asp:Label ID="notification1" runat="server" Text=""></asp:Label> </span>   <span class="message-time"></span></a>
                                            </li>
                                          <li class="clearfix">
                                                <a href="#" class="message-thumb">
                                                </a><a href="#" class="message-intro"><span class="message-meta"><asp:Label ID="notification2" runat="server" Text=""></asp:Label> </span>   <span class="message-time"></span></a>
                                            </li>
											 <li class="clearfix">
                                                <a href="#" class="message-thumb">
                                                </a><a href="#" class="message-intro"><span class="message-meta"><asp:Label ID="notification3" runat="server" Text=""></asp:Label> </span>   <span class="message-time"></span></a>
                                            </li>
                                            
                                            
                                            
                                            
                                        </ul>
                                        <a class="btn btn-link btn-block btn-view-all" href="#"><span></span></a>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </li>
                <li><a href="#" class="right-toggle-switch" style="touch-action: pan-y; user-select: none; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"><i class="zmdi zmdi-format-align-left"></i><span class="more-noty"></span></a></li>
            </ul>
        </div>
    </div>
    <!--Topbar Right End-->
</header>
<!--Topbar End Here-->

<!--Leftbar Start Here-->
<aside class="leftbar" style="height: 189px;">
    <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: 250px; height: 189px;"><div class="left-aside-container" style="overflow: hidden; width: 250px; height: 189px;">
   <ul class="list-accordion tree-style">
          
            <li class="list-title"><a href="dashboardSatluj.aspx">DashBoard</a></li>
      

            <!-- <li class="list-title">Forms</li> -->
                <% if(Session["RoleId"].ToString()=="3") { %>
			 <li class="acc-parent-li">             
           <a href="Employeedashboard.aspx" class="acc-parent"><i class="zmdi zmdi-view-web"></i><span class="list-label">Enter Data</span></a>
             
            </li>  
                 <% } %>			  
       <% if(Session["RoleId"].ToString()=="2") { %>
        <li class="acc-parent-li">
                <a href="Link.aspx" class="acc-parent"><i class="zmdi zmdi-view-web"></i><span class="list-label">Link</span></a>
                
            </li> 
                <% } %>         <% if (Session["RoleId"].ToString() == "2" || Session["RoleId"].ToString() == "4")
                                   { %>
        <li class="acc-parent-li">
                <a href="Feedback.aspx" class="acc-parent"><i class="zmdi zmdi-view-web"></i><span class="list-label">FeedBack</span></a>
                
            </li> 
                <% } %>       <% if(Session["RoleId"].ToString()=="4" || Session["RoleId"].ToString()=="3") { %>
			 <li class="acc-parent-li">
                <a href="ViewData.aspx" class="acc-parent"><i class="zmdi zmdi-view-web"></i><span class="list-label">Generation Status</span></a>
                
            </li>
                 <% } %>       <% if(Session["RoleId"].ToString()=="3") { %> 
            <li class="acc-parent-li">
                <a href="Notification.aspx" class="acc-parent"><i class="zmdi zmdi-view-web"></i><span class="list-label">Notification</span></a>
                
            </li>  
                <% } %>   
            <li class="acc-parent-li">
                <a href="#" class="acc-parent"><i class="zmdi zmdi-check"></i><span class="list-label"> Setting</span></a>
                <ul style="display: none;">
                    <li><a href="profilesetting.aspx">Profile </a></li>
                    <li><a href="ResetPassword.aspx">Reset Password </a></li>
                  
                </ul>
            </li>
        <% if(Session["RoleId"].ToString()=="4") { %>   
           <li class="acc-parent-li">
                <a href="#" class="acc-parent"><i class="zmdi zmdi-check"></i><span class="list-label"> Links</span></a>
                <ul style="display: none;">
                    <li> <a href="<%= urllink1.Text %>" class="alert-link"><%= lbllink1.Text %></a></li>
                    <li><a href="<%= urllink2.Text %>" class="alert-link"><%= lbllink2.Text %> </a></li>
                    <li><a href="<%= urllink3.Text %>" class="alert-link"><%= lbllink3.Text %></a></li>
                    <li> <a href="<%= urllink4.Text %>" class="alert-link"><%= lbllink4.Text %></a></li>
                </ul>
            </li>
        <% } %> 
        </ul>
         <% if(Session["RoleId"].ToString()=="8") { %>   
      <ul>
 <li class="acc-parent-li">
     <asp:Label ID="urllink1" runat="server" Text="" style="display:none"></asp:Label>
           <asp:Label ID="lbllink1" runat="server" Text="" style="display:none"></asp:Label>
                    <a href="<%= urllink1.Text %>" class="alert-link"><%= lbllink1.Text %></a>.
       </li>
        <li class="acc-parent-li">
            <asp:Label ID="urllink2" runat="server" Text="" style="display:none"></asp:Label>
          <asp:Label ID="lbllink2" runat="server" Text="" style="display:none"></asp:Label>
                    <a href="<%= urllink2.Text %>" class="alert-link"><strong><%= lbllink2.Text %></strong> </a>
       </li>
       <li class="acc-parent-li">
           <asp:Label ID="urllink3" runat="server" Text="" style="display:none"></asp:Label>
             <asp:Label ID="lbllink3" runat="server" Text="" style="display:none"></asp:Label>
                    <a href="<%= urllink3.Text %>" class="alert-link"><strong><%= lbllink3.Text %></strong> </a>.
       </li>
       <li class="acc-parent-li">
           <asp:Label ID="urllink4" runat="server" Text="" style="display:none"></asp:Label>
             <asp:Label ID="lbllink4" runat="server" Text="" style="display:none"></asp:Label>
                    <a href="<%= urllink4.Text %>" class="alert-link"><strong><%= lbllink4.Text %></strong> </a>.
       </li>
        </ul>
             <% } %> 
    </div><div class="slimScrollBar" style="background: rgba(255, 255, 255, 0.5); width: 4px; position: absolute; top: 48px; opacity: 0.4; display: none; border-radius: 0px; z-index: 99; right: 1px; height: 189px;"></div><div class="slimScrollRail" style="width: 4px; height: 100%; position: absolute; top: 0px; border-radius: 0px; background: rgb(34, 34, 34); opacity: 0.3; z-index: 90; right: 1px; display: none;"></div></div>
</aside>
<!--Leftbar End Here-->
<!--Page Container Start Here-->
<section class="main-container">
<div class="container-fluid">
<div class="page-header filled full-block light">
    <div class="row">
        <div class="col-md-12 col-sm-12">
           <marquee> <h2></h2>
            <p> <%= lblnotification.Text %></p></marquee>
        </div>
        
    </div>
</div>
<%--<div class="row">
    <div class="col-md-4 col-sm-4">
        <div class="stats-widget stats-widget">
            <div class="widget-header">
                <h3>Counter 1</h3>
                <asp:Label ID="lblnotification" runat="server" Text="" style="display:none"></asp:Label>
            </div>
			<div class="w_bg_teal stats-chart-container">
			<div id="days" class="timer_box"><h1>0</h1><p>text</p></div>
                <!-- <div id="widget-stats-chart" class="stats-chart">
                </div> -->
            </div>
        </div>
    </div>
    <div class="col-md-4 col-sm-4">
        <div class="widget-wrap stats-widget">
            <div class="widget-header">
                <h3>Counter 2</h3></div>
				<div class="w_bg_deep_purple stats-chart-container">
				<div id="Div1" class="timer_box"><h1>0</h1><p>text</p></div>
<!--                 <div id="widget-monthly-chart" class="stats-chart"></div>
 -->            </div>
        </div>
    </div>
    <div class="col-md-4 col-sm-4">
        <div class="widget-wrap stats-widget">
            <div class="widget-header">
                <h3>Cunter 3</h3>
                
            </div>
			<div class="w_bg_cyan stats-chart-container">
			<div id="Div2" class="timer_box"><h1>0</h1><p>text</p></div>
<!--                 <div id="widget-alltime-chart" class="stats-chart"></div>
 -->            </div>
        </div>
    </div>
</div>--%>
    <div class="row">
<div class="widget-container">
                        <div class="col-md- 12 m-b-30" style="
    background: #fff;
    padding: 2%;
"> 
                                <ul class="nav nav-tabs">
                                    <li class="tab active">
                                        <a href="#home" data-toggle="tab" aria-expanded="true"> 
                                           
                                            <span class="hidden-xs">NJHPS </span> 
                                        </a> 
                                    </li> 
                                    <li class="tab"> 
                                        <a href="#profile" data-toggle="tab" aria-expanded="false"> 
                                            
                                            <span class="hidden-xs">RHPS</span> 
                                        </a> 
                                    </li> 
                                    <li class="tab"> 
                                        <a href="#messages" data-toggle="tab" aria-expanded="false"> 
                                            
                                            <span class="hidden-xs">KWPP</span> 
                                        </a> 
                                    </li> 
                                    <li class="tab"> 
                                        <a href="#settings" data-toggle="tab" aria-expanded="false"> 
                                            
                                            <span class="hidden-xs">CSPP</span> 
                                        </a> 
                                    </li> 
                                </ul> 
                                <div class="tab-content" style="
    padding: 1% 0;
"> 
                                    <div class="tab-pane active" id="home"> 
                                       <h3>Nathpa Jhakri Hydro Power station(NJHPS) (6x 250 MW) </h3>
                                        <ul>
      <li>Nathpa Jhakri Hydroelectric Station of 1500 MW capacity is the country’s largest hydropower plant and was commissioned in May, 2004.</li>

<li>This is a run of the river project located on River Sutlej in Shimla district of Himachal Pradesh, India.</li>

 <li>It is designed to generate 6612 million units of electricity each year.
 Out of the total energy generated at the bus bar, 12 percent is supplied free of cost to the home state i.e. Himachal Pradesh. Remaining power has been allocated to different states / UTs of Northern Region by Ministry of Power, Government of India.</li>
   </ul>
        <img class="img-responsive" src="images/nathpa.jpg" />
 <br />
                                        <br />
                                        <div class="table-responsive">
<table class="table table-bordered foo-data-table" style="background: #f2fafa; color: #009eda;font-weight: 600">
<thead>
<tr>

<th colspan="2" data-hide="all" style="
    text-align: center; font-weight: 600
">Daily</th>

<th colspan="2" style="
    text-align: center; font-weight: 600
">Monthly</th>



<th class="td-center" colspan="2" style="
    text-align: center; font-weight: 600
">Yearly</th>


</tr>
</thead>
<tbody>
<tr>

<td>MoU Target</td>
<td>Actual Generation(MU)</td>
<td>MoU Target</td>
<td>Actual Generation(MU)</td>
<td>MoU Target</td>
<td>Actual Generation(MU)</td>



</tr>
<tr>
<!--<td style="
    padding: 0;
    margin: 0;
    margin-top: 9px;
    display: block;
">  </td> -->
<td><input name="khabsatluj1" type="text" id="khabsatluj1" class="form-control" disabled="disabled"  style="width:129px" runat="server">  </td>
<td><input name="khabspliti1" type="text" id="khabspliti1" class="form-control" disabled="disabled" runat="server"></td>
<td><input name="powari1" type="text" id="powari1" class="form-control" disabled="disabled"  style="width:129px" runat="server">  </td>
<td><input name="wangtoo1" type="text" id="wangtoo1" class="form-control" disabled="disabled" runat="server">  </td>
<td><input name="dkhabsatluj1" type="text" id="dkhabsatluj1" class="form-control" disabled="disabled"  style="width:129px" runat="server"></td>
<td><input name="dkhabspiti1" type="text" id="dkhabspiti1" class="form-control" disabled="disabled" runat="server">  </td>




</tr>
    


</tbody>
</table>
             </div>
<div>
    Datea Record Time For-> &nbsp: <asp:Label ID="lbltime" runat="server" Text="Label" ForeColor="Red" Font-Bold="true"></asp:Label>
</div>
                                    </div>
                                     
                                    <div class="tab-pane" id="profile">
                                       <h3>Rampur Hydro Power Station (RHPS) (6x68.67 MW) </h3>
                                        <ul>
      <li>Rampur Hydro Power Station of 412 MW installed capacity is located on River Satluj in Shimla and Kullu district of Himachal Pradesh, India and is operating in tandem with upstream project NJHPS. The project was commissioned in December, 2014.</li>

<li>RHPS is designed to generate 1878 million units of electricity each year.</li>

<li>Out of the total energy generated at the bus bar, 13 percent (including 1% LADA) is supplied free of cost to the home state i.e. Himachal Pradesh. Remaining power has been allocated to different states / UTs of Northern Region by Ministry of Power, Government of India,</li>
 </ul>  <img class="img-responsive" src="images/rampur.jpg" />
                                        <br />
                                         <br />
                                        <div class="table-responsive">
<table class="table table-bordered foo-data-table" style="background: #f2fafa;color: #009eda;font-weight: 600;">
<thead>
<tr>

<th colspan="2" data-hide="all" style="
    text-align: center; font-weight:600;
">Daily</th>

<th colspan="2" style="
    text-align: center;font-weight:600;
">Monthly</th>



<th class="td-center" colspan="2" style="
    text-align: center; font-weight:600;
">Yearly</th>


</tr>
</thead>
<tbody>
<tr>

<td>MoU Target</td>
<td>Actual Generation(MU)</td>
<td>MoU Target</td>
<td>Actual Generation(MU)</td>
<td>MoU Target</td>
<td>Actual Generation(MU)</td>



</tr>
<tr>
<!--<td style="
    padding: 0;
    margin: 0;
    margin-top: 9px;
    display: block;
">  </td> -->
<td><input name="khabsatluj2" type="text" id="khabsatluj2" class="form-control" disabled="disabled" style="width:129px" runat="server">  </td>
<td><input name="khabspliti2" type="text" id="khabspliti2" class="form-control" disabled="disabled" runat="server"></td>
<td><input name="powari2" type="text" id="powari2" class="form-control" disabled="disabled" style="width:129px" runat="server">  </td>
<td><input name="wangtoo2" type="text" id="wangtoo2" class="form-control" disabled="disabled" runat="server">  </td>
<td><input name="dkhabsatluj2" type="text" id="dkhabsatluj2" class="form-control" disabled="disabled" style="width:129px" runat="server"></td>
<td><input name="dkhabspiti2" type="text" id="dkhabspiti2" class="form-control" disabled="disabled" runat="server">  </td>




</tr>
    


</tbody>
</table>
             </div>
                                         <div>
    Datea Record Time For-> &nbsp: <asp:Label ID="lbltime2" runat="server" Text="Label" ForeColor="Red" Font-Bold="true"></asp:Label>
</div>

 </div> 
                                    
                                    <div class="tab-pane" id="messages">
                                        <h3>Khirvire Wind Power Project (47.60 MW)</h3>
                                        <ul>
      <li>The Khirvire Wind Power Project of 47.6 MW capacity is located in Khirvire/Kombhalane villages, Taluka- Akole, District - Ahmednagar, Maharashtra, India.</li>

<li>The project comprises of 56 Wind Electric Generators (WEGs) of 850kW capacity each. The project stands commissioned on 20.05.2014.</li>
<li> All the 56 WEGs of Khirvire Wind Power Project have been registered with IREDA for Generation Based Incentive.</li>
                                        </ul>
    <img class="img-responsive" src="images/khirvire.jpg" />
                                        <br />
                                        <br />
                                        <div class="table-responsive">
<table class="table table-bordered foo-data-table" style="background: #f2fafa;color: #009eda;font-weight: 600;">
<thead>
<tr>

<th colspan="2" data-hide="all" style="
    text-align: center;font-weight:600;
">Daily</th>

<th colspan="2" style="
    text-align: center;font-weight:600;
">Monthly</th>



<th class="td-center" colspan="2" style="
    text-align: center;font-weight:600;
">Yearly</th>


</tr>
</thead>
<tbody>
<tr>

<td>MoU Target</td>
<td>Actual Generation(MU)</td>
<td>MoU Target</td>
<td>Actual Generation(MU)</td>
<td>MoU Target</td>
<td>Actual Generation(MU)</td>



</tr>
<tr>
<!--<td style="
    padding: 0;
    margin: 0;
    margin-top: 9px;
    display: block;
">  </td> -->
<td><input name="khabsatluj3" type="text" id="khabsatluj3" class="form-control" disabled="disabled" style="width:129px" runat="server">  </td>
<td><input name="khabspliti3" type="text" id="khabspliti3" class="form-control" disabled="disabled" runat="server"></td>
<td><input name="powari3" type="text" id="powari3" class="form-control" disabled="disabled" style="width:129px" runat="server">  </td>
<td><input name="wangtoo3" type="text" id="wangtoo3" class="form-control" disabled="disabled" runat="server">  </td>
<td><input name="dkhabsatluj3" type="text" id="dkhabsatluj3" class="form-control" disabled="disabled" style="width:129px" runat="server"></td>
<td><input name="dkhabspiti3" type="text" id="dkhabspiti3" class="form-control" disabled="disabled" runat="server">  </td>




</tr>
    


</tbody>
</table>
             </div>
                                         <div>
    Datea Record Time For-> &nbsp: <asp:Label ID="lbltime3" runat="server" Text="Label" ForeColor="Red" Font-Bold="true"></asp:Label>
</div>

                                    </div> 
                                    
                                    <div class="tab-pane" id="settings">
                                       
      <h3>Charanka 5 MW Solar PV Power Project </h3>
                                        <ul>
      <li>SJVN has commissioned its first solar Plant i.e. 5MW Charanka Solar Power Project on 31.03.2017 in Charanka Solar Park in Patan district, Gujarat, India.
          </li>
<li>The project has been developed under REC Mechanism and the PPA for supply of power has been signed with Gujarat Urja Vikas Nigam Ltd.
</li>
                                        </ul>
        <img class="img-responsive" src="images/cspp.jpg" />
                                        <br />
                                         <br />
                                        <div class="table-responsive">
<table class="table table-bordered foo-data-table" style="background: #f2fafa;color: #009eda;font-weight: 600;">

<thead>
<tr>

<th colspan="2" data-hide="all" style="
    text-align: center; font-weight:600;
">Daily</th>

<th colspan="2" style="
    text-align: center;font-weight:600;
">Monthly</th>



<th class="td-center" colspan="2" style="
    text-align: center; font-weight:600;
">Yearly</th>


</tr>
</thead>
<tbody>
<tr>

<td>MoU Target</td>
<td>Actual Generation(MU)</td>
<td>MoU Target</td>
<td>Actual Generation(MU)</td>
<td>MoU Target</td>
<td>Actual Generation(MU)</td>



</tr>
<tr>
<!--<td style="
    padding: 0;
    margin: 0;
    margin-top: 9px;
    display: block;
">  </td> -->
<td><input name="khabsatluj4" type="text" id="khabsatluj4" class="form-control" disabled="disabled"  style="width:129px" runat="server">  </td>
<td><input name="khabspliti4" type="text" id="khabspliti4" class="form-control" disabled="disabled" runat="server"></td>
<td><input name="powari4" type="text" id="powari4" class="form-control" disabled="disabled" style="width:129px" runat="server">  </td>
<td><input name="wangtoo4" type="text" id="wangtoo4" class="form-control" disabled="disabled" runat="server">  </td>
<td><input name="dkhabsatluj4" type="text" id="dkhabsatluj4" class="form-control" disabled="disabled" style="width:129px" runat="server"></td>
<td><input name="dkhabspiti4" type="text" id="dkhabspiti4" class="form-control" disabled="disabled" runat="server">  </td>




</tr>
    


</tbody>
</table>
             </div>
 <div>
    Datea Record Time For-> &nbsp: <asp:Label ID="lbltime4" runat="server" Text="Label" ForeColor="Red" Font-Bold="true"></asp:Label>
</div>
                                    </div> 
                                </div> 
                            </div>
                        </div>
<div class="row">
    <%--<div class="col-md-12">
        <div class="widget-wrap">
            <div class="widget-header block-header margin-bottom-0 clearfix">
                <div class="pull-left">
                    <h3> Map Locations</h3>
                   <asp:Label ID="lblnotification" runat="server" Text="" style="display:none"></asp:Label>
               </div>
               
            </div>
            <div class="widget-container margin-top-0">
                <div class="widget-content">
				<div class="col-md-12 col-sm-12 col-xs-12 responsive-img">
                    <img src="images/map.jpg" style="
    width: 100%;
" />
				</div>
                  <!--   <div id="regional-visitors" class="regional-analytics">
                    </div> -->
                    <%--<div class="regional-map-stats">
                        <div class="row">
                            <div class="col-md-4 col-sm-4 border-right">
                                <div class="map-stats-container">
                                    <h3>Most Visited</h3>
                                    <div class="progress">
                                        <div class="progress-bar p-most-visited" data-progress="60">
                                        </div>
                                    </div>
                                    <div class="visit-progress-info">
                                        <span class="v-more"><i class="zmdi zmdi-long-arrow-up"></i></span> <span class="v-percent">30%</span> More than last month
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 border-right">
                                <div class="map-stats-container">
                                    <h3>Regular Visited</h3>
                                    <div class="progress">
                                        <div class="progress-bar p-reg-visited" data-progress="80">
                                        </div>
                                    </div>
                                    <div class="visit-progress-info">
                                        <span class="v-more"><i class="zmdi zmdi-long-arrow-up"></i></span> <span class="v-percent">60%</span> More than last month
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 border-right">
                                <div class="map-stats-container">
                                    <h3>Repeated Visited</h3>
                                    <div class="progress">
                                        <div class="progress-bar p-rep-visited" data-progress="40">
                                        </div>
                                    </div>
                                    <div class="visit-progress-info">
                                        <span class="v-less"><i class="zmdi zmdi-long-arrow-down"></i></span> <span class="v-percent">20%</span> Less than last month
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>--%>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
<div class="col-md-7">
    <div class="widget-wrap" style="visibility:hidden">
        <div class="widget-header block-header margin-bottom-0 clearfix">
            <div class="pull-left">
                <h3>Display Links</h3>
                <asp:Label ID="lblnotification" runat="server" Text="" Visible="false"></asp:Label>
                <!-- <p>Examples of notifications styles</p> -->
            </div>
        
        </div>
        <div class="widget-container">
            <div class="widget-content">
                
                
                <div class="alert alert-success" role="alert">
                   <%-- <asp:Label ID="urllink1" runat="server" Text="" style="display:none"></asp:Label>--%>
                 <%-- <asp:Label ID="lbllink1" runat="server" Text="" style="display:none"></asp:Label>
                    <a href="<%= urllink1.Text %>" class="alert-link"><strong><%= lbllink1.Text %></strong> </a>.--%>
                </div>
                <div class="alert alert-info" role="alert">
                  <%-- <asp:Label ID="urllink2" runat="server" Text="" style="display:none"></asp:Label>--%>
                    <%--<asp:Label ID="lbllink2" runat="server" Text="" style="display:none"></asp:Label>
                    <a href="<%= urllink2.Text %>" class="alert-link"><strong><%= lbllink2.Text %></strong> </a>.--%>
                </div>
                <div class="alert alert-warning" role="alert">
                   <%-- <asp:Label ID="urllink3" runat="server" Text="" style="display:none"></asp:Label>--%>
                   <%-- <asp:Label ID="lbllink3" runat="server" Text="" style="display:none"></asp:Label>
                    <a href="<%= urllink3.Text %>" class="alert-link"><strong><%= lbllink3.Text %></strong> </a>.--%>

                </div>
                <div class="alert alert-danger margin-bottom-0" role="alert">
                   <%-- <asp:Label ID="urllink4" runat="server" Text="" style="display:none"></asp:Label>--%>
                   <%-- <asp:Label ID="lbllink4" runat="server" Text="" style="display:none"></asp:Label>
                    <a href="<%= urllink4.Text %>" class="alert-link"><strong><%= lbllink4.Text %></strong> </a>.--%>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-md-5">
<div class="widget-wrap" style="visibility:hidden">
<div class="widget-header block-header margin-bottom-0 clearfix">
    <div class="pull-left">
        <h3>Feedback</h3>
        <!-- <p>A todo list with collapsible details</p> -->
    </div>
</div>
<div class="widget-container margin-top-0">
<div class="widget-content">
<div class="task-widget"  style="visibility:hidden">
<div class="widget-task-list todo-tasklist">
<div class="task-entry">
    <div class="task-intro">
        <div class="task-action">
            <input class="task-i-check" type="checkbox">
        </div>
        <div class="task-title">
            <asp:Label ID="lblfeedback1" runat="server" Text=""></asp:Label>
        </div>
    </div>
   
</div>
<div class="task-entry">
    <div class="task-intro">
        <div class="task-action">
            <input class="task-i-check" type="checkbox">
        </div>
        <div class="task-title">
           <asp:Label ID="lblfeedbback2" runat="server" Text=""></asp:Label>
        </div>
    </div>

</div>
<div class="task-entry">
    <div class="task-intro">
        <div class="task-action">
            <input class="task-i-check" type="checkbox">
        </div>
        <div class="task-title">
          <asp:Label ID="lblfeedback3" runat="server" Text=""></asp:Label>
        </div>
    </div>

</div>
<div class="task-entry">
    <div class="task-intro">
        <div class="task-action">
            <input class="task-i-check" type="checkbox">
        </div>
        <div class="task-title">
         <asp:Label ID="lblfeedback4" runat="server" Text=""></asp:Label>
        </div>
    </div>

</div>


</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<!--Footer Start Here -->
<footer class="footer-container">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <div class="footer-left">
                 
                </div>
            </div>
            <div class="col-md-6 col-sm-6">
                <div class="footer-right">
                    
                </div>
            </div>
        </div>
    </div>
</footer>
<!--Footer End Here -->
</section>
<!--Page Container End Here-->
<!--Rightbar Start Here-->
<aside class="rightbar">
<div class="user-profile-container">
            <div class="user-profile clearfix">
                <div class="admin-user-thumb">
                    <img src="images/Dummy.jpg" alt="admin">
                </div>
                <div class="admin-user-info">
                    <ul>
                        <%-- <% if(Session["RoleId"].ToString()=="1") { %>
                        <li><a href="#"> Super Admin Panel</a></li>
                       <% } %>
                         <% if(Session["RoleId"].ToString()=="2") { %>
                        <li><a href="#"> Admin Panel</a></li>
                       <% } %>
                         <% if(Session["RoleId"].ToString()=="3") { %>
                        <li><a href="#">Employee Panel</a></li>
                       <% } %>
                         <% if(Session["RoleId"].ToString()=="4") { %>
                        <li><a href="#"> User Panel</a></li>
                       <% } %>--%>
                         <%= Session["Name"].ToString()%>

                    </ul>
                </div>
            </div>
            <div class="admin-bar">
                <ul>
                    <li><a href="LogOut.aspx"><i class="zmdi zmdi-power"></i>
                    </a>
                    </li>
                  
                    
                </ul>
            </div>
        </div>
</aside>
<!--Rightbar End Here-->
<script src="js/lib/jquery.js"></script>
<script src="js/lib/jquery-migrate.js"></script>
<script src="js/lib/bootstrap.js"></script>
<script src="js/lib/jquery.ui.js"></script>
<script src="js/lib/jRespond.js"></script>
<script src="js/lib/nav.accordion.js"></script>
<script src="js/lib/hover.intent.js"></script>
<script src="js/lib/hammerjs.js"></script>
<script src="js/lib/jquery.hammer.js"></script>
<script src="js/lib/jquery.fitvids.js"></script>
<script src="js/lib/scrollup.js"></script>
<script src="js/lib/smoothscroll.js"></script>
<script src="js/lib/jquery.slimscroll.js"></script>
<script src="js/lib/jquery.syntaxhighlighter.js"></script>
<script src="js/lib/velocity.js"></script>
<script src="js/lib/jquery-jvectormap.js"></script>
<script src="js/lib/jquery-jvectormap-world-mill.js"></script>
<script src="js/lib/jquery-jvectormap-us-aea.js"></script>
<script src="js/lib/smart-resize.js"></script>
<!--iCheck-->
<script src="js/lib/icheck.js"></script>
<script src="js/lib/jquery.switch.button.js"></script>
<!--CHARTS-->
<script src="js/lib/chart/sparkline/jquery.sparkline.js"></script>
<script src="js/lib/chart/easypie/jquery.easypiechart.min.js"></script>
<script src="js/lib/chart/flot/excanvas.min.js"></script>
<script src="js/lib/chart/flot/jquery.flot.min.js"></script>
<script src="js/lib/chart/flot/curvedLines.js"></script>
<script src="js/lib/chart/flot/jquery.flot.time.min.js"></script>
<script src="js/lib/chart/flot/jquery.flot.stack.min.js"></script>
<script src="js/lib/chart/flot/jquery.flot.axislabels.js"></script>
<script src="js/lib/chart/flot/jquery.flot.resize.min.js"></script>
<script src="js/lib/chart/flot/jquery.flot.tooltip.min.js"></script>
<script src="js/lib/chart/flot/jquery.flot.spline.js"></script>
<script src="js/lib/chart/flot/jquery.flot.pie.min.js"></script>
<!--Forms-->
<script src="js/lib/jquery.maskedinput.js"></script>
<script src="js/lib/jquery.validate.js"></script>
<script src="js/lib/jquery.form.js"></script>
<script src="js/lib/j-forms.js"></script>
<script src="js/lib/jquery.loadmask.js"></script>
<script src="js/lib/vmap.init.js"></script>
<script src="js/lib/theme-switcher.js"></script>
<script src="js/apps.js"></script>
</body>
    </form>
</html>

