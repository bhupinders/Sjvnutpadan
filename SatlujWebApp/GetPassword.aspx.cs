﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

namespace SatlujWebApp
{
    public partial class GetPassword : System.Web.UI.Page
    {
        string emailquerystring = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            emailquerystring = Request.QueryString["emil"].ToString();
        }
         public string GetConnectionString()
        {
            return System.Configuration.ConfigurationManager.ConnectionStrings["Local"].ConnectionString;
        }

         protected void btnsignin_Click(object sender, EventArgs e)
         {
             SqlConnection con = new SqlConnection(GetConnectionString());
             SqlCommand cmd = new SqlCommand("update tbllogin set password=@password where emailid=@email",con);
             cmd.CommandType = CommandType.Text;
             con.Open();
             cmd.Parameters.AddWithValue("@password", Request.Form["password"].ToString());
             cmd.Parameters.AddWithValue("@email", emailquerystring);
             cmd.ExecuteNonQuery();
             cmd.Dispose();
             con.Close();
             lblmessage.Text = "password update. Please login !";
             password.Value = "";
             ConfirmPassword.Value = "";
             btnsignin.Enabled = false;
         }
    }
}