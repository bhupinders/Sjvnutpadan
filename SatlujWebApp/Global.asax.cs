﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;

namespace SatlujWebApp
{
    public class Global : System.Web.HttpApplication
    {
        void Session_Start(object sender, EventArgs e)
        {
            Application.Lock();
            Application["NoOfVisitors"] = 300+(int)Application["NoOfVisitors"] + 1;
            Application.UnLock();
            Session.Timeout = 18000;
        }
        protected void Application_Start(object sender, EventArgs e)
        {
            Application["NoOfVisitors"] = 0;
        }

     
        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}