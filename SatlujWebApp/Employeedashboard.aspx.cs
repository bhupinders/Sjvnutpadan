﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SatlujWebApp
{
    public partial class Employeedashboard : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            txtdtp.Enabled = false;
            if(Session["username"].ToString()==""||Session["username"].ToString()=="")
            {
                Response.Redirect("login.aspx");
               }
            if(!Page.IsPostBack)
            {
                datepicker.EnableViewState = false;
                SqlConnection con1 = new SqlConnection(GetConnectionString());
                SqlCommand cmdcount = new SqlCommand("select count(*) from tblnotificationurja", con1);
                con1.Open();
                int countnotification = (int)cmdcount.ExecuteScalar();
                lblnotificationcount.Text = countnotification.ToString();
                cmdcount.Dispose();
                con1.Close();
                GetDaily();
                getNotification();
                BindDropDown();
                BindGrid();
            }
        }
        public void BindDropDown()
        {
            SqlConnection con = new SqlConnection(GetConnectionString());
            SqlCommand cmd = new SqlCommand("select * from [TblLocationUrja]", con);
            con.Open();
            cmd.CommandType = System.Data.CommandType.Text;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            DropDownList1.DataSource = ds;
            DropDownList1.DataTextField = "ProjectName";
            DropDownList1.DataValueField = "ProjectName";
            DropDownList1.DataBind();
            da.Dispose();
            cmd.Dispose();
            con.Close();
        }
        protected void btnsave_Click(object sender, EventArgs e)
        {
            string[] values;
            string GetTIme = txtdtp.Text;
            if (GetTIme.Contains("-"))
            {
                values = GetTIme.Split('-');
            }
            else
            {
                values = GetTIme.Split('/');
            }

            string TIme = values[1] + "/" + values[0] + "/" + values[2];



            SqlConnection con = new SqlConnection(GetConnectionString());
            SqlCommand getdata = new SqlCommand("select * from TblGeneration where Convert(varchar,CAST(TimeString AS DATETIME),101)=@getdate and projectname=@projectname", con);
            getdata.CommandType = CommandType.Text;
            con.Open();
            getdata.Parameters.AddWithValue("@getdate", TIme.ToString());
            getdata.Parameters.AddWithValue("@projectname", DropDownList1.SelectedItem.Text);
            SqlDataReader getrecord = getdata.ExecuteReader();
            if (getrecord.HasRows)
            {
                lblmessage.Text = "Data is already inserted for this date!";
                con.Close();
              
            }
            else
            {

                SqlCommand cmd = new SqlCommand("insert into TblGeneration(projectname,DailyMouTarget,DailyActualGeneration,TimeString,EmailId) values(@projectname,@khabsatluj,@khabspliti,@timestring,@EmailId)", con);

                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@projectname", DropDownList1.SelectedItem.Text);
                cmd.Parameters.AddWithValue("@khabsatluj", Request.Form["khabsatluj"].ToString());
                cmd.Parameters.AddWithValue("@khabspliti", Request.Form["khabspliti"].ToString());
                cmd.Parameters.AddWithValue("@timestring", TIme.ToString());
                cmd.Parameters.AddWithValue("@EmailId", Session["Email"].ToString());
                cmd.ExecuteNonQuery();
                cmd.Dispose();
                foreach (Control control in form1.Controls)
                {
                    if (control is TextBox)
                    {
                        TextBox textBox = (TextBox)control;
                        textBox.Text = null;
                    }
                }
                lblmessage.Text = "Data Inserted!";
                lblmessage.ForeColor = System.Drawing.Color.Green;
                con.Close();
            }
            BindGrid();
            GetDaily();
            datepicker.EnableViewState = false;
            }
        public void getNotification()
        {
            SqlConnection con1 = new SqlConnection(GetConnectionString());
            SqlCommand cmdcount = new SqlCommand("select count(*) from tblnotificationurja", con1);
            con1.Open();
            int countnotification = (int)cmdcount.ExecuteScalar();
            lblnotificationcount.Text = countnotification.ToString();
            if (countnotification == 1)
            {
                SqlConnection con = new SqlConnection(GetConnectionString());
                SqlCommand cmd = new SqlCommand("select top(1) notification from [tblnotificationurja] order by id desc", con);
                con.Open();
                cmd.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dr.Read();
                    notification1.Text = dr["notification"].ToString();
                    lblnotification.Text = dr["notification"].ToString();
                }

                dr.Dispose();
                cmd.Dispose();
                con.Close();
                con.Close();
            }

            if (countnotification == 2)
            {
                SqlConnection con = new SqlConnection(GetConnectionString());
                SqlCommand cmd = new SqlCommand("select top(1) notification from [tblnotificationurja] order by id desc", con);
                con.Open();
                cmd.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dr.Read();
                    notification1.Text = dr["notification"].ToString();
                    lblnotification.Text = dr["notification"].ToString();
                }

                dr.Dispose();
                cmd.Dispose();

                SqlCommand cmd1 = new SqlCommand("SELECT id, notification FROM [tblnotificationurja] WHERE Id NOT IN (SELECT MAX( Id )FROM [tblnotificationurja] ) order by id desc", con);

                cmd1.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr1 = cmd1.ExecuteReader();
                if (dr1.HasRows)
                {
                    dr1.Read();

                    notification2.Text = dr1["notification"].ToString();
                }
                dr1.Dispose();
                cmd1.Dispose();
                con.Close();

            }
            if (countnotification >= 3)
            {
                SqlConnection con = new SqlConnection(GetConnectionString());
                SqlCommand cmd = new SqlCommand("select top(1) notification from [tblnotificationurja] order by id desc", con);
                con.Open();
                cmd.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dr.Read();
                    notification1.Text = dr["notification"].ToString();
                    lblnotification.Text = dr["notification"].ToString();
                }

                dr.Dispose();
                cmd.Dispose();

                SqlCommand cmd1 = new SqlCommand("SELECT id, notification FROM [tblnotificationurja] WHERE Id NOT IN (SELECT MAX( Id )FROM [tblnotificationurja] ) order by id desc", con);

                cmd1.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr1 = cmd1.ExecuteReader();
                if (dr1.HasRows)
                {
                    dr1.Read();

                    notification2.Text = dr1["notification"].ToString();
                }
                dr1.Dispose();
                cmd1.Dispose();

                SqlCommand cmd2 = new SqlCommand("SELECT TOP 1 * From(select Top 3 * from [tblnotificationurja] ORDER BY Id DESC) x ORDER BY Id", con);

                cmd2.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr2 = cmd2.ExecuteReader();
                if (dr2.HasRows)
                {
                    dr2.Read();
                    notification3.Text = dr2["notification"].ToString();
                }
                dr2.Dispose();
                cmd2.Dispose();
                con.Close();
            }
            con1.Close();
            cmdcount.Dispose();

        }
        public string GetConnectionString()
        {
            return System.Configuration.ConfigurationManager.ConnectionStrings["Local"].ConnectionString;
        }
        protected void datepicker_SelectionChanged(object sender, EventArgs e)
        {
            txtdtp.Text = datepicker.SelectedDate.ToShortDateString();
            datepicker.Visible = false;
        }
        protected void lnkpickdate_Click(object sender, EventArgs e)
        {
            datepicker.Visible = true;
        }
        public void GetDaily()
        {
            string day = DateTime.Today.ToString("MM-dd-yyyy", CultureInfo.InvariantCulture);
            string[] restr = day.Split('-');
            String sDate = DateTime.Now.ToString();
            DateTime datevalue = (Convert.ToDateTime(sDate.ToString()));

            String dy = restr[1].ToString();
            String mn = restr[0].ToString();
            String yy = datevalue.Year.ToString();
            string stringdate = mn + "/" + dy + "/" + yy;
            string gettime = mn + "/" + "01" + "/" + yy;
            SqlConnection con = new SqlConnection(GetConnectionString());
            SqlCommand cmd = new SqlCommand("select Sum(cast(dailyMoutarget as float )) as month from [TblGeneration] where CONVERT(datetime,timestring)>=@date  and projectname=@projectname", con);
            cmd.CommandType = CommandType.Text;
            con.Open();
            cmd.Parameters.AddWithValue("@date", gettime.ToString());
           // cmd.Parameters.AddWithValue("@date1", stringdate.ToString());
            cmd.Parameters.AddWithValue("@projectname", DropDownList1.SelectedItem.Text);
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                dr.Read();
                powari.Value = dr["month"].ToString();
            }
            dr.Dispose();
            cmd.Dispose();

            SqlCommand cmd1 = new SqlCommand("select Sum(cast(DailyActualGeneration as float )) as month from [TblGeneration] where CONVERT(datetime,timestring)>=@date1  and projectname=@projectname", con);
            cmd1.CommandType = CommandType.Text;          
            cmd1.Parameters.AddWithValue("@date1", gettime.ToString());
           // cmd1.Parameters.AddWithValue("@date", stringdate.ToString());
            cmd1.Parameters.AddWithValue("@projectname", DropDownList1.SelectedItem.Text);
            SqlDataReader dr1 = cmd1.ExecuteReader();
            if (dr1.HasRows)
            {
                dr1.Read();
                wangtoo.Value = dr1["month"].ToString();
            }
            dr1.Dispose();
            cmd1.Dispose();

            SqlCommand cmd2 = new SqlCommand("select Sum(cast(dailyMoutarget as float )) as month from [TblGeneration] where CONVERT(datetime,timestring)>=@date1 and  projectname=@projectname", con);
            cmd2.CommandType = CommandType.Text;
            cmd2.Parameters.AddWithValue("@date1", "04/01/2017");
           // cmd2.Parameters.AddWithValue("@date", stringdate.ToString());
            cmd2.Parameters.AddWithValue("@projectname", DropDownList1.SelectedItem.Text);
            SqlDataReader dr2 = cmd2.ExecuteReader();
            if (dr2.HasRows)
            {
                dr2.Read();
                dkhabsatluj.Value = dr2["month"].ToString();
            }
            dr2.Dispose();
            cmd2.Dispose();

            SqlCommand cmd3 = new SqlCommand("select Sum(cast(DailyActualGeneration as float )) as month from [TblGeneration] where CONVERT(datetime,timestring)>=@date1  and projectname=@projectname", con);
            cmd3.CommandType = CommandType.Text;
            cmd3.Parameters.AddWithValue("@date1", "04/01/2017");
           // cmd3.Parameters.AddWithValue("@date", stringdate.ToString());
            cmd3.Parameters.AddWithValue("@projectname", DropDownList1.SelectedItem.Text);
            SqlDataReader dr3 = cmd3.ExecuteReader();
            if (dr3.HasRows)
            {
                dr3.Read();
                dkhabspiti.Value = dr3["month"].ToString();
            }
            dr3.Dispose();
            cmd3.Dispose();
            con.Close();
        }
        public void BindGrid()
        {
            string day = DateTime.Today.ToString("MM-dd-yyyy", CultureInfo.InvariantCulture);
            string[] restr = day.Split('-');
            String sDate = DateTime.Now.ToString();
            DateTime datevalue = (Convert.ToDateTime(sDate.ToString()));

            String dy = restr[1].ToString();
            String mn = restr[0].ToString();
            String yy = datevalue.Year.ToString();
            string gettime=mn+"/"+dy+"/"+yy;

            string day1 = DateTime.Today.AddDays(-10).ToString("MM-dd-yyyy", CultureInfo.InvariantCulture);
            string[] restr1 = day1.Split('-');
            String sDate1 = DateTime.Now.AddDays(-10).ToString();
            DateTime datevalue1 = (Convert.ToDateTime(sDate1.ToString()));

            String dy1 = restr1[1].ToString();
            String mn1 = restr1[0].ToString();
            String yy1 = datevalue1.Year.ToString();
            string gettime1=mn1+"/"+dy1+"/"+yy1;

            SqlConnection con = new SqlConnection(GetConnectionString());
            SqlCommand cmd = new SqlCommand("select * from [TblGeneration] where CONVERT(datetime,timestring)>= @date1 and CONVERT(datetime,timestring)<=@date and projectname=@projectname", con);
            con.Open();
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.Parameters.AddWithValue("@date1", gettime1.ToString());
            cmd.Parameters.AddWithValue("@date", gettime.ToString());
            cmd.Parameters.AddWithValue("@projectname", DropDownList1.SelectedItem.Text);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            GridView1.DataSource = ds;
            GridView1.DataBind();
            con.Close();
            GetDaily();
        }

       

        protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GridView1.EditIndex = e.NewEditIndex;
            BindGrid();
            GetDaily();
        }

        protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            Label id = GridView1.Rows[e.RowIndex].FindControl("lblid") as Label;
            TextBox SplitPPM1 = GridView1.Rows[e.RowIndex].FindControl("SplitPPM1") as TextBox;
            TextBox SplitPPM2 = GridView1.Rows[e.RowIndex].FindControl("SplitPPM2") as TextBox;
            TextBox SplitPPM3 = GridView1.Rows[e.RowIndex].FindControl("SplitPPM3") as TextBox;
            //TextBox SplitPPM4 = GridView1.Rows[e.RowIndex].FindControl("SplitPPM4") as TextBox;
            //TextBox DischargeKhab1 = GridView1.Rows[e.RowIndex].FindControl("DischargeKhab1") as TextBox;
            //TextBox DischargeKhab2 = GridView1.Rows[e.RowIndex].FindControl("DischargeKhab2") as TextBox;
            //TextBox DischargeNathpa = GridView1.Rows[e.RowIndex].FindControl("DischargeNathpa") as TextBox;

            SqlConnection con = new SqlConnection(GetConnectionString());
            con.Open();
            //updating the record  
            SqlCommand cmd = new SqlCommand("Update TblGeneration set ProjectName='" + SplitPPM1.Text + "',DailyMouTarget='" + SplitPPM2.Text + "',DailyActualGeneration='" + SplitPPM3.Text + "' where ID=" + Convert.ToInt32(id.Text), con);
            cmd.ExecuteNonQuery();
            con.Close();
            //Setting the EditIndex property to -1 to cancel the Edit mode in Gridview  
            GridView1.EditIndex = -1;
            con.Close();
            //Call ShowData method for displaying updated data  
            BindGrid();
            GetDaily();
        }

        protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GridView1.EditIndex = -1;
            BindGrid();
            GetDaily();
        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindGrid();
            GetDaily();
        }

        protected void datepicker_DayRender(object sender, DayRenderEventArgs e)
        {
            if (e.Day.Date > DateTime.Today)
            {
                e.Day.IsSelectable = false;
            }
        }
    }
}