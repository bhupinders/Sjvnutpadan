﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="SatlujWebApp.Login" %>

<!DOCTYPE html>

<html>
<title>Homepage</title>
<head>
 <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="css/bootstrap.min.css" rel="stylesheet" media="all" type="text/css">
    <link href="css/style.css" rel="stylesheet" media="all" type="text/css">

    <link href="css/animate.css" rel="stylesheet" media="all" type="text/css">
    <link href="css/bootstrap-dropdownhover.css" rel="stylesheet" media="all" type="text/css">

     <!-- Bootstrap Dropdown Hover CSS -->
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/bootstrap-dropdownhover.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="js/jquery-3.2.1.min.js" type="text/javascript"></script> 
<script src="js/bootstrap.min.js" type="text/javascript"></script> 
<script src="js/bootstrap-dropdownhover.js" type="text/javascript"></script> 
    <script>

        function get() {
            // Load the SDK Asynchronously
            (function (d) {
                debugger;
                var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
                if (d.getElementById(id)) { return; }
                js = d.createElement('script'); js.id = id; js.async = true;
                js.src = "//connect.facebook.net/en_US/all.js";
                ref.parentNode.insertBefore(js, ref);
            }(document));

            // Init the SDK upon load
            window.fbAsyncInit = function () {
                FB.init({
                    appId: '1745025375716484', // App ID
                    channelUrl: '//' + window.location.hostname + '/channel', // Path to your Channel File
                    status: true, // check login status
                    cookie: true, // enable cookies to allow the server to access the session
                    xfbml: true  // parse XFBML
                });




                // listen for and handle auth.statusChange events
                FB.Event.subscribe('auth.statusChange', function (response) {
                    if (response.authResponse) {
                        // user has auth'd your app and is logged into Facebook
                        FB.api('/me?fields=id,name,email', function (me) {
                            if (me.email) {
                                document.getElementById('auth-displayname').innerHTML = me.email;
                                document.getElementById("lblmessage1").innerHTML = document.getElementById('auth-displayname').innerHTML;
                                document.getElementById("lblmessage1").style.display = "none";
                                document.getElementById("lblmessage2").innerHTML = me.id;
                                document.getElementById("lblmessage2").style.display = "none";
                                document.getElementById("lblmessage3").innerHTML = me.name;
                                document.getElementById("lblmessage3").style.display = "none";

                                $.ajax({

                                    type: "POST",
                                    url: "Login.aspx/Get",
                                    data: '{Email: "' + $('#<%= lblmessage1.ClientID %>').text() + '",Id: "' + $('#<%= lblmessage2.ClientID %>').text() + '" ,Name: "' + $('#<%= lblmessage3.ClientID %>').text() + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (res) {
            debugger;
            // Redirect to personal_profile.aspx passing it the ID we got back from the web method call
            window.location = "dashboardsatluj.aspx?username=" + res.d;
        }
    });

}

    })
    document.getElementById('auth-loggedout').style.display = 'none';
    document.getElementById('auth-loggedin').style.display = 'block';
} else {
    // user has not auth'd your app, or is not logged into Facebook
    document.getElementById('auth-loggedout').style.display = 'block';
    document.getElementById('auth-loggedin').style.display = 'none';
}
});
    //$("#auth-logoutlink").click(function() { FB.logout(function() { window.location.reload(); }); });
}
}
</script>
    
</head>
    <body>
        <form runat="server" id="form1">
    <div class="container">
<div class="col-md-5 col-sm-5 col-xs-12 splash-box ">
<div class="login-container shadow-z-1">
<div class="row">
	<div class="col-md-6 col-sm-6 col-xs-6 arrow-icon" style="
    text-align: left;
    padding-left: 5%;
    color: #009eda;
">
	
	</div>
	<div class="col-md-6 col-sm-6 col-xs-12" style="
    float: right;
    padding-right: 5%;
    color: #009eda;
">
	<h5 style="
    float: right;
    text-align: right;
"><a style="text-align: right; float: right;" href="RegisterUser.aspx">Sign Up</a></h5>
	</div>
</div>
<div class="login-box">
    <img src="images/login-logo.png" />
    

</div>
<div class="login-form">
    <span>
        <asp:Label ID="lblinfo" runat="server" Text="" ForeColor="Red"></asp:Label></span>
<div class="input-group">
  <span class="input-group-addon" id="sizing-addon2"><i class="fa fa-user" aria-hidden="true"></i></span>
  <asp:Label ID="lblmessage1" runat="server" Text=""></asp:Label>
     <asp:Label ID="lblmessage2" runat="server" Text=""></asp:Label>
      <asp:Label ID="lblmessage3" runat="server" Text=""></asp:Label>
  <input type="text" class="form-control" placeholder="Email/Phone" aria-describedby="sizing-addon2" id="txtusername" name="txtusername">
</div>
<div class="input-group">
  <span class="input-group-addon" id="sizing-addon2"><i class="fa fa-unlock-alt" aria-hidden="true"></i>
</span>
  <input type="password" class="form-control" placeholder="Password" aria-describedby="sizing-addon2" id="txtpassword" name="txtpassword">
</div>
&nbsp;&nbsp;<a style="text-align: right; float: right;" href="ForgotPassword.aspx">Forgot password?</a>
     <asp:Button ID="btnlogin" class="btn btn-info primary-btn" style="
    width: 100%;
    margin-top: 3%;
" runat="server" Text="Log IN" OnClick="btnlogin_Click" />
   <div style="
    display: none;
">
        <asp:Label ID="Label1" runat="server" Text="Total Visit: "></asp:Label><asp:Label ID="lbl1" runat="server" Text="Label"></asp:Label>
    </div>

    <div class="row">
<div class="col-md-6 col-sm-6">
<div class="social-btn twitter">
<i class="fa fa-twitter"></i>
    <asp:Button ID="btngoogle" runat="server" Text="Google"  OnClick="btngoogle_Click" />

</div>
</div>
<div class="col-md-12 col-sm-12" >
<div class="social-btn facebook">
<%--<i class="fa fa-facebook"></i>--%>
<button style="
    width: 100%;
" type="button" onclick="get();">Login with Facebook</button>
</div>

</div>
</div>
    <div>
        <span>Total Visit:&nbsp;<asp:Label ID="lblcount" runat="server" Text="Label"></asp:Label></span>
    </div>
    </form>
     <div id="auth-status">
<div id="auth-loggedout">

<div class="fb-login-button" autologoutlink="true" scope="email,user_checkins" ></div>
</div>
<div id="auth-loggedin" style="display: none">
<span id="auth-displayname" style="display:none"></span><a href="#" id="auth-logoutlink"></a>
</div>
</div>
</div>
</div>
</div>
</div>

        </body>
   
</html>
