﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SatlujWebApp
{
    public partial class ViewData : System.Web.UI.Page
    {
        string currentyear = "";
        protected void Page_Load(object sender, EventArgs e)
        {
           
            lblmessagedate.Visible = false;
            txtdtp.Enabled = false;
      
            if (Session["username"].ToString() == null || Session["username"].ToString() == "")
            {
                Response.Redirect("login.aspx");
            }
            if (!Page.IsPostBack)
            {
                datepicker.EnableViewState = false;
                // BindGrid();
                BindDropDown();
                getNotification();
                getLink();
                SqlConnection con1 = new SqlConnection(GetConnectionString());
                SqlCommand cmdcount = new SqlCommand("select count(*) from tblnotificationurja", con1);
                con1.Open();
                int countnotification = (int)cmdcount.ExecuteScalar();
                lblnotificationcount.Text = countnotification.ToString();
                cmdcount.Dispose();
                con1.Close();
            }
        }


     
        public void getLink()
        {
            SqlConnection con1 = new SqlConnection(GetConnectionString());
            SqlCommand cmdcount = new SqlCommand("select count(*) from  [TblLinksUrja]", con1);
            con1.Open();
            int count = (int)cmdcount.ExecuteScalar();
            if (count == 1)
            {
                SqlConnection con = new SqlConnection(GetConnectionString());
                SqlCommand cmd = new SqlCommand("select top(1) id,name,description,url from [TblLinksUrja] order by id desc", con);
                con.Open();
                cmd.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dr.Read();
                    lbllink4.Text = dr["description"].ToString();
                    urllink4.Text = dr["url"].ToString();
                }
                dr.Dispose();
                cmd.Dispose();
                con.Close();
            }
            if (count == 2)
            {
                SqlConnection con = new SqlConnection(GetConnectionString());
                SqlCommand cmd = new SqlCommand("select top(1) id,name,description,url from [TblLinksUrja] order by id desc", con);
                con.Open();
                cmd.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dr.Read();
                    lbllink4.Text = dr["description"].ToString();
                    urllink4.Text = dr["url"].ToString();
                }
                dr.Dispose();
                cmd.Dispose();
                SqlCommand cmd1 = new SqlCommand("SELECT name,Description,Url FROM [TblLinksUrja] WHERE Id NOT IN (SELECT MAX( Id )FROM [TblLinksUrja] ) order by id desc", con);

                cmd1.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr1 = cmd1.ExecuteReader();
                if (dr1.HasRows)
                {
                    dr1.Read();
                    lbllink3.Text = dr1["description"].ToString();
                    urllink3.Text = dr1["url"].ToString();
                }
                dr1.Dispose();
                cmd1.Dispose();
                con.Close();
            }
            if (count == 3)
            {
                SqlConnection con = new SqlConnection(GetConnectionString());
                SqlCommand cmd = new SqlCommand("select top(1) id,name,description,url from [TblLinksUrja] order by id desc", con);
                con.Open();
                cmd.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dr.Read();
                    lbllink4.Text = dr["description"].ToString();
                    urllink4.Text = dr["url"].ToString();
                }
                dr.Dispose();
                cmd.Dispose();
                SqlCommand cmd1 = new SqlCommand("SELECT name,Description,Url FROM [TblLinksUrja] WHERE Id NOT IN (SELECT MAX( Id )FROM [TblLinksUrja] ) order by id desc", con);

                cmd1.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr1 = cmd1.ExecuteReader();
                if (dr1.HasRows)
                {
                    dr1.Read();
                    lbllink3.Text = dr1["description"].ToString();
                    urllink3.Text = dr1["url"].ToString();
                }
                dr1.Dispose();
                cmd1.Dispose();

                SqlCommand cmd2 = new SqlCommand("SELECT TOP 1 * From(select Top 3 * from [TblLinksUrja] ORDER BY Id DESC) x ORDER BY Id", con);

                cmd2.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr2 = cmd2.ExecuteReader();
                if (dr2.HasRows)
                {
                    dr2.Read();
                    lbllink2.Text = dr2["description"].ToString();
                    urllink2.Text = dr2["url"].ToString();
                }
                dr2.Dispose();
                cmd2.Dispose();
                con.Close();
            }

            if (count >= 4)
            {
                SqlConnection con = new SqlConnection(GetConnectionString());
                SqlCommand cmd = new SqlCommand("select top(1) id,name,description,url from [TblLinksUrja] order by id desc", con);
                con.Open();
                cmd.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dr.Read();
                    lbllink4.Text = dr["description"].ToString();
                    urllink4.Text = dr["url"].ToString();
                }
                dr.Dispose();
                cmd.Dispose();
                SqlCommand cmd1 = new SqlCommand("SELECT name,Description,Url FROM [TblLinksUrja] WHERE Id NOT IN (SELECT MAX( Id )FROM [TblLinksUrja] ) order by id desc", con);

                cmd1.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr1 = cmd1.ExecuteReader();
                if (dr1.HasRows)
                {
                    dr1.Read();
                    lbllink3.Text = dr1["description"].ToString();
                    urllink3.Text = dr1["url"].ToString();
                }
                dr1.Dispose();
                cmd1.Dispose();

                SqlCommand cmd2 = new SqlCommand("SELECT TOP 1 * From(select Top 3 * from [TblLinksUrja] ORDER BY Id DESC) x ORDER BY Id", con);

                cmd2.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr2 = cmd2.ExecuteReader();
                if (dr2.HasRows)
                {
                    dr2.Read();
                    lbllink2.Text = dr2["description"].ToString();
                    urllink2.Text = dr2["url"].ToString();
                }
                dr2.Dispose();
                cmd2.Dispose();

                SqlCommand cmd4 = new SqlCommand("SELECT TOP 1 * From(select Top 4 * from [TblLinksUrja] ORDER BY Id DESC) x ORDER BY Id", con);

                cmd4.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr4 = cmd4.ExecuteReader();
                if (dr4.HasRows)
                {
                    dr4.Read();
                    lbllink1.Text = dr4["description"].ToString();
                    urllink1.Text = dr4["url"].ToString();
                }
                dr4.Dispose();
                cmd4.Dispose();
                con.Close();

            }
            con1.Close();
        }
        public void getNotification()
        {
            SqlConnection con1 = new SqlConnection(GetConnectionString());
            SqlCommand cmdcount = new SqlCommand("select count(*) from tblnotificationurja", con1);
            con1.Open();
            int countnotification = (int)cmdcount.ExecuteScalar();
            lblnotificationcount.Text = countnotification.ToString();
            if (countnotification == 1)
            {
                SqlConnection con = new SqlConnection(GetConnectionString());
                SqlCommand cmd = new SqlCommand("select top(1) notification from [tblnotificationurja] order by id desc", con);
                con.Open();
                cmd.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dr.Read();
                    notification1.Text = dr["notification"].ToString();
                    lblnotification.Text = dr["notification"].ToString();
                }

                dr.Dispose();
                cmd.Dispose();
                con.Close();
            }

            if (countnotification == 2)
            {
                SqlConnection con = new SqlConnection(GetConnectionString());
                SqlCommand cmd = new SqlCommand("select top(1) notification from [tblnotificationurja] order by id desc", con);
                con.Open();
                cmd.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dr.Read();
                    notification1.Text = dr["notification"].ToString();
                    lblnotification.Text = dr["notification"].ToString();
                }

                dr.Dispose();
                cmd.Dispose();

                SqlCommand cmd1 = new SqlCommand("SELECT id, notification FROM [tblnotificationurja] WHERE Id NOT IN (SELECT MAX( Id )FROM [tblnotificationurja] ) order by id desc", con);

                cmd1.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr1 = cmd1.ExecuteReader();
                if (dr1.HasRows)
                {
                    dr1.Read();

                    notification2.Text = dr1["notification"].ToString();
                }
                dr1.Dispose();
                cmd1.Dispose();
                con.Close();

            }
            if (countnotification >= 3)
            {
                SqlConnection con = new SqlConnection(GetConnectionString());
                SqlCommand cmd = new SqlCommand("select top(1) notification from [tblnotificationurja] order by id desc", con);
                con.Open();
                cmd.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dr.Read();
                    notification1.Text = dr["notification"].ToString();
                    lblnotification.Text = dr["notification"].ToString();
                }

                dr.Dispose();
                cmd.Dispose();

                SqlCommand cmd1 = new SqlCommand("SELECT id, notification FROM [tblnotificationurja] WHERE Id NOT IN (SELECT MAX( Id )FROM [tblnotificationurja] ) order by id desc", con);

                cmd1.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr1 = cmd1.ExecuteReader();
                if (dr1.HasRows)
                {
                    dr1.Read();

                    notification2.Text = dr1["notification"].ToString();
                }
                dr1.Dispose();
                cmd1.Dispose();

                SqlCommand cmd2 = new SqlCommand("SELECT TOP 1 * From(select Top 3 * from [tblnotificationurja] ORDER BY Id DESC) x ORDER BY Id", con);

                cmd2.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr2 = cmd2.ExecuteReader();
                if (dr2.HasRows)
                {
                    dr2.Read();
                    notification3.Text = dr2["notification"].ToString();
                }
                dr2.Dispose();
                cmd2.Dispose();
                con.Close();
            }
            con1.Close();
            cmdcount.Dispose();

        }

        public void BindDropDown()
        
        {
            SqlConnection con = new SqlConnection(GetConnectionString());
            SqlCommand cmd = new SqlCommand("select distinct(Projectname) from [TblLocationUrja]", con);
            con.Open();
            cmd.CommandType = System.Data.CommandType.Text;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            DropDownList1.DataSource = ds;
            DropDownList1.DataTextField = "ProjectName";
            DropDownList1.DataValueField = "ProjectName";
            DropDownList1.DataBind();
            da.Dispose();
            cmd.Dispose();
            con.Close();
        }
        public void BindGrid()
        {
            SqlConnection con = new SqlConnection(GetConnectionString());
            SqlCommand cmd = new SqlCommand("select * from [TblGeneration] order by id desc", con);
            con.Open();
            cmd.CommandType = System.Data.CommandType.Text;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            con.Close();
            // GridView1.DataSource = ds;
            //  GridView1.DataBind();

        }
        public string GetConnectionString()
        {
            return System.Configuration.ConfigurationManager.ConnectionStrings["Local"].ConnectionString;
        }

        protected void btnshow_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(GetConnectionString());
            SqlCommand cmd = new SqlCommand("select sum(cast(dailymoutarget as float)) dailymou,sum(cast(DailyActualGeneration as float)) dailyactual from [TblGeneration] where CONVERT(datetime,timestring)=@date and projectname=@projectname", con);
            con.Open();
            string date = txtdtp.Text;
            if (string.IsNullOrEmpty(date))
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Notify", "alert('Please select the date first');", true);
                return;
            }
            DateTime d =Convert.ToDateTime( txtdtp.Text);
            string day = d.ToString("MM-dd-yyyy", CultureInfo.InvariantCulture);
            string[] restr = day.Split('-');

            String dy = restr[1].ToString();
            String mn = restr[0].ToString();
            String yy = restr[2].ToString();
            string gettime = mn + "/" + dy + "/" + yy;
            if (Convert.ToInt32(mn) <= 03)
            {
                currentyear = Convert.ToString(Convert.ToInt32(yy) - 1);
            }
            else
            {
                currentyear = yy;
            }


            cmd.Parameters.AddWithValue("@date", gettime);
            cmd.Parameters.AddWithValue("@projectname", DropDownList1.SelectedItem.Text);
           SqlDataReader dr=cmd.ExecuteReader();
           if (dr.HasRows)
           {
               dr.Read();
               Projectname.Value = DropDownList1.SelectedItem.Text;
               if (dr["dailymou"].ToString() != "" || dr["dailyactual"].ToString() != "")
               {
                   khabsatluj.Value = String.Format("{0:0.0000}", Convert.ToDecimal(dr["dailymou"].ToString()));
                   khabspliti.Value = String.Format("{0:0.0000}", Convert.ToDecimal(dr["dailyactual"].ToString()));

               }
               else
               {
                   khabsatluj.Value = "0.0";
                   khabspliti.Value = "0.0";
               }
           }


            dr.Dispose();
            cmd.Dispose();
           // txtdtp.Text = "";


            SqlCommand cmd1 = new SqlCommand("  select sum(cast(dailymoutarget as float)) MonthlyMouTarget, sum(cast(DailyActualGeneration as float)) MonthlyActualTarget from [TblGeneration] where CONVERT(datetime,timestring)>=@datemonth and CONVERT(datetime,timestring)<=@date and projectname=@projectname", con);
            
            string date1 = txtdtp.Text;
            string input = date1.Substring(0, date1.IndexOf("-") + 1);
            string datemonth = mn + "/" + "01/" + yy;
            cmd1.Parameters.AddWithValue("@datemonth", datemonth);
            cmd1.Parameters.AddWithValue("@date", gettime);
            cmd1.Parameters.AddWithValue("@projectname", DropDownList1.SelectedItem.Text);
            SqlDataReader dr1 = cmd1.ExecuteReader();
            if (dr1.HasRows)
            {
                dr1.Read();
                Projectname.Value = DropDownList1.SelectedItem.Text;
                if (dr1["MonthlyMouTarget"].ToString() != "" || dr1["MonthlyActualTarget"].ToString() != "")
                {
                    powari.Value = String.Format("{0:0.0000}", Convert.ToDecimal(dr1["MonthlyMouTarget"].ToString()));
                    wangtoo.Value = String.Format("{0:0.0000}", Convert.ToDecimal(dr1["MonthlyActualTarget"].ToString()));
                }
                else
                {
                    powari.Value = "0.0";
                    wangtoo.Value = "0.0";
                }
                
            }


            dr1.Dispose();
            cmd1.Dispose();
           // txtdtp.Text = "";



            SqlCommand cmd2 = new SqlCommand("select sum(cast(dailymoutarget as float)) YearlyMouTarget, sum(cast(DailyActualGeneration as float)) YearlyActualTarget from [TblGeneration] where CONVERT(datetime,timestring)>=@currentyear and CONVERT(datetime,timestring)<=@date and projectname=@projectname", con);
          
            string date2 = txtdtp.Text;

            cmd2.Parameters.AddWithValue("@currentyear",string.Format("04/01/{0}",currentyear));
            cmd2.Parameters.AddWithValue("@date", gettime);
            cmd2.Parameters.AddWithValue("@projectname", DropDownList1.SelectedItem.Text);
            SqlDataReader dr2 = cmd2.ExecuteReader();
            if (dr2.HasRows)
            {
                dr2.Read();
                Projectname.Value = DropDownList1.SelectedItem.Text;
                dkhabsatluj.Value = String.Format("{0:0.0000}", Convert.ToDecimal(dr2["YearlyMouTarget"].ToString()));
                dkhabspiti.Value = String.Format("{0:0.0000}", Convert.ToDecimal(dr2["YearlyActualTarget"].ToString()));

            }


            dr2.Dispose();
            cmd2.Dispose();
            lblmessagedate.Text = "Data Report for: "+txtdtp.Text;
            lblmessagedate.Visible = true;
            con.Close();
            //  txtdtp.Text = "";

            datepicker.EnableViewState = false;
        }
        protected void lnkpickdate_Click(object sender, EventArgs e)
        {
            datepicker.Visible = true;
        }

        protected void datepicker_SelectionChanged(object sender, EventArgs e)
        {
            txtdtp.Text = datepicker.SelectedDate.ToShortDateString();
            datepicker.Visible = false;
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            //BindGrid();
        }

        protected void txtcumelative_TextChanged(object sender, EventArgs e)
        {

        }

        protected void lnkreport_Click(object sender, EventArgs e)
        {
            if (txtdtp.Text == "")
            {
                //lblmessagedate.Text = "Please select the date";
                //lblmessagedate.Visible = true;
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Notify", "alert('Please select the date first');", true);
                return;
               
            }
            SqlConnection con = new SqlConnection(GetConnectionString());
            SqlCommand cmd = new SqlCommand("select sum(cast(dailymoutarget as float)) dailymou,sum(cast(DailyActualGeneration as float)) dailyactual from [TblGeneration] where CONVERT(datetime,timestring)=@date ", con);
            con.Open();
            string date = txtdtp.Text;

            DateTime d = Convert.ToDateTime(txtdtp.Text);
            string day = d.ToString("MM-dd-yyyy", CultureInfo.InvariantCulture);
            string[] restr = day.Split('-');

            String dy = restr[1].ToString();
            String mn = restr[0].ToString();
            String yy = restr[2].ToString();
            string gettime = mn + "/" + dy + "/" + yy;
            if (Convert.ToInt32(mn) <= 03)
            {
                currentyear = Convert.ToString(Convert.ToInt32(yy) - 1);
            }
            else
            {
                currentyear = yy;
            }

            cmd.Parameters.AddWithValue("@date", gettime);
            
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                dr.Read();
                if (dr["dailymou"].ToString() != "" || dr["dailyactual"].ToString() != "")
                {
                    khabsatluj1.Value = String.Format("{0:0.0000}", Convert.ToDecimal(dr["dailymou"].ToString()));
                    khabspliti1.Value = String.Format("{0:0.0000}", Convert.ToDecimal(dr["dailyactual"].ToString()));
                }
                else
                {
                    khabsatluj1.Value = "0.0";
                    khabspliti1.Value = "0.0";
                }
            }


            dr.Dispose();
            cmd.Dispose();
            // txtdtp.Text = "";


            SqlCommand cmd1 = new SqlCommand(" select sum(cast(dailymoutarget as float)) MonthlyMouTarget, sum(cast(DailyActualGeneration as float)) MonthlyActualTarget from [TblGeneration] where CONVERT(datetime,timestring)>=@datemonth and CONVERT(datetime,timestring)<=@date ", con);

            string date1 = txtdtp.Text;
            string input = date1.Substring(0, date1.IndexOf("-") + 1);
            string datemonth = mn + "/" + "01/" + yy;
            cmd1.Parameters.AddWithValue("@datemonth", datemonth);
            cmd1.Parameters.AddWithValue("@date", gettime);
           
            SqlDataReader dr1 = cmd1.ExecuteReader();
            if (dr1.HasRows)
            {
                dr1.Read();
              //  Projectname.Value = DropDownList1.SelectedItem.Text;
                if (dr1["MonthlyMouTarget"] != "" || dr1["MonthlyActualTarget"].ToString() != "")
                {
                    powari1.Value = String.Format("{0:0.0000}", Convert.ToDecimal(dr1["MonthlyMouTarget"].ToString()));
                    wangtoo1.Value = String.Format("{0:0.0000}", Convert.ToDecimal(dr1["MonthlyActualTarget"].ToString()));
                }
                else
                {
                    powari1.Value = "0.0";
                    wangtoo1.Value = "0.0";
                }

            }


            dr1.Dispose();
            cmd1.Dispose();
            // txtdtp.Text = "";



            SqlCommand cmd2 = new SqlCommand("select sum(cast(dailymoutarget as float)) YearlyMouTarget, sum(cast(DailyActualGeneration as float)) YearlyActualTarget from [TblGeneration] where CONVERT(datetime,timestring)>=@currentyear and CONVERT(datetime,timestring)<=@date ", con);

            string date2 = txtdtp.Text;

            cmd2.Parameters.AddWithValue("@currentyear", string.Format("04/01/{0}", currentyear));
            cmd2.Parameters.AddWithValue("@date", gettime);
          
            SqlDataReader dr2 = cmd2.ExecuteReader();
            if (dr2.HasRows)
            {
                dr2.Read();
              //  Projectname.Value = DropDownList1.SelectedItem.Text;

                dkhabsatluj1.Value = String.Format("{0:0.0000}", Convert.ToDecimal(dr2["YearlyMouTarget"].ToString()));
                dkhabspiti1.Value = String.Format("{0:0.0000}", Convert.ToDecimal(dr2["YearlyActualTarget"].ToString()));
               
              
            }


            dr2.Dispose();
            cmd2.Dispose();
            lblmessagedate.Text = "Data Report for: " + txtdtp.Text;
            lblmessagedate.Visible = true;
            con.Close();
            datepicker.EnableViewState = false;
        }
    }
}