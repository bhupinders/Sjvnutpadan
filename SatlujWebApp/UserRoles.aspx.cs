﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace SatlujWebApp
{
    public partial class UserRoles : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["username"].ToString() == null || Session["username"].ToString() == "")
            {
                Response.Redirect("Login.aspx");
            }

            if (!Page.IsPostBack)
            {
                BindRoles();
                getNotification();
                SqlConnection con1 = new SqlConnection(GetConnectionString());
                SqlCommand cmdcount = new SqlCommand("select count(*) from tblnotificationurja", con1);
                con1.Open();
                int countnotification = (int)cmdcount.ExecuteScalar();
                lblnotificationcount.Text = countnotification.ToString();
                cmdcount.Dispose();
                con1.Close();
            }
        }
        public void getNotification()
        {
            SqlConnection con1 = new SqlConnection(GetConnectionString());
            SqlCommand cmdcount = new SqlCommand("select count(*) from tblnotificationurja", con1);
            con1.Open();
            int countnotification = (int)cmdcount.ExecuteScalar();
            lblnotificationcount.Text = countnotification.ToString();
            if (countnotification == 1)
            {
                SqlConnection con = new SqlConnection(GetConnectionString());
                SqlCommand cmd = new SqlCommand("select top(1) notification from [tblnotificationurja] order by id desc", con);
                con.Open();
                cmd.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dr.Read();
                    notification1.Text = dr["notification"].ToString();
                    lblnotification.Text = dr["notification"].ToString();
                }

                dr.Dispose();
                cmd.Dispose();
                con.Close();
            }

            if (countnotification == 2)
            {
                SqlConnection con = new SqlConnection(GetConnectionString());
                SqlCommand cmd = new SqlCommand("select top(1) notification from [tblnotificationurja] order by id desc", con);
                con.Open();
                cmd.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dr.Read();
                    notification1.Text = dr["notification"].ToString();
                    lblnotification.Text = dr["notification"].ToString();
                }

                dr.Dispose();
                cmd.Dispose();

                SqlCommand cmd1 = new SqlCommand("SELECT id, notification FROM [tblnotificationurja] WHERE Id NOT IN (SELECT MAX( Id )FROM [tblnotificationurja] ) order by id desc", con);

                cmd1.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr1 = cmd1.ExecuteReader();
                if (dr1.HasRows)
                {
                    dr1.Read();

                    notification2.Text = dr1["notification"].ToString();
                }
                dr1.Dispose();
                cmd1.Dispose();
                con.Close();

            }
            if (countnotification >= 3)
            {
                SqlConnection con = new SqlConnection(GetConnectionString());
                SqlCommand cmd = new SqlCommand("select top(1) notification from [tblnotificationurja] order by id desc", con);
                con.Open();
                cmd.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dr.Read();
                    notification1.Text = dr["notification"].ToString();
                    lblnotification.Text = dr["notification"].ToString();
                }

                dr.Dispose();
                cmd.Dispose();

                SqlCommand cmd1 = new SqlCommand("SELECT id, notification FROM [tblnotificationurja] WHERE Id NOT IN (SELECT MAX( Id )FROM [tblnotificationurja] ) order by id desc", con);

                cmd1.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr1 = cmd1.ExecuteReader();
                if (dr1.HasRows)
                {
                    dr1.Read();

                    notification2.Text = dr1["notification"].ToString();
                }
                dr1.Dispose();
                cmd1.Dispose();

                SqlCommand cmd2 = new SqlCommand("SELECT TOP 1 * From(select Top 3 * from [tblnotificationurja] ORDER BY Id DESC) x ORDER BY Id", con);

                cmd2.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr2 = cmd2.ExecuteReader();
                if (dr2.HasRows)
                {
                    dr2.Read();
                    notification3.Text = dr2["notification"].ToString();
                }
                dr2.Dispose();
                cmd2.Dispose();
                con.Close();
            }
            con1.Close();
            cmdcount.Dispose();

        }
        public void BindRoles()
        {
            SqlConnection con = new SqlConnection(GetConnectionString());
            con.Open();
            SqlCommand cmd = new SqlCommand("select * from tbluserroles", con);
            cmd.CommandType = CommandType.Text;
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                dr.Read();
                if (dr["linkedit"].ToString() == "1")
                {
                    linkedit.Checked = true;
                }
                if (dr["locationedit"].ToString() == "1")
                {
                    locationedit.Checked = true;
                }
                if (dr["feedbackedit"].ToString() == "1")
                {
                  //  feedbackedit.Checked = true;
                }
                if (dr["linkview"].ToString() == "1")
                {
                    linkview.Checked = true;
                }
                if (dr["locationview"].ToString() == "1")
                {
                    locationview.Checked = true;
                }
                if (dr["feedbackview"].ToString() == "1")
                {
                  //  feedbackview.Checked = true;
                }
            }
            dr.Dispose();
            cmd.Dispose();
            con.Close();


        }
        public string GetConnectionString()
        {
            return System.Configuration.ConfigurationManager.ConnectionStrings["Local"].ConnectionString;
        }
        protected void btnsubmit_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(GetConnectionString());
            con.Open();
            if (linkedit.Checked == true)
            {
                SqlCommand cmd = new SqlCommand("update userroles set linkedit=1 ");
                cmd.CommandType = CommandType.Text;
                cmd.ExecuteNonQuery();
                cmd.Dispose();
                lblmessage.Text = "Rights Updated!";
                lblmessage.ForeColor = System.Drawing.Color.Green;
            }
            if (locationedit.Checked == true)
            {
                SqlCommand cmd1 = new SqlCommand("update userroles set locationedit=1 ");
                cmd1.CommandType = CommandType.Text;
                cmd1.ExecuteNonQuery();
                cmd1.Dispose();
                lblmessage.Text = "Rights Updated!";
                lblmessage.ForeColor = System.Drawing.Color.Green;
            }
            //if (feedbackedit.Checked == true)
            //{
            //    SqlCommand cmd2 = new SqlCommand("update userroles set feedbackedit=1 ");
            //    cmd2.CommandType = CommandType.Text;
            //    cmd2.ExecuteNonQuery();
            //    cmd2.Dispose();
            //    lblmessage.Text = "Rights Updated!";
            //    lblmessage.ForeColor = System.Drawing.Color.Green;
            //}
            if (linkview.Checked == true)
            {
                SqlCommand cmd3 = new SqlCommand("update userroles set linkview=1 ");
                cmd3.CommandType = CommandType.Text;
                cmd3.ExecuteNonQuery();
                cmd3.Dispose();
                lblmessage.Text = "Rights Updated!";
                lblmessage.ForeColor = System.Drawing.Color.Green;
            }
            if (locationview.Checked == true)
            {
                SqlCommand cmd4 = new SqlCommand("update userroles set locationview=1 ");
                cmd4.CommandType = CommandType.Text;
                cmd4.ExecuteNonQuery();
                cmd4.Dispose();
                lblmessage.Text = "Rights Updated!";
                lblmessage.ForeColor = System.Drawing.Color.Green;
            }
            //if (feedbackview.Checked == true)
            //{
            //    SqlCommand cmd5 = new SqlCommand("update userroles set feedbackview=1 ");
            //    cmd5.CommandType = CommandType.Text;
            //    cmd5.ExecuteNonQuery();
            //    cmd5.Dispose();
            //    lblmessage.Text = "Rights Updated!";
            //    lblmessage.ForeColor = System.Drawing.Color.Green;
            //}
            BindRoles();
        }
    }
}