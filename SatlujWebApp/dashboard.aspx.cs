﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SatlujWebApp
{
    public partial class dashboard1 : System.Web.UI.Page
    {
        string daytime = "";
        string daytime1 = "";
        string daytime2 = "";
        string daytime3 = "";
        string NHHPSDate = "";
        string RHPSDate = "";
        string KWPPDate = "";
        string CSPPDate = "";
        string currentyear = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (Session["username"].ToString() == null )
            {
                Response.Redirect("Login.aspx");
            }
            if (!Page.IsPostBack)
            {
                getNotification();
                getLink();
                getFeedback();
                GetDailyNJHPS();
                GetDailyRHPS();
                GetDailyKWPP();
                GetDailyCSPP();
            }
        }

        public void GetDailyNJHPS()
        {
            string day = DateTime.Today.ToString("MM-dd-yyyy", CultureInfo.InvariantCulture);
            string[] restr = day.Split('-');
            String sDate = DateTime.Now.ToString();
            DateTime datevalue = (Convert.ToDateTime(sDate.ToString()));

            String dy = restr[1].ToString();
            String mn = datevalue.Month.ToString();
            String yy = datevalue.Year.ToString();
            string gettime = mn + "/" + dy + "/" + yy;
            if (Convert.ToInt32(mn) <= 03)
            {
                currentyear = Convert.ToString(Convert.ToInt32(yy) - 1);
            }
            else
            {
                currentyear = yy;
            }

            SqlConnection con = new SqlConnection(GetConnectionString());
            SqlCommand time = new SqlCommand("select max(CONVERT(smalldatetime, TimeString, 101)) as daytime from [TblGeneration] where projectname='NJHPS'", con);
            con.Open();
            SqlDataReader drday = time.ExecuteReader();
            if (drday.HasRows)
            {
                drday.Read();
            }
            string nhpstime = drday["daytime"].ToString();
            string[] nhpstimenew = nhpstime.Split('-');
            daytime = nhpstimenew[1] + "/" + nhpstimenew[0] + "/" + nhpstimenew[2];
            NHHPSDate = daytime;
            lbltime.Text = daytime.Substring(0, daytime.IndexOf(" ") + 1);
            drday.Dispose();
            SqlCommand cmd = new SqlCommand("select top 1 sum(cast(dailymoutarget as float)) as dailymou,sum(cast(DailyActualGeneration as float)) as dailyactual from [TblGeneration] where CONVERT(datetime,timestring)=@date and projectname=@projectname", con);

            cmd.Parameters.AddWithValue("@date", daytime);
            cmd.Parameters.AddWithValue("@projectname", "NJHPS");
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                dr.Read();

                khabsatluj1.Value = String.Format("{0:0.0000}", Convert.ToDecimal(dr["dailymou"].ToString()));
                khabspliti1.Value = String.Format("{0:0.0000}", Convert.ToDecimal(dr["dailyactual"].ToString()));

            }


            dr.Dispose();
            cmd.Dispose();
            // txtdtp.Text = "";


            SqlCommand cmd1 = new SqlCommand("select sum(cast(dailymoutarget as float)) MonthlyMouTarget, sum(cast(DailyActualGeneration as float)) MonthlyActualTarget from [TblGeneration] where CONVERT(datetime,timestring)>=@datemonth and CONVERT(datetime,timestring)<=@date and projectname=@projectname", con);


            string day1 = DateTime.Today.ToString("MM-dd-yyyy", CultureInfo.InvariantCulture);
            string[] restr1 = day1.Split('-');

            string d1 = restr1[1].ToString();
            string mn1 = restr1[0].ToString();
            string yy1 = restr1[2].ToString();
            string[] newdate = NHHPSDate.Split('/');
            string datemonth = newdate[0].ToString() + "/" + "01" + "/" + newdate[2].ToString();
            cmd1.Parameters.AddWithValue("@datemonth", datemonth);
            cmd1.Parameters.AddWithValue("@date", NHHPSDate);
            cmd1.Parameters.AddWithValue("@projectname", "NJHPS");
            SqlDataReader dr1 = cmd1.ExecuteReader();
            if (dr1.HasRows)
            {
                dr1.Read();

                powari1.Value = String.Format("{0:0.0000}", Convert.ToDecimal(dr1["MonthlyMouTarget"].ToString()));
                wangtoo1.Value = String.Format("{0:0.0000}", Convert.ToDecimal(dr1["MonthlyActualTarget"].ToString()));

            }


            dr1.Dispose();
            cmd1.Dispose();
            // txtdtp.Text = "";



            SqlCommand cmd2 = new SqlCommand("select sum(cast(dailymoutarget as float)) YearlyMouTarget, sum(cast(DailyActualGeneration as float)) YearlyActualTarget from [TblGeneration] where CONVERT(datetime,timestring)>=@currentyear and CONVERT(datetime,timestring)<=@date and projectname=@projectname", con);

            cmd2.Parameters.AddWithValue("@currentyear",string.Format("04/01/{0}",currentyear));
            cmd2.Parameters.AddWithValue("@date", NHHPSDate);
            cmd2.Parameters.AddWithValue("@projectname", "NJHPS");
            SqlDataReader dr2 = cmd2.ExecuteReader();
            if (dr2.HasRows)
            {
                dr2.Read();

                dkhabsatluj1.Value = String.Format("{0:0.0000}", Convert.ToDecimal(dr2["YearlyMouTarget"].ToString()));
                dkhabspiti1.Value = String.Format("{0:0.0000}", Convert.ToDecimal(dr2["YearlyActualTarget"].ToString()));

            }


            dr2.Dispose();
            cmd2.Dispose();

            con.Close();


        }
        public void GetDailyRHPS()
        {

            string day = DateTime.Today.ToString("MM-dd-yyyy", CultureInfo.InvariantCulture);
            string[] restr = day.Split('-');
            String sDate = DateTime.Now.ToString();
            DateTime datevalue = (Convert.ToDateTime(sDate.ToString()));

            String dy = restr[1].ToString();
            String mn = datevalue.Month.ToString();
            String yy = datevalue.Year.ToString();
            string gettime = mn + "/" + dy + "/" + yy;
            if (Convert.ToInt32(mn) <= 03)
            {
                currentyear = Convert.ToString(Convert.ToInt32(yy) - 1);
            }
            else
            {
                currentyear = yy;
            }

            SqlConnection con = new SqlConnection(GetConnectionString());
            SqlCommand time = new SqlCommand("   select max(CONVERT(smalldatetime, TimeString, 101)) as daytime from [TblGeneration] where projectname like'%RHPS%'", con);
            con.Open();
            SqlDataReader drday = time.ExecuteReader();
            if (drday.HasRows)
            {
                drday.Read();

            }
            string nhpstime = drday["daytime"].ToString();
            string[] nhpstimenew = nhpstime.Split('-');
            daytime1 = nhpstimenew[1] + "/" + nhpstimenew[0] + "/" + nhpstimenew[2];

            RHPSDate = daytime1;
            lbltime2.Text = daytime1.Substring(0, daytime1.IndexOf(" ") + 1);
            drday.Dispose();
            SqlCommand cmd = new SqlCommand("select top 1 sum(cast(dailymoutarget as float)) as dailymou,sum(cast(DailyActualGeneration as float)) as dailyactual from [TblGeneration] where CONVERT(datetime,timestring)=@date and projectname like'%RHPS%'", con);

            cmd.Parameters.AddWithValue("@date", daytime1);
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                dr.Read();

                khabsatluj2.Value = String.Format("{0:0.0000}", Convert.ToDecimal(dr["dailymou"].ToString()));
                khabspliti2.Value = String.Format("{0:0.0000}", Convert.ToDecimal(dr["dailyactual"].ToString()));

            }


            dr.Dispose();
            cmd.Dispose();
            // txtdtp.Text = "";


            SqlCommand cmd1 = new SqlCommand("select sum(cast(dailymoutarget as float)) MonthlyMouTarget, sum(cast(DailyActualGeneration as float)) MonthlyActualTarget from [TblGeneration] where CONVERT(datetime,timestring)>=@datemonth and CONVERT(datetime,timestring)<=@date and projectname like'%RHPS%'", con);


            string day1 = DateTime.Today.ToString("MM-dd-yyyy", CultureInfo.InvariantCulture);
            string[] restr1 = day1.Split('-');

            string d1 = restr1[1].ToString();
            string mn1 = restr1[0].ToString();
            string yy1 = restr1[2].ToString();
            string[] newdate = RHPSDate.Split('/');
            string datemonth = newdate[0].ToString() + "/" + "01" + "/" + newdate[2].ToString();

            cmd1.Parameters.AddWithValue("@datemonth", datemonth);
            cmd1.Parameters.AddWithValue("@date", RHPSDate);

            SqlDataReader dr1 = cmd1.ExecuteReader();
            if (dr1.HasRows)
            {
                dr1.Read();

                powari2.Value = String.Format("{0:0.0000}", Convert.ToDecimal(dr1["MonthlyMouTarget"].ToString()));
                wangtoo2.Value = String.Format("{0:0.0000}", Convert.ToDecimal(dr1["MonthlyActualTarget"].ToString()));

            }


            dr1.Dispose();
            cmd1.Dispose();
            // txtdtp.Text = "";



            SqlCommand cmd2 = new SqlCommand("select sum(cast(dailymoutarget as float)) YearlyMouTarget, sum(cast(DailyActualGeneration as float)) YearlyActualTarget from [TblGeneration] where CONVERT(datetime,timestring)>=@currentdate and CONVERT(datetime,timestring)<=@date and projectname like'%RHPS%'", con);

            cmd.Parameters.AddWithValue("@currentdate", string.Format("04/01/{0}", currentyear));
            cmd2.Parameters.AddWithValue("@date", RHPSDate);

            SqlDataReader dr2 = cmd2.ExecuteReader();
            if (dr2.HasRows)
            {
                dr2.Read();

                dkhabsatluj2.Value = String.Format("{0:0.0000}", Convert.ToDecimal(dr2["YearlyMouTarget"].ToString()));
                dkhabspiti2.Value = String.Format("{0:0.0000}", Convert.ToDecimal(dr2["YearlyActualTarget"].ToString()));

            }


            dr2.Dispose();
            cmd2.Dispose();

            con.Close();

        }
        public void GetDailyKWPP()
        {
            string day = DateTime.Today.ToString("MM-dd-yyyy", CultureInfo.InvariantCulture);
            string[] restr = day.Split('-');
            String sDate = DateTime.Now.ToString();
            DateTime datevalue = (Convert.ToDateTime(sDate.ToString()));

            String dy = restr[1].ToString();
            String mn = datevalue.Month.ToString();
            String yy = datevalue.Year.ToString();
            string gettime = mn + "/" + dy + "/" + yy;
            if (Convert.ToInt32(mn) <= 03)
            {
                currentyear = Convert.ToString(Convert.ToInt32(yy) - 1);
            }
            else
            {
                currentyear = yy;
            }
            SqlConnection con = new SqlConnection(GetConnectionString());
            SqlCommand time = new SqlCommand("   select max(CONVERT(smalldatetime, TimeString, 101)) as daytime from [TblGeneration] where projectname='KWPP'", con);
            con.Open();
            SqlDataReader drday = time.ExecuteReader();
            if (drday.HasRows)
            {
                drday.Read();
            }
            string nhpstime = drday["daytime"].ToString();
            string[] nhpstimenew = nhpstime.Split('-');
            daytime2 = nhpstimenew[1] + "/" + nhpstimenew[0] + "/" + nhpstimenew[2];

            KWPPDate = daytime2;
            lbltime3.Text = daytime2.Substring(0, daytime2.IndexOf(" ") + 1);
            drday.Dispose();
            SqlCommand cmd = new SqlCommand("select top 1 sum(cast(dailymoutarget as float)) as dailymou,sum(cast(DailyActualGeneration as float)) as dailyactual from [TblGeneration] where CONVERT(datetime,timestring)=@date and projectname='KWPP'", con);

            cmd.Parameters.AddWithValue("@date", daytime2);

            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                dr.Read();

                khabsatluj3.Value = String.Format("{0:0.0000}", Convert.ToDecimal(dr["dailymou"].ToString()));
                khabspliti3.Value = String.Format("{0:0.0000}", Convert.ToDecimal(dr["dailyactual"].ToString()));

            }


            dr.Dispose();
            cmd.Dispose();
            // txtdtp.Text = "";


            SqlCommand cmd1 = new SqlCommand("select sum(cast(dailymoutarget as float)) MonthlyMouTarget, sum(cast(DailyActualGeneration as float)) MonthlyActualTarget from [TblGeneration] where CONVERT(datetime,timestring)>=@datemonth and CONVERT(datetime,timestring)<=@date and projectname=@projectname", con);


            string day1 = DateTime.Today.ToString("MM-dd-yyyy", CultureInfo.InvariantCulture);
            string[] restr1 = day1.Split('-');

            string d1 = restr1[1].ToString();
            string mn1 = restr1[0].ToString();
            string yy1 = restr1[2].ToString();
            string[] newdate = KWPPDate.Split('/');
            string datemonth = newdate[0].ToString() + "/" + "01" + "/" + newdate[2].ToString();

            cmd1.Parameters.AddWithValue("@datemonth", datemonth);
            cmd1.Parameters.AddWithValue("@date", KWPPDate);
            cmd1.Parameters.AddWithValue("@projectname", "KWPP");
            SqlDataReader dr1 = cmd1.ExecuteReader();
            if (dr1.HasRows)
            {
                dr1.Read();

                powari3.Value = String.Format("{0:0.0000}", Convert.ToDecimal(dr1["MonthlyMouTarget"].ToString()));
                wangtoo3.Value = String.Format("{0:0.0000}", Convert.ToDecimal(dr1["MonthlyActualTarget"].ToString()));

            }


            dr1.Dispose();
            cmd1.Dispose();
            // txtdtp.Text = "";



            SqlCommand cmd2 = new SqlCommand("select sum(cast(dailymoutarget as float)) YearlyMouTarget, sum(cast(DailyActualGeneration as float)) YearlyActualTarget from [TblGeneration] where CONVERT(datetime,timestring)>=@currentyear and CONVERT(datetime,timestring)<=@date and projectname=@projectname", con);

            cmd.Parameters.AddWithValue("@currentdate", string.Format("04/01/{0}", currentyear));
            cmd2.Parameters.AddWithValue("@date", KWPPDate);
            cmd2.Parameters.AddWithValue("@projectname", "KWPP");
            SqlDataReader dr2 = cmd2.ExecuteReader();
            if (dr2.HasRows)
            {
                dr2.Read();

                dkhabsatluj3.Value = String.Format("{0:0.0000}", Convert.ToDecimal(dr2["YearlyMouTarget"].ToString()));
                dkhabspiti3.Value = String.Format("{0:0.0000}", Convert.ToDecimal(dr2["YearlyActualTarget"].ToString()));

            }


            dr2.Dispose();
            cmd2.Dispose();

            con.Close();
        }
        public void GetDailyCSPP()
        {
            string day = DateTime.Today.ToString("MM-dd-yyyy", CultureInfo.InvariantCulture);
            string[] restr = day.Split('-');
            String sDate = DateTime.Now.ToString();
            DateTime datevalue = (Convert.ToDateTime(sDate.ToString()));

            String dy = restr[1].ToString();
            String mn = datevalue.Month.ToString();
            String yy = datevalue.Year.ToString();
            string gettime = mn + "/" + dy + "/" + yy;
            if (Convert.ToInt32(mn) <= 03)
            {
                currentyear = Convert.ToString(Convert.ToInt32(yy) - 1);
            }
            else
            {
                currentyear = yy;
            }

            SqlConnection con = new SqlConnection(GetConnectionString());
            SqlCommand time = new SqlCommand(" select max(CONVERT(smalldatetime, TimeString, 101)) as daytime from [TblGeneration] where projectname='CSPP'", con);
            con.Open();
            SqlDataReader drday = time.ExecuteReader();
            if (drday.HasRows)
            {
                drday.Read();
            }
            string nhpstime = drday["daytime"].ToString();
            string[] nhpstimenew = nhpstime.Split('-');
            daytime3 = nhpstimenew[1] + "/" + nhpstimenew[0] + "/" + nhpstimenew[2];

            CSPPDate = daytime3;
            lbltime4.Text = daytime3.Substring(0, daytime3.IndexOf(" ") + 1);
            drday.Dispose();
            SqlCommand cmd = new SqlCommand("select top 1 sum(cast(dailymoutarget as float)) as dailymou,sum(cast(DailyActualGeneration as float)) as dailyactual from [TblGeneration] where CONVERT(datetime,timestring)=@date and projectname='CSPP'", con);

            cmd.Parameters.AddWithValue("@date", daytime3);

            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                dr.Read();

                khabsatluj4.Value = String.Format("{0:0.0000}", Convert.ToDecimal(dr["dailymou"].ToString()));
                khabspliti4.Value = String.Format("{0:0.0000}", Convert.ToDecimal(dr["dailyactual"].ToString()));

            }


            dr.Dispose();
            cmd.Dispose();
            // txtdtp.Text = "";


            SqlCommand cmd1 = new SqlCommand("select sum(cast(dailymoutarget as float)) MonthlyMouTarget, sum(cast(DailyActualGeneration as float)) MonthlyActualTarget from [TblGeneration] where CONVERT(datetime,timestring)>=@datemonth and CONVERT(datetime,timestring)<=@date and projectname=@projectname", con);


            string day1 = DateTime.Today.ToString("MM-dd-yyyy", CultureInfo.InvariantCulture);
            string[] restr1 = day1.Split('-');

            string d1 = restr1[1].ToString();
            string mn1 = restr1[0].ToString();
            string yy1 = restr1[2].ToString();
            string[] newdate = CSPPDate.Split('/');
            string datemonth = newdate[0].ToString() + "/" + "01" + "/" + newdate[2].ToString();
            cmd1.Parameters.AddWithValue("@datemonth", datemonth);
            cmd1.Parameters.AddWithValue("@date", CSPPDate);
            cmd1.Parameters.AddWithValue("@projectname", "CSPP");
            SqlDataReader dr1 = cmd1.ExecuteReader();
            if (dr1.HasRows)
            {
                dr1.Read();

                powari4.Value = String.Format("{0:0.0000}", Convert.ToDecimal(dr1["MonthlyMouTarget"].ToString()));
                wangtoo4.Value = String.Format("{0:0.0000}", Convert.ToDecimal(dr1["MonthlyActualTarget"].ToString()));

            }


            dr1.Dispose();
            cmd1.Dispose();
            // txtdtp.Text = "";



            SqlCommand cmd2 = new SqlCommand("select sum(cast(dailymoutarget as float)) YearlyMouTarget, sum(cast(DailyActualGeneration as float)) YearlyActualTarget from [TblGeneration] where CONVERT(datetime,timestring)>='04/01/2017' and CONVERT(datetime,timestring)<=@date and projectname=@projectname", con);

            cmd2.Parameters.AddWithValue("@currentyear",string.Format("04/01/{0}",currentyear));
            cmd2.Parameters.AddWithValue("@date", CSPPDate);
            cmd2.Parameters.AddWithValue("@projectname", "CSPP");
            SqlDataReader dr2 = cmd2.ExecuteReader();
            if (dr2.HasRows)
            {
                dr2.Read();

                dkhabsatluj4.Value = String.Format("{0:0.0000}", Convert.ToDecimal(dr2["YearlyMouTarget"].ToString()));
                dkhabspiti4.Value = String.Format("{0:0.0000}", Convert.ToDecimal(dr2["YearlyActualTarget"].ToString()));

            }


            dr2.Dispose();
            cmd2.Dispose();

            con.Close();
        }
        public void getNotification()
        {
            SqlConnection con1 = new SqlConnection(GetConnectionString());
            SqlCommand cmdcount = new SqlCommand("select count(*) from tblnotificationurja", con1);
            con1.Open();
            int countnotification = (int)cmdcount.ExecuteScalar();
            lblnotificationcount.Text = countnotification.ToString();
            if (countnotification == 1)
            {
                SqlConnection con = new SqlConnection(GetConnectionString());
                SqlCommand cmd = new SqlCommand("select top(1) notification from [tblnotificationurja] order by id desc", con);
                con.Open();
                cmd.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dr.Read();
                    notification1.Text = dr["notification"].ToString();
                    lblnotification.Text = dr["notification"].ToString();
                }

                dr.Dispose();
                cmd.Dispose();
                con.Close();
              
            }

            if (countnotification == 2)
            {
                SqlConnection con = new SqlConnection(GetConnectionString());
                SqlCommand cmd = new SqlCommand("select top(1) notification from [tblnotificationurja] order by id desc", con);
                con.Open();
                cmd.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dr.Read();
                    notification1.Text = dr["notification"].ToString();
                    lblnotification.Text = dr["notification"].ToString();
                }

                dr.Dispose();
                cmd.Dispose();

                SqlCommand cmd1 = new SqlCommand("SELECT id, notification FROM [tblnotificationurja] WHERE Id NOT IN (SELECT MAX( Id )FROM [tblnotificationurja] ) order by id desc", con);

                cmd1.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr1 = cmd1.ExecuteReader();
                if (dr1.HasRows)
                {
                    dr1.Read();

                    notification2.Text = dr1["notification"].ToString();
                }
                dr1.Dispose();
                cmd1.Dispose();
                con.Close();

            }
            if (countnotification >= 3)
            {
                SqlConnection con = new SqlConnection(GetConnectionString());
                SqlCommand cmd = new SqlCommand("select top(1) notification from [tblnotificationurja] order by id desc", con);
                con.Open();
                cmd.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dr.Read();
                    notification1.Text = dr["notification"].ToString();
                    lblnotification.Text = dr["notification"].ToString();
                }

                dr.Dispose();
                cmd.Dispose();

                SqlCommand cmd1 = new SqlCommand("SELECT id, notification FROM [tblnotificationurja] WHERE Id NOT IN (SELECT MAX( Id )FROM [tblnotificationurja] ) order by id desc", con);

                cmd1.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr1 = cmd1.ExecuteReader();
                if (dr1.HasRows)
                {
                    dr1.Read();

                    notification2.Text = dr1["notification"].ToString();
                }
                dr1.Dispose();
                cmd1.Dispose();

                SqlCommand cmd2 = new SqlCommand("SELECT TOP 1 * From(select Top 3 * from [tblnotificationurja] ORDER BY Id DESC) x ORDER BY Id", con);

                cmd2.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr2 = cmd2.ExecuteReader();
                if (dr2.HasRows)
                {
                    dr2.Read();
                    notification3.Text = dr2["notification"].ToString();
                }
                dr2.Dispose();
                cmd2.Dispose();
                con.Close();
            }

            cmdcount.Dispose();
            con1.Close();

        }
        public string GetConnectionString()
        {
            return System.Configuration.ConfigurationManager.ConnectionStrings["Local"].ConnectionString;
        }
        public void getLink()
        {
            SqlConnection con1 = new SqlConnection(GetConnectionString());
            SqlCommand cmdcount = new SqlCommand("select count(*) from  [TblLinksUrja]", con1);
            con1.Open();
            int count = (int)cmdcount.ExecuteScalar();
            if (count == 1)
            {
                SqlConnection con = new SqlConnection(GetConnectionString());
                SqlCommand cmd = new SqlCommand("select top(1) id,name,description,url from [TblLinksUrja] order by id desc", con);
                con.Open();
                cmd.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dr.Read();
                    lbllink4.Text = dr["description"].ToString();
                    urllink4.Text = dr["url"].ToString();
                }
                dr.Dispose();
                cmd.Dispose();
                con.Close();
            }
            if (count == 2)
            {
                SqlConnection con = new SqlConnection(GetConnectionString());
                SqlCommand cmd = new SqlCommand("select top(1) id,name,description,url from [TblLinksUrja] order by id desc", con);
                con.Open();
                cmd.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dr.Read();
                    lbllink4.Text = dr["description"].ToString();
                    urllink4.Text = dr["url"].ToString();
                }
                dr.Dispose();
                cmd.Dispose();
                SqlCommand cmd1 = new SqlCommand("SELECT name,Description,Url FROM [TblLinksUrja] WHERE Id NOT IN (SELECT MAX( Id )FROM [TblLinksUrja] ) order by id desc", con);

                cmd1.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr1 = cmd1.ExecuteReader();
                if (dr1.HasRows)
                {
                    dr1.Read();
                    lbllink3.Text = dr1["description"].ToString();
                    urllink3.Text = dr1["url"].ToString();
                }
                dr1.Dispose();
                cmd1.Dispose();
                con.Close();
            }
            if (count == 3)
            {
                SqlConnection con = new SqlConnection(GetConnectionString());
                SqlCommand cmd = new SqlCommand("select top(1) id,name,description,url from [TblLinksUrja] order by id desc", con);
                con.Open();
                cmd.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dr.Read();
                    lbllink4.Text = dr["description"].ToString();
                    urllink4.Text = dr["url"].ToString();
                }
                dr.Dispose();
                cmd.Dispose();
                SqlCommand cmd1 = new SqlCommand("SELECT name,Description,Url FROM [TblLinksUrja] WHERE Id NOT IN (SELECT MAX( Id )FROM [TblLinksUrja] ) order by id desc", con);

                cmd1.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr1 = cmd1.ExecuteReader();
                if (dr1.HasRows)
                {
                    dr1.Read();
                    lbllink3.Text = dr1["description"].ToString();
                    urllink3.Text = dr1["url"].ToString();
                }
                dr1.Dispose();
                cmd1.Dispose();

                SqlCommand cmd2 = new SqlCommand("SELECT TOP 1 * From(select Top 3 * from [TblLinksUrja] ORDER BY Id DESC) x ORDER BY Id", con);

                cmd2.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr2 = cmd2.ExecuteReader();
                if (dr2.HasRows)
                {
                    dr2.Read();
                    lbllink2.Text = dr2["description"].ToString();
                    urllink2.Text = dr2["url"].ToString();
                }
                dr2.Dispose();
                cmd2.Dispose();
                con.Close();
            }

            if (count >= 4)
            {
                SqlConnection con = new SqlConnection(GetConnectionString());
                SqlCommand cmd = new SqlCommand("select top(1) id,name,description,url from [TblLinksUrja] order by id desc", con);
                con.Open();
                cmd.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dr.Read();
                    lbllink4.Text = dr["description"].ToString();
                    urllink4.Text = dr["url"].ToString();
                }
                dr.Dispose();
                cmd.Dispose();
                SqlCommand cmd1 = new SqlCommand("SELECT name,Description,Url FROM [TblLinksUrja] WHERE Id NOT IN (SELECT MAX( Id )FROM [TblLinksUrja] ) order by id desc", con);

                cmd1.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr1 = cmd1.ExecuteReader();
                if (dr1.HasRows)
                {
                    dr1.Read();
                    lbllink3.Text = dr1["description"].ToString();
                    urllink3.Text = dr1["url"].ToString();
                }
                dr1.Dispose();
                cmd1.Dispose();

                SqlCommand cmd2 = new SqlCommand("SELECT TOP 1 * From(select Top 3 * from [TblLinksUrja] ORDER BY Id DESC) x ORDER BY Id", con);

                cmd2.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr2 = cmd2.ExecuteReader();
                if (dr2.HasRows)
                {
                    dr2.Read();
                    lbllink2.Text = dr2["description"].ToString();
                    urllink2.Text = dr2["url"].ToString();
                }
                dr2.Dispose();
                cmd2.Dispose();

                SqlCommand cmd4 = new SqlCommand("SELECT TOP 1 * From(select Top 4 * from [TblLinksUrja] ORDER BY Id DESC) x ORDER BY Id", con);

                cmd4.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr4 = cmd4.ExecuteReader();
                if (dr4.HasRows)
                {
                    dr4.Read();
                    lbllink1.Text = dr4["description"].ToString();
                    urllink1.Text = dr4["url"].ToString();
                }
                dr4.Dispose();
                cmd4.Dispose();
                con.Close();

            }
            con1.Close();
        }
        public void getFeedback()
        {
            SqlConnection con = new SqlConnection(GetConnectionString());
            SqlCommand cmd = new SqlCommand("select top(1) feedback from [TblFeedBack] order by id desc", con);
            con.Open();
            cmd.CommandType = System.Data.CommandType.Text;
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                dr.Read();
                lblfeedback4.Text = dr["feedback"].ToString();
                
            }
            dr.Dispose();
            cmd.Dispose();
            SqlCommand cmd1 = new SqlCommand("SELECT feedback FROM [TblFeedBack] WHERE Id NOT IN (SELECT MAX( Id )FROM [TblFeedBack] ) order by id desc", con);

            cmd1.CommandType = System.Data.CommandType.Text;
            SqlDataReader dr1 = cmd1.ExecuteReader();
            if (dr1.HasRows)
            {
                dr1.Read();
                lblfeedback3.Text = dr1["feedback"].ToString();
                
            }
            dr1.Dispose();
            cmd1.Dispose();
            SqlCommand cmd2 = new SqlCommand("SELECT TOP 1 * From(select Top 3 * from [TblFeedBack] ORDER BY Id DESC) x ORDER BY Id", con);

            cmd2.CommandType = System.Data.CommandType.Text;
            SqlDataReader dr2 = cmd2.ExecuteReader();
            if (dr2.HasRows)
            {
                dr2.Read();
                lblfeedbback2.Text = dr2["feedback"].ToString();
              
            }
            dr2.Dispose();
            cmd2.Dispose();

            SqlCommand cmd4 = new SqlCommand("SELECT TOP 1 * From(select Top 4 * from [TblFeedBack] ORDER BY Id DESC) x ORDER BY Id", con);
            cmd4.CommandType = System.Data.CommandType.Text;
            SqlDataReader dr4 = cmd4.ExecuteReader();
            if (dr4.HasRows)
            {
                dr4.Read();
                lblfeedback1.Text = dr4["feedback"].ToString();
               
            }
            dr4.Dispose();
            cmd4.Dispose();
            con.Close();
        }
    }
}