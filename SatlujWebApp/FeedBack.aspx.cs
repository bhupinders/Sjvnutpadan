﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SatlujWebApp
{
    public partial class FeedBack : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(Session["username"].ToString()==null || Session["username"].ToString()=="")
            {
                Response.Redirect("Login.aspx");
            }
            if (!Page.IsPostBack)
            {
                BindData();
                BindData1();
                getNotification();
                getLink();
                SqlConnection con1 = new SqlConnection(GetConnectionString());
                SqlCommand cmdcount = new SqlCommand("select count(*) from tblnotificationurja", con1);
                con1.Open();
                int countnotification = (int)cmdcount.ExecuteScalar();
                lblnotificationcount.Text = countnotification.ToString();
                cmdcount.Dispose();
                con1.Close();
            }
        }

        protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            grdfeedback.EditIndex = -1;
            BindData();
        }
        public void getLink()
        {
            SqlConnection con1 = new SqlConnection(GetConnectionString());
            SqlCommand cmdcount = new SqlCommand("select count(*) from  [TblLinksUrja]", con1);
            con1.Open();
            int count = (int)cmdcount.ExecuteScalar();
            if (count == 1)
            {
                SqlConnection con = new SqlConnection(GetConnectionString());
                SqlCommand cmd = new SqlCommand("select top(1) id,name,description,url from [TblLinksUrja] order by id desc", con);
                con.Open();
                cmd.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dr.Read();
                    lbllink4.Text = dr["description"].ToString();
                    urllink4.Text = dr["url"].ToString();
                }
                dr.Dispose();
                cmd.Dispose();
                con.Close();
            }
            if (count == 2)
            {
                SqlConnection con = new SqlConnection(GetConnectionString());
                SqlCommand cmd = new SqlCommand("select top(1) id,name,description,url from [TblLinksUrja] order by id desc", con);
                con.Open();
                cmd.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dr.Read();
                    lbllink4.Text = dr["description"].ToString();
                    urllink4.Text = dr["url"].ToString();
                }
                dr.Dispose();
                cmd.Dispose();
                SqlCommand cmd1 = new SqlCommand("SELECT name,Description,Url FROM [TblLinksUrja] WHERE Id NOT IN (SELECT MAX( Id )FROM [TblLinksUrja] ) order by id desc", con);

                cmd1.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr1 = cmd1.ExecuteReader();
                if (dr1.HasRows)
                {
                    dr1.Read();
                    lbllink3.Text = dr1["description"].ToString();
                    urllink3.Text = dr1["url"].ToString();
                }
                dr1.Dispose();
                cmd1.Dispose();
                con.Close();
            }
            if (count == 3)
            {
                SqlConnection con = new SqlConnection(GetConnectionString());
                SqlCommand cmd = new SqlCommand("select top(1) id,name,description,url from [TblLinksUrja] order by id desc", con);
                con.Open();
                cmd.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dr.Read();
                    lbllink4.Text = dr["description"].ToString();
                    urllink4.Text = dr["url"].ToString();
                }
                dr.Dispose();
                cmd.Dispose();
                SqlCommand cmd1 = new SqlCommand("SELECT name,Description,Url FROM [TblLinksUrja] WHERE Id NOT IN (SELECT MAX( Id )FROM [TblLinksUrja] ) order by id desc", con);

                cmd1.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr1 = cmd1.ExecuteReader();
                if (dr1.HasRows)
                {
                    dr1.Read();
                    lbllink3.Text = dr1["description"].ToString();
                    urllink3.Text = dr1["url"].ToString();
                }
                dr1.Dispose();
                cmd1.Dispose();

                SqlCommand cmd2 = new SqlCommand("SELECT TOP 1 * From(select Top 3 * from [TblLinksUrja] ORDER BY Id DESC) x ORDER BY Id", con);

                cmd2.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr2 = cmd2.ExecuteReader();
                if (dr2.HasRows)
                {
                    dr2.Read();
                    lbllink2.Text = dr2["description"].ToString();
                    urllink2.Text = dr2["url"].ToString();
                }
                dr2.Dispose();
                cmd2.Dispose();
                con.Close();
            }

            if (count == 4)
            {
                SqlConnection con = new SqlConnection(GetConnectionString());
                SqlCommand cmd = new SqlCommand("select top(1) id,name,description,url from [TblLinksUrja] order by id desc", con);
                con.Open();
                cmd.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dr.Read();
                    lbllink4.Text = dr["description"].ToString();
                    urllink4.Text = dr["url"].ToString();
                }
                dr.Dispose();
                cmd.Dispose();
                SqlCommand cmd1 = new SqlCommand("SELECT name,Description,Url FROM [TblLinksUrja] WHERE Id NOT IN (SELECT MAX( Id )FROM [TblLinksUrja] ) order by id desc", con);

                cmd1.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr1 = cmd1.ExecuteReader();
                if (dr1.HasRows)
                {
                    dr1.Read();
                    lbllink3.Text = dr1["description"].ToString();
                    urllink3.Text = dr1["url"].ToString();
                }
                dr1.Dispose();
                cmd1.Dispose();

                SqlCommand cmd2 = new SqlCommand("SELECT TOP 1 * From(select Top 3 * from [TblLinksUrja] ORDER BY Id DESC) x ORDER BY Id", con);

                cmd2.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr2 = cmd2.ExecuteReader();
                if (dr2.HasRows)
                {
                    dr2.Read();
                    lbllink2.Text = dr2["description"].ToString();
                    urllink2.Text = dr2["url"].ToString();
                }
                dr2.Dispose();
                cmd2.Dispose();

                SqlCommand cmd4 = new SqlCommand("SELECT TOP 1 * From(select Top 4 * from [TblLinksUrja] ORDER BY Id DESC) x ORDER BY Id", con);

                cmd4.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr4 = cmd4.ExecuteReader();
                if (dr4.HasRows)
                {
                    dr4.Read();
                    lbllink1.Text = dr4["description"].ToString();
                    urllink1.Text = dr4["url"].ToString();
                }
                dr4.Dispose();
                cmd4.Dispose();
                con.Close();

            }
            con1.Close();
        }
        public void getNotification()
        {
            SqlConnection con1 = new SqlConnection(GetConnectionString());
            SqlCommand cmdcount = new SqlCommand("select count(*) from tblnotificationurja", con1);
            con1.Open();
            int countnotification = (int)cmdcount.ExecuteScalar();
            lblnotificationcount.Text = countnotification.ToString();
            if (countnotification == 1)
            {
                SqlConnection con = new SqlConnection(GetConnectionString());
                SqlCommand cmd = new SqlCommand("select top(1) notification from [tblnotificationurja] order by id desc", con);
                con.Open();
                cmd.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dr.Read();
                    notification1.Text = dr["notification"].ToString();
                    lblnotification.Text = dr["notification"].ToString();
                }

                dr.Dispose();
                cmd.Dispose();
                con.Close();
            }

            if (countnotification == 2)
            {
                SqlConnection con = new SqlConnection(GetConnectionString());
                SqlCommand cmd = new SqlCommand("select top(1) notification from [tblnotificationurja] order by id desc", con);
                con.Open();
                cmd.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dr.Read();
                    notification1.Text = dr["notification"].ToString();
                    lblnotification.Text = dr["notification"].ToString();
                }

                dr.Dispose();
                cmd.Dispose();

                SqlCommand cmd1 = new SqlCommand("SELECT id, notification FROM [tblnotificationurja] WHERE Id NOT IN (SELECT MAX( Id )FROM [tblnotificationurja] ) order by id desc", con);

                cmd1.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr1 = cmd1.ExecuteReader();
                if (dr1.HasRows)
                {
                    dr1.Read();

                    notification2.Text = dr1["notification"].ToString();
                }
                dr1.Dispose();
                cmd1.Dispose();
                con.Close();

            }
            if (countnotification >= 3)
            {
                SqlConnection con = new SqlConnection(GetConnectionString());
                SqlCommand cmd = new SqlCommand("select top(1) notification from [tblnotificationurja] order by id desc", con);
                con.Open();
                cmd.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dr.Read();
                    notification1.Text = dr["notification"].ToString();
                    lblnotification.Text = dr["notification"].ToString();
                }

                dr.Dispose();
                cmd.Dispose();

                SqlCommand cmd1 = new SqlCommand("SELECT id, notification FROM [tblnotificationurja] WHERE Id NOT IN (SELECT MAX( Id )FROM [tblnotificationurja] ) order by id desc", con);

                cmd1.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr1 = cmd1.ExecuteReader();
                if (dr1.HasRows)
                {
                    dr1.Read();

                    notification2.Text = dr1["notification"].ToString();
                }
                dr1.Dispose();
                cmd1.Dispose();

                SqlCommand cmd2 = new SqlCommand("SELECT TOP 1 * From(select Top 3 * from [tblnotificationurja] ORDER BY Id DESC) x ORDER BY Id", con);

                cmd2.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr2 = cmd2.ExecuteReader();
                if (dr2.HasRows)
                {
                    dr2.Read();
                    notification3.Text = dr2["notification"].ToString();
                }
                dr2.Dispose();
                cmd2.Dispose();
                con.Close();
            }
            con1.Close();

            cmdcount.Dispose();

        }
        protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            Label id = grdfeedback.Rows[e.RowIndex].FindControl("lblid") as Label;
            TextBox Name = grdfeedback.Rows[e.RowIndex].FindControl("txtname") as TextBox;
            TextBox FeedBack = grdfeedback.Rows[e.RowIndex].FindControl("txtfeedback") as TextBox;
            
            SqlConnection con = new SqlConnection(GetConnectionString());
            con.Open();
            //updating the record  
            SqlCommand cmd = new SqlCommand("Update TblFeedBackUrja set Name='" + Name.Text + "',feedback='" + FeedBack.Text + "' where ID=" + Convert.ToInt32(id.Text), con);
            cmd.ExecuteNonQuery();
            con.Close();
            //Setting the EditIndex property to -1 to cancel the Edit mode in Gridview  
            grdfeedback.EditIndex = -1;
            con.Close();
            //Call ShowData method for displaying updated data  
            BindData();
        }

        protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
        {
            grdfeedback.EditIndex = e.NewEditIndex;
            BindData();
        }

        protected void btnsave_Click(object sender, EventArgs e)
        {

            SqlConnection con = new SqlConnection(GetConnectionString());
            SqlCommand cmd = new SqlCommand("insert into TblFeedBackUrja values(@name,@feedback)", con);
            cmd.CommandType = System.Data.CommandType.Text;
            con.Open();
            cmd.Parameters.AddWithValue("@name", Request.Form["name"].ToString());
            cmd.Parameters.AddWithValue("@feedback", Request.Form["feedback"].ToString());
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            lblmessage.Text = "Record Inserted";
            lblmessage.ForeColor = System.Drawing.Color.Green;
            con.Close();
            BindData();
        }
        public void BindData()
        {
            SqlConnection con = new SqlConnection(GetConnectionString());
            con.Open();
            SqlCommand cmd = new SqlCommand("select * from TblFeedBackUrja order by id desc", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            grdfeedback.DataSource = ds;
            grdfeedback.DataBind();            
            cmd.Dispose();
            con.Close();
        }
        public void BindData1()
        {
            SqlConnection con = new SqlConnection(GetConnectionString());
            con.Open();
            SqlCommand cmd = new SqlCommand("select * from TblFeedBackUrja order by id desc", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            GridView1.DataSource = ds;
            GridView1.DataBind();
            cmd.Dispose();
            con.Close();
        }
        public string GetConnectionString()
        {
            return System.Configuration.ConfigurationManager.ConnectionStrings["Local"].ConnectionString;
        }
    }
}