﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RegisterUser.aspx.cs" Inherits="SatlujWebApp.RegisterUser" %>


<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
<title></title>
<link type="text/css" rel="stylesheet" href="css/font-awesome.css">
<link type="text/css" rel="stylesheet" href="css/material-design-iconic-font.css">
<link type="text/css" rel="stylesheet" href="css/bootstrap.css">
<link type="text/css" rel="stylesheet" href="css/animate.css">
<link type="text/css" rel="stylesheet" href="css/layout.css">
<link type="text/css" rel="stylesheet" href="css/components.css">
<link type="text/css" rel="stylesheet" href="css/widgets.css">
<link type="text/css" rel="stylesheet" href="css/plugins.css">
<link type="text/css" rel="stylesheet" href="css/pages.css">
<link type="text/css" rel="stylesheet" href="css/bootstrap-extend.css">
<link type="text/css" rel="stylesheet" href="css/common.css">
<link type="text/css" rel="stylesheet" href="css/responsive.css">
</head>
    <script>

    function get(){
// Load the SDK Asynchronously
        (function (d) {
            debugger;
var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
if (d.getElementById(id)) { return; }
js = d.createElement('script'); js.id = id; js.async = true;
js.src = "//connect.facebook.net/en_US/all.js";
ref.parentNode.insertBefore(js, ref);
} (document));

// Init the SDK upon load
window.fbAsyncInit = function() {
FB.init({
    appId: '1745025375716484', // App ID
channelUrl: '//' + window.location.hostname + '/channel', // Path to your Channel File
status: true, // check login status
cookie: true, // enable cookies to allow the server to access the session
xfbml: true  // parse XFBML
});


 

// listen for and handle auth.statusChange events
FB.Event.subscribe('auth.statusChange', function(response) {
if (response.authResponse) {
// user has auth'd your app and is logged into Facebook
    FB.api('/me?fields=id,name,email', function (me) {
if (me.email) {
    document.getElementById('auth-displayname').innerHTML = me.email;
    document.getElementById("lblmessage1").innerHTML = document.getElementById('auth-displayname').innerHTML;
    document.getElementById("lblmessage1").style.display = "none";
    $.ajax({
        
        type: "POST",
        url: "RegisterUser.aspx/Get",
        data: '{Hello: "' +$('#<%= lblmessage1.ClientID %>').text() +'" }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
      
    });
  
}

})
document.getElementById('auth-loggedout').style.display = 'none';
document.getElementById('auth-loggedin').style.display = 'block';
} else {
// user has not auth'd your app, or is not logged into Facebook
document.getElementById('auth-loggedout').style.display = 'block';
document.getElementById('auth-loggedin').style.display = 'none';
}
});
//$("#auth-logoutlink").click(function() { FB.logout(function() { window.location.reload(); }); });
}
    }
</script>
  <form runat="server" id="form1">
<body class="login-page social-login">
<!--Page Container Start Here-->
<section class="login-container boxed-login">
<div class="container">
<div class="col-md-6 col-md-offset-6 col-sm-4 col-sm-offset-6" style="
    margin: 0 auto;
">
<div class="login-form-container">
<form action=" method="post" class="j-forms" id="forms-login" novalidate>

<div class="login-form-header">
 <div class="logo">
                   <h3><img src="images/logo.png"/ ></h3>
                       <!--  <a href="index.html" title="Admin Template"><img src="images/logo.png" alt="logo"></a> -->
                    </div></div>
<div class="login-form-content j-forms">



<!-- start login -->
<div class="unit">
<div class="input login-input">
<label class="icon-left" for="login">
<i class="zmdi zmdi-account"></i>
</label>
<input class="form-control login-frm-input" type="text" id="username" name="username" placeholder="Email Id" pattern="[^@\s]+@[^@\s]+\.[^@\s]+" title="Invalid email address" runat="server" required="required">
</div>
</div>
<!-- end login -->
<div class="unit">
<div class="input login-input">
<label class="icon-left" for="password">
<i class="zmdi zmdi-key"></i>
</label>
    <asp:Label ID="lblmessage1" runat="server" Text=""></asp:Label>
<input class="form-control login-frm-input" type="text" id="phone" name="phone" placeholder="Phone No" runat="server" pattern="[1-9]{1}[0-9]{9}"
                title="10 digit no." required="required">
<span class="hint">

</span>
</div>
</div>
<!-- start password -->

    <div class="unit">
<div class="input login-input">
<label class="icon-left" for="password">
<i class="zmdi zmdi-key"></i>
</label>
    
<input class="form-control login-frm-input" type="text" id="txtusername" name="txtusername" placeholder="Name" runat="server" 
               required="required">
<span class="hint">

</span>
</div>
</div>

<div class="unit" style="
    margin-bottom: 0;
" >
<div class="input login-input">
<label class="icon-left" for="password">
<i class="zmdi zmdi-key"></i>
</label>
<input class="form-control login-frm-input" type="password" id="password" name="password" placeholder="Password" runat="server" required="required">
<span class="hint">

</span>
</div>
</div>

<div class="unit">
<div class="input login-input">
<label class="icon-left" for="password">
<i class="zmdi zmdi-key"></i>
</label>
<input class="form-control login-frm-input" type="password" id="ConfirmPassword" name="ConfirmPassword" placeholder="Confirm Password" runat="server" required="required">
<span class="hint">

</span>
</div>
</div>
<!-- end password -->


<!-- start keep logged -->

<!-- end keep logged -->

<!-- start response from server -->
<div class="response"></div>
<!-- end response from server -->



</div>
<div class="login-form-footer">
<asp:Button ID="btnsignin" runat="server" Text="Sign in" class="btn-block btn btn-info primary-btn" OnClick="btnsignin_Click"/>
     <asp:Label ID="lblmessage" runat="server" Text=""></asp:Label>
</div>
    
   
  
<div class="divider-text gap-top-20 gap-bottom-45">

</div>


    </form>
<!-- start social buttons -->
<div class="row" style="display:none">
<div class="col-md-6 col-sm-6">
<div class="social-btn twitter">
<i class="fa fa-twitter"></i>
<button type="button">Twitter</button>
</div>
</div>
<div class="col-md-6 col-sm-6">
<div class="social-btn facebook">
<i class="fa fa-facebook"></i>
<button type="button" onclick="get();">Facebook</button>
</div>
</div>
</div>







</form>
</div>
</div>
</div>
<!--Footer Start Here -->
<footer class="login-page-footer">
<div class="container">
<div class="row">
<div class="col-md-12 col-md-offset-12 col-sm-12 col-sm-offset-12">
<div class="footer-content">

</div>
</div>
</div>
</div>
</footer>
<!--Footer End Here -->
</section>
<!--Page Container End Here-->
<script src="js/lib/jquery.js"></script>
<script src="js/lib/jquery-migrate.js"></script>
<script src="js/lib/bootstrap.js"></script>
<script src="js/lib/jRespond.js"></script>
<script src="js/lib/hammerjs.js"></script>
<script src="js/lib/jquery.hammer.js"></script>
<script src="js/lib/smoothscroll.js"></script>
<script src="js/lib/smart-resize.js"></script>

<script src="js/lib/jquery.validate.js"></script>
<script src="js/lib/jquery.form.js"></script>
<script src="js/lib/j-forms.js"></script>
<script src="js/lib/login-validation.js"></script>
</body>
   
    <div id="auth-status">
<div id="auth-loggedout">

<div class="fb-login-button" autologoutlink="true" scope="email,user_checkins"></div>
</div>
<div id="auth-loggedin" style="display: none">
<span id="auth-displayname" style="display:none"></span>(<a href="#" id="auth-logoutlink"></a>)
</div>
</div>
   
</html>
