﻿using ASPSnippets.FaceBookAPI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using TweetSharp;

namespace SatlujWebApp
{
    public partial class Notification : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            postApi();
            if (Session["username"].ToString()==null||Session["username"].ToString()=="")
            {
                Response.Redirect("Login.aspx");
            }
            if (!Page.IsPostBack)
            {
                BindData();
                SqlConnection con1 = new SqlConnection(GetConnectionString());
                SqlCommand cmdcount = new SqlCommand("select count(*) from tblnotificationurja", con1);
                con1.Open();
                int countnotification = (int)cmdcount.ExecuteScalar();
                lblnotificationcount.Text = countnotification.ToString();
                cmdcount.Dispose();
                con1.Close();
                getNotification();
            }
        }
        public void postApi()
        {
            FaceBookConnect.API_Key = "1745025375716484";
            FaceBookConnect.API_Secret = "38e8c9f3760de7371fd74c3a6ebe4156";
            if (!IsPostBack)
            {

                string code = Request.QueryString["code"];
                if (!string.IsNullOrEmpty(code))
                {

                    if (Session["File"] == null)
                    {
                        Dictionary<string, string> d = new Dictionary<string, string>();
                        d.Add("caption", "");
                        d.Add("name", "");
                        d.Add("message", notification.InnerText);
                        d.Add("link", "http://sjvnutpadan.tk/Login.aspx");
                        FaceBookConnect.Post(code, "me/feed", d);
                    }
                    else
                    {
                        Dictionary<string, string> data = new Dictionary<string, string>();
                        data.Add("message", Session["Message"].ToString());
                        FaceBookConnect.Post(code, "me/feed", data);
                    }


                    Session["File"] = null;
                    Session["Message"] = null;
                }



            }
        }
        protected void btnsave_Click(object sender, EventArgs e)
        {

            string key = "bsSMEsGevLD1heaq5g27FjUOv";
            string secret = "lwdT7jUc6tFsfW2oJ1Emo1A4vkAAkvi0DosYOZn8HVQVO08g0c";
            string token = "758607071440928768-8F8AMFndOAnSIlUqZxRTSFy3H0ydsX6";
            string tokenSecret = "KJUNWzFybPtw8OAu8jRv31Wk0HNW9hPYe70sTOIm6SooU";
            string message = notification.InnerText;          
            string imagePath = string.Empty;
            var service = new TweetSharp.TwitterService(key, secret);
            service.AuthenticateWith(token, tokenSecret);      
            if (imagePath.Length > 0)
            {
                using (var stream = new FileStream(imagePath, FileMode.Open))
                {
                    var result = service.SendTweetWithMedia(new SendTweetWithMediaOptions
                    {
                        Status = message,
                        Images = new Dictionary<string, Stream> { { "john", stream } }
                    });
                }
            }
            else 
            {
                var result = service.SendTweet(new SendTweetOptions
                {
                    Status = message
                });

            }

  
            SqlConnection con = new SqlConnection(GetConnectionString());
            con.Open();
            SqlCommand cmd = new SqlCommand("Insert into TblNotificationUrja values(@notification,@Time)", con);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@notification",notification.Value.ToString());
            cmd.Parameters.AddWithValue("@Time", System.DateTime.Now.ToString());
            cmd.ExecuteNonQuery();
            cmd.Dispose();
           // notification.Value = "";
            lblmessage.Text = "Record nserted!";
            string DeviceId = "";
            FCMPushNotification ins = new FCMPushNotification();
            SqlCommand cmddevice = new SqlCommand("SELECT deviceid FROM [PushNotification] where Platform='android' and Location='Urja'", con);
            SqlDataAdapter da = new SqlDataAdapter(cmddevice);
            DataTable ds = new DataTable();
            da.Fill(ds);
            foreach (DataRow row in ds.Rows)
            {
                DeviceId = row["deviceid"].ToString();
                ins.SendNotification1(notification.Value.ToString(), DeviceId);
            }


            Session["File"] = "";
            Session["Message"] = notification.InnerText;
            FaceBookConnect.Authorize("user_photos", Request.Url.AbsoluteUri.Split('?')[0]);
           // btnsave.Enabled = false;

        }


        public class FCMPushNotification
        {
            public FCMPushNotification()
            {
                // TODO: Add constructor logic here
            }

            public bool Successful
            {
                get;
                set;
            }

            public string Response
            {
                get;
                set;
            }
            public Exception Error
            {
                get;
                set;
            }



            public FCMPushNotification SendNotification1(string message, string DeviceId)
            {
                FCMPushNotification result = new FCMPushNotification();
                try
                {
                    result.Successful = true;
                    result.Error = null;
                    // var value = message;
                    var requestUri = "https://fcm.googleapis.com/fcm/send";

                    WebRequest webRequest = WebRequest.Create(requestUri);
                    webRequest.Method = "POST";
                    webRequest.Headers.Add(string.Format("Authorization: key={0}", "AAAANjpfqIk:APA91bE7eodrWNy-Mu4cPF9AD-pcuyOc95kPDCjPaVwLbgLctOr0LMN_vKOSnxM8NQwFLk06mrbePCnWS-VGJbdzRr6XMv0WNCjXfmRv5v7U04JiESqKy8dskjGrrLm6JSweqBYpY8it"));
                    webRequest.Headers.Add(string.Format("Sender: id={0}", "232907581577"));
                    webRequest.ContentType = "application/json";

                    var data = new
                    {
                        to = DeviceId, // Uncoment this if you want to test for single device
                        // to = "/topics/" + _topic, // this is for topic 
                        notification = new
                        {
                            title = "Notification",
                            body = message,
                            //icon="mTeyicon"
                        }
                    };
                    var serializer = new JavaScriptSerializer();
                    var json = serializer.Serialize(data);

                    Byte[] byteArray = Encoding.UTF8.GetBytes(json);

                    webRequest.ContentLength = byteArray.Length;
                    using (Stream dataStream = webRequest.GetRequestStream())
                    {
                        dataStream.Write(byteArray, 0, byteArray.Length);

                        using (WebResponse webResponse = webRequest.GetResponse())
                        {
                            using (Stream dataStreamResponse = webResponse.GetResponseStream())
                            {
                                using (StreamReader tReader = new StreamReader(dataStreamResponse))
                                {
                                    String sResponseFromServer = tReader.ReadToEnd();
                                    result.Response = sResponseFromServer;
                                }
                            }
                        }
                    }

                }
                catch (Exception ex)
                {
                    result.Successful = false;
                    result.Response = null;
                    result.Error = ex;
                }
                return result;
            }
        }



        public void BindData()
        {
            SqlConnection con = new SqlConnection(GetConnectionString());
            con.Open();
            SqlCommand cmd = new SqlCommand("select * from TblNotificationUrja order by id desc", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            grdfeedback.DataSource = ds;
            grdfeedback.DataBind();
            cmd.Dispose();
            con.Close();
        }
        public void getNotification()
        {
            SqlConnection con1 = new SqlConnection(GetConnectionString());
            SqlCommand cmdcount = new SqlCommand("select count(*) from tblnotificationurja", con1);
            con1.Open();
            int countnotification = (int)cmdcount.ExecuteScalar();
            lblnotificationcount.Text = countnotification.ToString();
            if (countnotification == 1)
            {
                SqlConnection con = new SqlConnection(GetConnectionString());
                SqlCommand cmd = new SqlCommand("select top(1) notification from [tblnotificationurja] order by id desc", con);
                con.Open();
                cmd.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dr.Read();
                    notification1.Text = dr["notification"].ToString();
                    lblnotification.Text = dr["notification"].ToString();
                }

                dr.Dispose();
                cmd.Dispose();
                con.Close();
            }

            if (countnotification == 2)
            {
                SqlConnection con = new SqlConnection(GetConnectionString());
                SqlCommand cmd = new SqlCommand("select top(1) notification from [tblnotificationurja] order by id desc", con);
                con.Open();
                cmd.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dr.Read();
                    notification1.Text = dr["notification"].ToString();
                    lblnotification.Text = dr["notification"].ToString();
                }

                dr.Dispose();
                cmd.Dispose();

                SqlCommand cmd1 = new SqlCommand("SELECT id, notification FROM [tblnotificationurja] WHERE Id NOT IN (SELECT MAX( Id )FROM [tblnotificationurja] ) order by id desc", con);

                cmd1.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr1 = cmd1.ExecuteReader();
                if (dr1.HasRows)
                {
                    dr1.Read();

                    notification2.Text = dr1["notification"].ToString();
                }
                dr1.Dispose();
                cmd1.Dispose();
                con.Close();

            }
            if (countnotification >= 3)
            {
                SqlConnection con = new SqlConnection(GetConnectionString());
                SqlCommand cmd = new SqlCommand("select top(1) notification from [tblnotificationurja] order by id desc", con);
                con.Open();
                cmd.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dr.Read();
                    notification1.Text = dr["notification"].ToString();
                    lblnotification.Text = dr["notification"].ToString();
                }

                dr.Dispose();
                cmd.Dispose();

                SqlCommand cmd1 = new SqlCommand("SELECT id, notification FROM [tblnotificationurja] WHERE Id NOT IN (SELECT MAX( Id )FROM [tblnotificationurja] ) order by id desc", con);

                cmd1.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr1 = cmd1.ExecuteReader();
                if (dr1.HasRows)
                {
                    dr1.Read();

                    notification2.Text = dr1["notification"].ToString();
                }
                dr1.Dispose();
                cmd1.Dispose();

                SqlCommand cmd2 = new SqlCommand("SELECT TOP 1 * From(select Top 3 * from [tblnotificationurja] ORDER BY Id DESC) x ORDER BY Id", con);

                cmd2.CommandType = System.Data.CommandType.Text;
                SqlDataReader dr2 = cmd2.ExecuteReader();
                if (dr2.HasRows)
                {
                    dr2.Read();
                    notification3.Text = dr2["notification"].ToString();
                }
                dr2.Dispose();
                cmd2.Dispose();
                con.Close();
            }
            con1.Close();
            cmdcount.Dispose();

        }
        public string GetConnectionString()
        {
            return System.Configuration.ConfigurationManager.ConnectionStrings["Local"].ConnectionString;
        }

        protected void grdfeedback_RowEditing(object sender, GridViewEditEventArgs e)
        {
            grdfeedback.EditIndex = e.NewEditIndex;
            BindData();
        }

        protected void grdfeedback_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            grdfeedback.EditIndex = -1;
            BindData();
        }

        protected void grdfeedback_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            Label id = grdfeedback.Rows[e.RowIndex].FindControl("lblid") as Label;
            TextBox Name = grdfeedback.Rows[e.RowIndex].FindControl("txtname") as TextBox;
           

            SqlConnection con = new SqlConnection(GetConnectionString());
            con.Open();
            //updating the record  
            SqlCommand cmd = new SqlCommand("Update TblNotificationUrja set notification='" + Name.Text + "' where ID=" + Convert.ToInt32(id.Text), con);
            cmd.ExecuteNonQuery();
            con.Close();
            //Setting the EditIndex property to -1 to cancel the Edit mode in Gridview  
            grdfeedback.EditIndex = -1;
            con.Close();
            //Call ShowData method for displaying updated data  
            BindData();
        }

        protected void grdfeedback_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdfeedback.PageIndex = e.NewPageIndex;
            BindData();
        }
    }
}